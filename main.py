import cv2
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import os
import argparse
from PIL import Image
#== Parameters =======================================================================
BLUR = 21
CANNY_THRESH_1 = 150
CANNY_THRESH_2 = 200
MASK_DILATE_ITER = 10
MASK_ERODE_ITER = 10
MASK_COLOR = (0.0,0.0,1.0) # In BGR formatigs
Configs = {
    'version'   : '20210111',
    'inputDIR'  :  "C:/Temp/202012-28152410/",
    'FinalIMG'  :  "C:/Temp/202012-28152410/test99.png",
    'HeatmapIMG':  "C:/Temp/202012-28152410/heattest2.png",

}


def Main():
    getArgs()
    MergeImages(Configs['inputDIR'],Configs['FinalIMG'])
    heatmapimg(Configs['FinalIMG'],Configs['HeatmapIMG'])
def getArgs():
    #Create parser
    parser =argparse.ArgumentParser(description= 'Parse arguments')
    parser.add_argument('-inputdir',nargs=1,type=str,help='input dir for inital image location')
    parser.add_argument('-Finalimg', nargs=1, type=str, help='filepath + name for final combined image')
    parser.add_argument('-Heatmapimg', nargs=1, type=str, help='filepath+name for heatmapimg')

    # parse args
    try:
        args = parser.parse_args()
    except:
        parser.print_help()
       # sys.exit(2)
    if args.inputdir is not None:
        Configs['inputDIR']=args.inputdir[0]
    if args.Finalimg is not None:
        Configs['FinalIMG']=args.Finalimg[0]
    if args.Heatmapimg is not None:
        Configs['HeatmapIMG']=args.Heatmapimg[0]







def MergeImages(inpath, outfinalimg):
    count=0
    for filename in os.listdir(inpath):
        if filename.endswith(".png") and count < 500:
            if count == 0:
                img1 = cv2.imread("C:/Temp/Combined/Starting_Img.png")
                img2 = cv2.imread(inpath + filename)
                # I want to put logo on top-left corner, So I create a ROI
                # rows,cols,channels = img2.shape
                # roi = img1[0:rows, 0:cols ]

                # Now create a mask of logo and create its inverse mask also
                img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

                ret, mask = cv2.threshold(img2gray, 10, 100, cv2.THRESH_BINARY)
                mask_inv = cv2.bitwise_not(mask)

                # Now black-out the area of logo in ROI
                img1_bg = cv2.bitwise_and(img1, img1, mask=mask_inv)

                # Take only region of logo from logo image.
                img2_fg = cv2.bitwise_and(img2, img2, mask=mask)

                # Put logo in ROI and modify the main image
                dst = cv2.add(img2_fg, img1_bg)
                img3 = dst
                cv2.imwrite(outfinalimg, img3)
                count = count + 1
            else:
                img1 = cv2.imread(outfinalimg)
                img2 = cv2.imread(inpath + filename)
                # I want to put logo on top-left corner, So I create a ROI
                # rows, cols, channels = img2.shape
                # roi = img1[0:rows, 0:cols]

                # Now create a mask of logo and create its inverse mask also
                img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
                ret, mask = cv2.threshold(img2gray, 10, 100, cv2.THRESH_BINARY)
                mask_inv = cv2.bitwise_not(mask)

                # Now black-out the area of logo in ROI
                img1_bg = cv2.bitwise_and(img1, img1, mask=mask_inv)

                # Take only region of logo from logo image.
                img2_fg = cv2.bitwise_and(img2, img2, mask=mask)

                # Put logo in ROI and modify the main image
                dst = cv2.add(img2_fg, img1_bg)
                img3 = dst
                cv2.imwrite(outfinalimg, img3)
                count = count + 1
def heatmapimg(inputdir,outheatmapimg):
    image = cv2.imread(inputdir, 0)
    colormap = plt.get_cmap('inferno')
    heatmap = (colormap(image) * 2 ** 16).astype(np.uint16)[:, :, :3]
    heatmap = cv2.cvtColor(heatmap, cv2.COLOR_RGB2BGR)

    cv2.imshow('image', heatmap)
    cv2.imwrite(outheatmapimg, heatmap)
    cv2.waitKey()
if __name__ == '__main__':
   Main()