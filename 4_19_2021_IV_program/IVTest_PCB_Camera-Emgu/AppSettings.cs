﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AlignmentStation
{
  public class AppSettings
  {
        public String CamSerialNumber { get; set; } = "4103357554";
        public int Thr { get; set; } = 120;

        public int WA_x { get; set; } = 100;
        public int WA_y { get; set; } = 50;
        public int WA_height { get; set; } = 200;
        public int WA_width { get; set; } =200;
	public int WA_centroid0 { get; set; } = 0;


        public int WA_x1 { get; set; } = 500;
        public int WA_y1 { get; set; } = 50;
        public int WA_height1 { get; set; } = 200;
        public int WA_width1 { get; set; } = 200;
	public int WA_centroid1 { get; set; } = 0;

        public int WA_x2 { get; set; } = 100;
        public int WA_y2 { get; set; } = 300;
        public int WA_height2 { get; set; } = 200;
        public int WA_width2 { get; set; } = 200;
	public int WA_centroid2 { get; set; } = 0;

        public int WA_x3 { get; set; } = 500;
        public int WA_y3 { get; set; } = 300;
        public int WA_height3 { get; set; } = 200;
        public int WA_width3 { get; set; } = 200;
	public int WA_centroid3 { get; set; } = 0;

    }
}
