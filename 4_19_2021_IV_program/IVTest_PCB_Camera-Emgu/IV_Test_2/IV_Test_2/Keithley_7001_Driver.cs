﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Newport.PowerMeterCommands;
using System;
using System.Threading;
using log4net;
using System.Windows.Forms;
using System.IO.Ports;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using log4net.Config;
namespace IV_Test
{
    class Keithley_7001_Driver
    {

        public static void clos_rows_test(NationalInstruments.VisaNS.MessageBasedSession KE7001A, NationalInstruments.VisaNS.MessageBasedSession KE7001B,int colnum)
        {
            try
            {
                //KE7001A.Write(":clos (@	1	!	1	!	1	)");
                //KE7001A.Write(":clos (@	1	!	2	!	1	)");
                //KE7001A.Write(":clos (@	1	!	3	!	1	)");
                //KE7001A.Write(":clos (@	1	!	4	!	1	)");
                //KE7001A.Write(":clos (@	1	!	5	!	1	)");
                KE7001A.Write(":clos (@	1!1!1,1!1!2,1!1!3,1!1!4,1!1!5,1!1!6,1!1!7,1!1!8,1!1!9,1!1!10,2!1!1,2!1!2,2!1!3,2!1!4,2!1!5,2!1!6)");
                if (colnum < 11)
                {
                    KE7001B.Write(":clos (@	1!1!" + colnum + ")");
                }
              
                else 
                {
                    KE7001B.Write(":clos (@	2!1!" + colnum%(10) + ")");


                }

                


            }
            catch (Exception ex)
            {
               
                MessageBox.Show(ex.Message);
            }

        }
        public static void clos_rows_test_2(NationalInstruments.VisaNS.MessageBasedSession KE7001A, NationalInstruments.VisaNS.MessageBasedSession KE7001B, int colnum)
        {
            try
            {
                //KE7001A.Write(":clos (@	1	!	1	!	1	)");
                //KE7001A.Write(":clos (@	1	!	2	!	1	)");
                //KE7001A.Write(":clos (@	1	!	3	!	1	)");
                //KE7001A.Write(":clos (@	1	!	4	!	1	)");
                //KE7001A.Write(":clos (@	1	!	5	!	1	)");
                KE7001A.Write(":clos (@	1!1!1,1!1!2,1!1!3,1!1!4,1!1!5,1!1!6,1!1!7,1!1!8,1!1!9,1!1!10,2!1!1,2!1!2,2!1!3,2!1!4,2!1!5,2!1!6)");
                if (colnum == 1)
                {
                    colnum = 8;
                    KE7001B.Write(":clos (@	1!1!" + colnum + ")");
                }
                else
                {
                    colnum = 6;
                    KE7001B.Write(":clos (@	2!1!" + colnum + ")");
                }
                //  KE7001B.Write(":clos (@	1!1!" + colnum + ")");






            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        public static void test(NationalInstruments.VisaNS.MessageBasedSession KE7001A, NationalInstruments.VisaNS.MessageBasedSession KE7001B, int colnum)
        {
            try
            {
                //KE7001A.Write(":clos (@	1	!	1	!	1	)");
                //KE7001A.Write(":clos (@	1	!	2	!	1	)");
                //KE7001A.Write(":clos (@	1	!	3	!	1	)");
                //KE7001A.Write(":clos (@	1	!	4	!	1	)");
                //KE7001A.Write(":clos (@	1	!	5	!	1	)");
                KE7001A.Write(":clos (@	1!1!1)");

                KE7001B.Write(":clos (@	1!1!" + colnum + ")");


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        public static void open_rows_test(NationalInstruments.VisaNS.MessageBasedSession KE7001A, NationalInstruments.VisaNS.MessageBasedSession KE7001B, int colnum)
        {
            try
            {
                //KE7001A.Write(":clos (@	1	!	1	!	1	)");
                //KE7001A.Write(":clos (@	1	!	2	!	1	)");
                //KE7001A.Write(":clos (@	1	!	3	!	1	)");
                //KE7001A.Write(":clos (@	1	!	4	!	1	)");
                //KE7001A.Write(":clos (@	1	!	5	!	1	)");
                KE7001A.Write(":open (@	1!1!1,1!1!2,1!1!3,1!1!4,1!1!5,1!1!6,1!1!7,1!1!8,1!1!9,1!1!10,2!1!1,2!1!2,2!1!3,2!1!4,2!1!5,2!1!6)");
               //KE7001A.Write(":open (@	1!1!1,1!1!2)");

                if (colnum < 11)
                {
                    KE7001B.Write(":open (@	1!1!" + colnum + ")");
                }
               
                else
                {
                    KE7001B.Write(":open (@	2!1!" + colnum % (10) + ")");


                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        public static void open_rows_test_2(NationalInstruments.VisaNS.MessageBasedSession KE7001A, NationalInstruments.VisaNS.MessageBasedSession KE7001B, int colnum)
        {
            try
            {
                //KE7001A.Write(":clos (@	1	!	1	!	1	)");
                //KE7001A.Write(":clos (@	1	!	2	!	1	)");
                //KE7001A.Write(":clos (@	1	!	3	!	1	)");
                //KE7001A.Write(":clos (@	1	!	4	!	1	)");
                //KE7001A.Write(":clos (@	1	!	5	!	1	)");
                KE7001A.Write(":open (@	1!1!1,1!1!2,1!1!3,1!1!4,1!1!5,1!1!6,1!1!7,1!1!8,1!1!9,1!1!10,2!1!1,2!1!2,2!1!3,2!1!4,2!1!5,2!1!6)");
                //KE7001A.Write(":open (@	1!1!1,1!1!2)");

                if (colnum == 1)
                {
                    colnum = 8;
                    KE7001B.Write(":open (@	1!1!" + colnum + ")");
                }
                else
                {
                    colnum = 6;
                    KE7001B.Write(":open(@ 2!1!" + colnum + ")");

                }
                //KE7001B.Write(":open(@ 1!1!" + colnum + ")");



            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
    }
}
