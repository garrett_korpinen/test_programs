﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Newport.PowerMeterCommands;
using System;
using System.Threading;
using log4net;
using System.Windows.Forms;
using System.IO.Ports;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using log4net.Config;

namespace IV_Test
{
    class Vektrex_Driver
    {
        public IP_Port SMUSS400;

        public static IP_Port Initial_SMUSS400(string pulse_shap, double compvoltage, double current, double pulson, double pulseoff)
        {
         


            IP_Port SMUSS400;
            SMUSS400 = new IP_Port("10.0.0.220", 8282);
            SMUSS400.writeLine("*IDN?");
          

            SMUSS400.writeLine("SOUR1:FUNC:SHAP " + pulse_shap);            //mode single pulse
            SMUSS400.writeLine("SOUR1:VOLT " + compvoltage.ToString());                          //set compliance Voltage            
            SMUSS400.writeLine("SOUR1:CURR " + current.ToString());                         //set current
            SMUSS400.writeLine("SOUR1:PULS:TON " + pulson.ToString());                 //set Time ON 
            SMUSS400.writeLine("SOUR1:PULS:TOFF " + pulseoff.ToString());
            SMUSS400.writeLine("SOUR1:PULS:OFFSET 0");

            SMUSS400.writeLine("SOUR0:PULS:TRIG ALWAYS"); //set trigger to always occur after current is sent
                                                          // SMUSS400.writeLine("SOUR0:PULS:TRIG:DEL .10");  //set delay for trigger in seconds
            SMUSS400.writeLine("OUTP0:TRIG:SLOP POS"); // POS for Hi_Lo NEG for Lo_Hi


            SMUSS400.writeLine("VOLT:TRIG:SOUR HARDWARE");
            SMUSS400.writeLine("VOLT:TRIG:EDGE RISING");
            SMUSS400.writeLine("VOLT:TRIG:DEL 80");
            SMUSS400.writeLine("VOLT:READ:COUN 5");
            SMUSS400.writeLine("VOLT:TRIG:COUN 1");
            SMUSS400.writeLine("VOLT:RANG 10");
            SMUSS400.writeLine("VOLT:APER 2");
            SMUSS400.writeLine("OUTP1 ON");
            return SMUSS400;
   



        }
        public static void switchon(IP_Port SMUSS400)
        {

            SMUSS400.writeLine("OUTP1 OFF");
            SMUSS400.writeLine("OUTP1:CONN AUX");
            




        }
        public static void switchoff(IP_Port SMUSS400)
        {

            //SMUSS400.writeLine("OUTP1 OFF");
            //SMUSS400.writeLine("OUTP1 OFF");
            SMUSS400.writeLine("OUTP1:CONN PRI");





        }
        public static string Single_Pulse(IP_Port SMUSS400, string pulse_shap, double compvoltage, double current, double pulson, double pulseoff)
        {
            try
            {
                //Thread.Sleep(200);


                var watch = System.Diagnostics.Stopwatch.StartNew();
                


                string dataline = "0.0";
                string MeasuredVoltage;
                // IP_Port SMUSS400;
                //IP_Port SMUSS400;
                SMUSS400.writeLine("SOUR1:FUNC:SHAP " + pulse_shap);            //mode single pulse
                SMUSS400.writeLine("SOUR1:VOLT " + compvoltage.ToString());                          //set compliance Voltage            
                SMUSS400.writeLine("SOUR1:CURR " + current.ToString());                         //set current
                SMUSS400.writeLine("SOUR1:PULS:TON " + pulson.ToString());                 //set Time ON 
                SMUSS400.writeLine("SOUR1:PULS:TOFF " + pulseoff.ToString());
                SMUSS400.writeLine("SOUR1:PULS:OFFSET 0");

                SMUSS400.writeLine("SOUR0:PULS:TRIG ALWAYS"); //set trigger to always occur after current is sent
                                                              // SMUSS400.writeLine("SOUR0:PULS:TRIG:DEL .10");  //set delay for trigger in seconds
                SMUSS400.writeLine("OUTP0:TRIG:SLOP POS"); // POS for Hi_Lo NEG for Lo_Hi


                SMUSS400.writeLine("VOLT:TRIG:SOUR HARDWARE");
                SMUSS400.writeLine("VOLT:TRIG:EDGE RISING");
                SMUSS400.writeLine("VOLT:TRIG:DEL 80");
                SMUSS400.writeLine("VOLT:READ:COUN 5");
                SMUSS400.writeLine("VOLT:TRIG:COUN 1");
                SMUSS400.writeLine("VOLT:RANG 10");
                SMUSS400.writeLine("VOLT:APER 2");
                SMUSS400.writeLine("OUTP1 ON");

                // SMUSS400.writeLine("OUTP1 ON");
                //Thread.Sleep(90);
                SMUSS400.writeLine("VOLT:INIT");
                Thread.Sleep(200);
                SMUSS400.writeLine("OUTP1:TRIG");


                SMUSS400.writeLine("VOLT:FETC?");

                dataline = SMUSS400.readLine();

                MeasuredVoltage = Convert.ToString((Convert.ToDouble(dataline.Substring(0, 4)) + Convert.ToDouble(dataline.Substring(14, 4)) + Convert.ToDouble(dataline.Substring(28, 4)) + Convert.ToDouble(dataline.Substring(42, 4)) + Convert.ToDouble(dataline.Substring(56, 4))) / 5);

                // SMUSS400.writeLine("OUTP1 OFF");
                Thread.Sleep(100);
                //MessageBox.Show("this is in the single pulse func"+MeasuredVoltage.ToString());


                
                return "999";
                //watch.Stop();
               // var elapsedMs = watch.ElapsedMilliseconds;
                
                //return dataline;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "-5";
                
            }
            
        }

        public static string pulldata(IP_Port SMUSS400)
        {
            try
            {
                //Thread.Sleep(200);
  

                var watch = System.Diagnostics.Stopwatch.StartNew();



                string dataline = "0.0";
                string MeasuredVoltage;
                // IP_Port SMUSS400;
                //IP_Port SMUSS400;
              

                SMUSS400.writeLine("VOLT:FETC?");

                dataline = SMUSS400.readLine();

                MeasuredVoltage = Convert.ToString((Convert.ToDouble(dataline.Substring(0, 4)) + Convert.ToDouble(dataline.Substring(14, 4)) + Convert.ToDouble(dataline.Substring(28, 4)) + Convert.ToDouble(dataline.Substring(42, 4)) + Convert.ToDouble(dataline.Substring(56, 4))) / 5);

                // SMUSS400.writeLine("OUTP1 OFF");
                //Thread.Sleep(250);
               // MessageBox.Show("HERE IN PULLDATA"+MeasuredVoltage.ToString());



                return MeasuredVoltage;
                //watch.Stop();
                // var elapsedMs = watch.ElapsedMilliseconds;

                //return dataline;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "-5";

            }

        }
        public static string Single_Pulse2(IP_Port SMUSS400)
        {
            try
            {
                string dataline = "0.0";
                string MeasuredVoltage;

                SMUSS400.writeLine("OUTP1 ON");
                Thread.Sleep(200);
                SMUSS400.writeLine("VOLT:INIT");
                Thread.Sleep(200);
                SMUSS400.writeLine("OUTP1:TRIG");

                SMUSS400.writeLine("VOLT:FETC?");

                dataline = SMUSS400.readLine();

                MeasuredVoltage = Convert.ToString((Convert.ToDouble(dataline.Substring(0, 4)) + Convert.ToDouble(dataline.Substring(14, 4)) + Convert.ToDouble(dataline.Substring(28, 4)) + Convert.ToDouble(dataline.Substring(42, 4)) + Convert.ToDouble(dataline.Substring(56, 4))) / 5);

                SMUSS400.writeLine("OUTP1 OFF");
                Thread.Sleep(100);

                return MeasuredVoltage;
            }
            catch (Exception ex)
            {
                return "-5";
                MessageBox.Show(ex.Message);
            }
            
            //return dataline;
        }
    }
}
