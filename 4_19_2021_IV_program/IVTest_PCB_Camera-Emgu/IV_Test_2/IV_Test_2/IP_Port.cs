﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;

namespace IV_Test
{
    class IP_Port
    {
        //IP Address
        private string IpAddr;
        //N9010A Port number
        private int PortNumber;
        //a network endpoint as an IP address and a port number
        private IPEndPoint ip = null;
        //IPEndPoint property
        public IPEndPoint Ip
        {
            get
            {
                if (ip == null)
                    ip = new IPEndPoint(IPAddress.Parse(this.IpAddr), this.PortNumber);
                return ip;
            }
            set { ip = value; }
        }
        //instrIPAddress "10.0.0.220"
        //instrPortNo: 8282
        public IP_Port(string instrIPAddress, int instrPortNo)
        {
            this.IpAddr = instrIPAddress;
            this.PortNumber = instrPortNo;

            if (!Soket.Connected)
                throw new ApplicationException("Instrument at " + this.Ip.Address + ":" + this.Ip.Port + " is not connected");
        }

        //Writes an remote command to SMU with "\n"
        public void writeLine(string command)
        {
            Soket.Send(Encoding.ASCII.GetBytes(command + "\n"));
        }
        //Reads SMU response as string to a command sent with writeLine method
        public string readLine()
        {
            byte[] data = new byte[1024];
            int receivedDataLength = Soket.Receive(data);
            return Encoding.ASCII.GetString(data, 0, receivedDataLength);
        }

        Socket soket = null;

        //TCPIP connection to SMU 
        public Socket Soket
        {
            get
            {
                if (soket == null)
                {
                    soket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    try
                    {
                        if (!soket.Connected)
                            soket.Connect(this.Ip);
                    }
                    //Throw an exception if not connected
                    catch (SocketException e)
                    {
                        Console.WriteLine("Unable to connect to server.");
                        throw new ApplicationException("Instrument at " + this.Ip.Address + ":" + this.Ip.Port + " is not connected");
                    }
                }
                return soket;
            }
            set { soket = value; }
        }

    }


}
