﻿namespace IV_Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.buttonExit = new System.Windows.Forms.Button();
            this.Initialize = new System.Windows.Forms.Button();
            this.Messages = new System.Windows.Forms.RichTextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxSN = new System.Windows.Forms.TextBox();
            this.labelSN = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonStartTest2 = new System.Windows.Forms.Button();
            this.radioButtonPowerMeter = new System.Windows.Forms.RadioButton();
            this.radioButtonCamera = new System.Windows.Forms.RadioButton();
            this.buttonStartTest = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.buttonOpenAll = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.checkBox26 = new System.Windows.Forms.CheckBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.checkBox33 = new System.Windows.Forms.CheckBox();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.checkBox36 = new System.Windows.Forms.CheckBox();
            this.checkBox37 = new System.Windows.Forms.CheckBox();
            this.checkBox38 = new System.Windows.Forms.CheckBox();
            this.checkBox39 = new System.Windows.Forms.CheckBox();
            this.checkBox40 = new System.Windows.Forms.CheckBox();
            this.checkBox41 = new System.Windows.Forms.CheckBox();
            this.checkBox42 = new System.Windows.Forms.CheckBox();
            this.checkBox43 = new System.Windows.Forms.CheckBox();
            this.checkBox44 = new System.Windows.Forms.CheckBox();
            this.checkBox45 = new System.Windows.Forms.CheckBox();
            this.checkBox46 = new System.Windows.Forms.CheckBox();
            this.checkBox47 = new System.Windows.Forms.CheckBox();
            this.checkBox48 = new System.Windows.Forms.CheckBox();
            this.checkBox49 = new System.Windows.Forms.CheckBox();
            this.checkBox50 = new System.Windows.Forms.CheckBox();
            this.checkBox51 = new System.Windows.Forms.CheckBox();
            this.checkBox52 = new System.Windows.Forms.CheckBox();
            this.checkBox53 = new System.Windows.Forms.CheckBox();
            this.checkBox54 = new System.Windows.Forms.CheckBox();
            this.checkBox55 = new System.Windows.Forms.CheckBox();
            this.checkBox56 = new System.Windows.Forms.CheckBox();
            this.checkBox57 = new System.Windows.Forms.CheckBox();
            this.checkBox58 = new System.Windows.Forms.CheckBox();
            this.checkBox59 = new System.Windows.Forms.CheckBox();
            this.checkBox60 = new System.Windows.Forms.CheckBox();
            this.checkBox61 = new System.Windows.Forms.CheckBox();
            this.checkBox62 = new System.Windows.Forms.CheckBox();
            this.checkBox63 = new System.Windows.Forms.CheckBox();
            this.checkBox64 = new System.Windows.Forms.CheckBox();
            this.checkBox65 = new System.Windows.Forms.CheckBox();
            this.checkBox66 = new System.Windows.Forms.CheckBox();
            this.checkBox67 = new System.Windows.Forms.CheckBox();
            this.checkBox68 = new System.Windows.Forms.CheckBox();
            this.checkBox69 = new System.Windows.Forms.CheckBox();
            this.checkBox70 = new System.Windows.Forms.CheckBox();
            this.checkBox71 = new System.Windows.Forms.CheckBox();
            this.checkBox72 = new System.Windows.Forms.CheckBox();
            this.checkBox73 = new System.Windows.Forms.CheckBox();
            this.checkBox74 = new System.Windows.Forms.CheckBox();
            this.checkBox75 = new System.Windows.Forms.CheckBox();
            this.checkBox76 = new System.Windows.Forms.CheckBox();
            this.checkBox77 = new System.Windows.Forms.CheckBox();
            this.checkBox78 = new System.Windows.Forms.CheckBox();
            this.checkBox79 = new System.Windows.Forms.CheckBox();
            this.checkBox80 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox81 = new System.Windows.Forms.CheckBox();
            this.checkBox82 = new System.Windows.Forms.CheckBox();
            this.checkBox83 = new System.Windows.Forms.CheckBox();
            this.checkBox84 = new System.Windows.Forms.CheckBox();
            this.checkBox85 = new System.Windows.Forms.CheckBox();
            this.checkBox86 = new System.Windows.Forms.CheckBox();
            this.checkBox87 = new System.Windows.Forms.CheckBox();
            this.checkBox88 = new System.Windows.Forms.CheckBox();
            this.checkBox89 = new System.Windows.Forms.CheckBox();
            this.checkBox90 = new System.Windows.Forms.CheckBox();
            this.checkBox91 = new System.Windows.Forms.CheckBox();
            this.checkBox92 = new System.Windows.Forms.CheckBox();
            this.checkBox93 = new System.Windows.Forms.CheckBox();
            this.checkBox94 = new System.Windows.Forms.CheckBox();
            this.checkBox95 = new System.Windows.Forms.CheckBox();
            this.checkBox96 = new System.Windows.Forms.CheckBox();
            this.checkBox97 = new System.Windows.Forms.CheckBox();
            this.checkBox98 = new System.Windows.Forms.CheckBox();
            this.checkBox99 = new System.Windows.Forms.CheckBox();
            this.checkBox100 = new System.Windows.Forms.CheckBox();
            this.checkBox101 = new System.Windows.Forms.CheckBox();
            this.checkBox102 = new System.Windows.Forms.CheckBox();
            this.checkBox103 = new System.Windows.Forms.CheckBox();
            this.checkBox104 = new System.Windows.Forms.CheckBox();
            this.checkBox105 = new System.Windows.Forms.CheckBox();
            this.checkBox106 = new System.Windows.Forms.CheckBox();
            this.checkBox107 = new System.Windows.Forms.CheckBox();
            this.checkBox108 = new System.Windows.Forms.CheckBox();
            this.checkBox109 = new System.Windows.Forms.CheckBox();
            this.checkBox110 = new System.Windows.Forms.CheckBox();
            this.checkBox111 = new System.Windows.Forms.CheckBox();
            this.checkBox112 = new System.Windows.Forms.CheckBox();
            this.checkBox113 = new System.Windows.Forms.CheckBox();
            this.checkBox114 = new System.Windows.Forms.CheckBox();
            this.checkBox115 = new System.Windows.Forms.CheckBox();
            this.checkBox116 = new System.Windows.Forms.CheckBox();
            this.checkBox117 = new System.Windows.Forms.CheckBox();
            this.checkBox118 = new System.Windows.Forms.CheckBox();
            this.checkBox119 = new System.Windows.Forms.CheckBox();
            this.checkBox120 = new System.Windows.Forms.CheckBox();
            this.checkBox121 = new System.Windows.Forms.CheckBox();
            this.checkBox122 = new System.Windows.Forms.CheckBox();
            this.checkBox123 = new System.Windows.Forms.CheckBox();
            this.checkBox124 = new System.Windows.Forms.CheckBox();
            this.checkBox125 = new System.Windows.Forms.CheckBox();
            this.checkBox126 = new System.Windows.Forms.CheckBox();
            this.checkBox127 = new System.Windows.Forms.CheckBox();
            this.checkBox128 = new System.Windows.Forms.CheckBox();
            this.checkBox129 = new System.Windows.Forms.CheckBox();
            this.checkBox130 = new System.Windows.Forms.CheckBox();
            this.checkBox131 = new System.Windows.Forms.CheckBox();
            this.checkBox132 = new System.Windows.Forms.CheckBox();
            this.checkBox133 = new System.Windows.Forms.CheckBox();
            this.checkBox134 = new System.Windows.Forms.CheckBox();
            this.checkBox135 = new System.Windows.Forms.CheckBox();
            this.checkBox136 = new System.Windows.Forms.CheckBox();
            this.checkBox137 = new System.Windows.Forms.CheckBox();
            this.checkBox138 = new System.Windows.Forms.CheckBox();
            this.checkBox139 = new System.Windows.Forms.CheckBox();
            this.checkBox140 = new System.Windows.Forms.CheckBox();
            this.checkBox141 = new System.Windows.Forms.CheckBox();
            this.checkBox142 = new System.Windows.Forms.CheckBox();
            this.checkBox143 = new System.Windows.Forms.CheckBox();
            this.checkBox144 = new System.Windows.Forms.CheckBox();
            this.checkBox145 = new System.Windows.Forms.CheckBox();
            this.checkBox146 = new System.Windows.Forms.CheckBox();
            this.checkBox147 = new System.Windows.Forms.CheckBox();
            this.checkBox148 = new System.Windows.Forms.CheckBox();
            this.checkBox149 = new System.Windows.Forms.CheckBox();
            this.checkBox150 = new System.Windows.Forms.CheckBox();
            this.checkBox151 = new System.Windows.Forms.CheckBox();
            this.checkBox152 = new System.Windows.Forms.CheckBox();
            this.checkBox153 = new System.Windows.Forms.CheckBox();
            this.checkBox154 = new System.Windows.Forms.CheckBox();
            this.checkBox155 = new System.Windows.Forms.CheckBox();
            this.checkBox156 = new System.Windows.Forms.CheckBox();
            this.checkBox157 = new System.Windows.Forms.CheckBox();
            this.checkBox158 = new System.Windows.Forms.CheckBox();
            this.checkBox159 = new System.Windows.Forms.CheckBox();
            this.checkBox160 = new System.Windows.Forms.CheckBox();
            this.resultImageBox = new Emgu.CV.UI.ImageBox();
            this.buttonVDC = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.richTextPowerMeter = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxDelay = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.buttonPMSettings = new System.Windows.Forms.Button();
            this.radioButtonKL = new System.Windows.Forms.RadioButton();
            this.radioButtonBK = new System.Windows.Forms.RadioButton();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.radioButtonPulse = new System.Windows.Forms.RadioButton();
            this.button3 = new System.Windows.Forms.Button();
            this.numericUpDownCurrent = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.radioButtonVT = new System.Windows.Forms.RadioButton();
            this.numericUpDownCurrentVT = new System.Windows.Forms.NumericUpDown();
            this.Live = new System.Windows.Forms.Button();
            this.Refresh = new System.Windows.Forms.Button();
            this.Button_Quit = new System.Windows.Forms.Button();
            this.DisplayWindow = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultImageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCurrentVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayWindow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonExit
            // 
            this.buttonExit.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.buttonExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExit.ForeColor = System.Drawing.Color.Red;
            this.buttonExit.Location = new System.Drawing.Point(409, 6);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(2);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(60, 30);
            this.buttonExit.TabIndex = 19;
            this.buttonExit.Text = "CLOSE";
            this.buttonExit.UseVisualStyleBackColor = false;
            this.buttonExit.Click += new System.EventHandler(this.button6_Click);
            // 
            // Initialize
            // 
            this.Initialize.BackColor = System.Drawing.Color.CornflowerBlue;
            this.Initialize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Initialize.Location = new System.Drawing.Point(10, 6);
            this.Initialize.Margin = new System.Windows.Forms.Padding(2);
            this.Initialize.Name = "Initialize";
            this.Initialize.Size = new System.Drawing.Size(60, 30);
            this.Initialize.TabIndex = 0;
            this.Initialize.Text = "Initialize";
            this.Initialize.UseVisualStyleBackColor = false;
            this.Initialize.Click += new System.EventHandler(this.Initialize_Click);
            // 
            // Messages
            // 
            this.Messages.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.Messages.ForeColor = System.Drawing.Color.Blue;
            this.Messages.Location = new System.Drawing.Point(81, 6);
            this.Messages.Margin = new System.Windows.Forms.Padding(2);
            this.Messages.Name = "Messages";
            this.Messages.Size = new System.Drawing.Size(200, 42);
            this.Messages.TabIndex = 1;
            this.Messages.Text = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(296, 6);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(108, 42);
            this.pictureBox1.TabIndex = 72;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxSN
            // 
            this.textBoxSN.BackColor = System.Drawing.Color.White;
            this.textBoxSN.Location = new System.Drawing.Point(328, 38);
            this.textBoxSN.Name = "textBoxSN";
            this.textBoxSN.Size = new System.Drawing.Size(60, 20);
            this.textBoxSN.TabIndex = 112;
            this.textBoxSN.Text = "test";
            // 
            // labelSN
            // 
            this.labelSN.AutoSize = true;
            this.labelSN.ForeColor = System.Drawing.Color.Green;
            this.labelSN.Location = new System.Drawing.Point(258, 41);
            this.labelSN.Name = "labelSN";
            this.labelSN.Size = new System.Drawing.Size(67, 13);
            this.labelSN.TabIndex = 111;
            this.labelSN.Text = "File Name:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.buttonStartTest2);
            this.groupBox4.Controls.Add(this.radioButtonPowerMeter);
            this.groupBox4.Controls.Add(this.radioButtonCamera);
            this.groupBox4.Controls.Add(this.buttonStartTest);
            this.groupBox4.Controls.Add(this.labelSN);
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.textBoxSN);
            this.groupBox4.Controls.Add(this.buttonOpenAll);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.Green;
            this.groupBox4.Location = new System.Drawing.Point(9, 52);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(468, 64);
            this.groupBox4.TabIndex = 99;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "VCSEL Auto DC Test";
            // 
            // buttonStartTest2
            // 
            this.buttonStartTest2.BackColor = System.Drawing.Color.GreenYellow;
            this.buttonStartTest2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonStartTest2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonStartTest2.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartTest2.ForeColor = System.Drawing.Color.Black;
            this.buttonStartTest2.Location = new System.Drawing.Point(391, 41);
            this.buttonStartTest2.Margin = new System.Windows.Forms.Padding(2);
            this.buttonStartTest2.Name = "buttonStartTest2";
            this.buttonStartTest2.Size = new System.Drawing.Size(65, 20);
            this.buttonStartTest2.TabIndex = 365;
            this.buttonStartTest2.Text = "LaserTel";
            this.buttonStartTest2.UseVisualStyleBackColor = false;
//            this.buttonStartTest2.Click += new System.EventHandler(this.buttonStartTest2_Click);
            // 
            // radioButtonPowerMeter
            // 
            this.radioButtonPowerMeter.AutoSize = true;
            this.radioButtonPowerMeter.Location = new System.Drawing.Point(116, 33);
            this.radioButtonPowerMeter.Name = "radioButtonPowerMeter";
            this.radioButtonPowerMeter.Size = new System.Drawing.Size(133, 17);
            this.radioButtonPowerMeter.TabIndex = 352;
            this.radioButtonPowerMeter.Text = "VDC+Image+Power";
            this.radioButtonPowerMeter.UseVisualStyleBackColor = true;
            // 
            // radioButtonCamera
            // 
            this.radioButtonCamera.AutoSize = true;
            this.radioButtonCamera.Checked = true;
            this.radioButtonCamera.Location = new System.Drawing.Point(116, 16);
            this.radioButtonCamera.Name = "radioButtonCamera";
            this.radioButtonCamera.Size = new System.Drawing.Size(91, 17);
            this.radioButtonCamera.TabIndex = 351;
            this.radioButtonCamera.TabStop = true;
            this.radioButtonCamera.Text = "VDC+Image";
            this.radioButtonCamera.UseVisualStyleBackColor = true;
            // 
            // buttonStartTest
            // 
            this.buttonStartTest.BackColor = System.Drawing.Color.GreenYellow;
            this.buttonStartTest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonStartTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonStartTest.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartTest.ForeColor = System.Drawing.Color.Black;
            this.buttonStartTest.Location = new System.Drawing.Point(394, 13);
            this.buttonStartTest.Margin = new System.Windows.Forms.Padding(2);
            this.buttonStartTest.Name = "buttonStartTest";
            this.buttonStartTest.Size = new System.Drawing.Size(66, 20);
            this.buttonStartTest.TabIndex = 266;
            this.buttonStartTest.Text = "AMS Test";
            this.buttonStartTest.UseVisualStyleBackColor = false;
            this.buttonStartTest.Click += new System.EventHandler(this.buttonStartTest_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Yellow;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(254, 11);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(135, 25);
            this.button5.TabIndex = 374;
            this.button5.Text = "Reset_Time_Stamp";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // buttonOpenAll
            // 
            this.buttonOpenAll.BackColor = System.Drawing.Color.Red;
            this.buttonOpenAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonOpenAll.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonOpenAll.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOpenAll.ForeColor = System.Drawing.Color.Black;
            this.buttonOpenAll.Location = new System.Drawing.Point(2, 20);
            this.buttonOpenAll.Margin = new System.Windows.Forms.Padding(2);
            this.buttonOpenAll.Name = "buttonOpenAll";
            this.buttonOpenAll.Size = new System.Drawing.Size(90, 30);
            this.buttonOpenAll.TabIndex = 181;
            this.buttonOpenAll.Text = "Open ALL";
            this.buttonOpenAll.UseVisualStyleBackColor = false;
            this.buttonOpenAll.Click += new System.EventHandler(this.buttonOpenAll_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(20, 150);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 182;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(39, 150);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(15, 14);
            this.checkBox2.TabIndex = 183;
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(60, 150);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(15, 14);
            this.checkBox3.TabIndex = 184;
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(80, 150);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(15, 14);
            this.checkBox4.TabIndex = 185;
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(100, 150);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 186;
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(120, 150);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 187;
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(140, 150);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 188;
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(160, 150);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 189;
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(180, 150);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 190;
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(200, 150);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(15, 14);
            this.checkBox10.TabIndex = 191;
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(20, 170);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(15, 14);
            this.checkBox11.TabIndex = 201;
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.checkBox11_CheckedChanged);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(39, 171);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(15, 14);
            this.checkBox12.TabIndex = 200;
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.checkBox12_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(60, 170);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(15, 14);
            this.checkBox13.TabIndex = 199;
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.checkBox13_CheckedChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(80, 170);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(15, 14);
            this.checkBox14.TabIndex = 198;
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.checkBox14_CheckedChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(100, 170);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(15, 14);
            this.checkBox15.TabIndex = 197;
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.checkBox15_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(121, 170);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(15, 14);
            this.checkBox16.TabIndex = 196;
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.checkBox16_CheckedChanged);
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Location = new System.Drawing.Point(140, 170);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(15, 14);
            this.checkBox17.TabIndex = 195;
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.CheckedChanged += new System.EventHandler(this.checkBox17_CheckedChanged);
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Location = new System.Drawing.Point(160, 170);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(15, 14);
            this.checkBox18.TabIndex = 194;
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.CheckedChanged += new System.EventHandler(this.checkBox18_CheckedChanged);
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Location = new System.Drawing.Point(179, 170);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(15, 14);
            this.checkBox19.TabIndex = 193;
            this.checkBox19.UseVisualStyleBackColor = true;
            this.checkBox19.CheckedChanged += new System.EventHandler(this.checkBox19_CheckedChanged);
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Location = new System.Drawing.Point(200, 170);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(15, 14);
            this.checkBox20.TabIndex = 192;
            this.checkBox20.UseVisualStyleBackColor = true;
            this.checkBox20.CheckedChanged += new System.EventHandler(this.checkBox20_CheckedChanged);
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Location = new System.Drawing.Point(20, 190);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(15, 14);
            this.checkBox21.TabIndex = 221;
            this.checkBox21.UseVisualStyleBackColor = true;
            this.checkBox21.CheckedChanged += new System.EventHandler(this.checkBox21_CheckedChanged);
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.Location = new System.Drawing.Point(40, 190);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(15, 14);
            this.checkBox22.TabIndex = 220;
            this.checkBox22.UseVisualStyleBackColor = true;
            this.checkBox22.CheckedChanged += new System.EventHandler(this.checkBox22_CheckedChanged);
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Location = new System.Drawing.Point(61, 190);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(15, 14);
            this.checkBox23.TabIndex = 219;
            this.checkBox23.UseVisualStyleBackColor = true;
            this.checkBox23.CheckedChanged += new System.EventHandler(this.checkBox23_CheckedChanged);
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.Location = new System.Drawing.Point(81, 190);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(15, 14);
            this.checkBox24.TabIndex = 218;
            this.checkBox24.UseVisualStyleBackColor = true;
            this.checkBox24.CheckedChanged += new System.EventHandler(this.checkBox24_CheckedChanged);
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.Location = new System.Drawing.Point(101, 190);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(15, 14);
            this.checkBox25.TabIndex = 217;
            this.checkBox25.UseVisualStyleBackColor = true;
            this.checkBox25.CheckedChanged += new System.EventHandler(this.checkBox25_CheckedChanged);
            // 
            // checkBox26
            // 
            this.checkBox26.AutoSize = true;
            this.checkBox26.Location = new System.Drawing.Point(122, 190);
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new System.Drawing.Size(15, 14);
            this.checkBox26.TabIndex = 216;
            this.checkBox26.UseVisualStyleBackColor = true;
            this.checkBox26.CheckedChanged += new System.EventHandler(this.checkBox26_CheckedChanged);
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.Location = new System.Drawing.Point(141, 190);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(15, 14);
            this.checkBox27.TabIndex = 215;
            this.checkBox27.UseVisualStyleBackColor = true;
            this.checkBox27.CheckedChanged += new System.EventHandler(this.checkBox27_CheckedChanged);
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.Location = new System.Drawing.Point(161, 190);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(15, 14);
            this.checkBox28.TabIndex = 214;
            this.checkBox28.UseVisualStyleBackColor = true;
            this.checkBox28.CheckedChanged += new System.EventHandler(this.checkBox28_CheckedChanged);
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.Location = new System.Drawing.Point(180, 190);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(15, 14);
            this.checkBox29.TabIndex = 213;
            this.checkBox29.UseVisualStyleBackColor = true;
            this.checkBox29.CheckedChanged += new System.EventHandler(this.checkBox29_CheckedChanged);
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.Location = new System.Drawing.Point(201, 190);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(15, 14);
            this.checkBox30.TabIndex = 212;
            this.checkBox30.UseVisualStyleBackColor = true;
            this.checkBox30.CheckedChanged += new System.EventHandler(this.checkBox30_CheckedChanged);
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.Location = new System.Drawing.Point(20, 210);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(15, 14);
            this.checkBox31.TabIndex = 211;
            this.checkBox31.UseVisualStyleBackColor = true;
            this.checkBox31.CheckedChanged += new System.EventHandler(this.checkBox31_CheckedChanged);
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.Location = new System.Drawing.Point(40, 210);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(15, 14);
            this.checkBox32.TabIndex = 210;
            this.checkBox32.UseVisualStyleBackColor = true;
            this.checkBox32.CheckedChanged += new System.EventHandler(this.checkBox32_CheckedChanged);
            // 
            // checkBox33
            // 
            this.checkBox33.AutoSize = true;
            this.checkBox33.Location = new System.Drawing.Point(60, 210);
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new System.Drawing.Size(15, 14);
            this.checkBox33.TabIndex = 209;
            this.checkBox33.UseVisualStyleBackColor = true;
            this.checkBox33.CheckedChanged += new System.EventHandler(this.checkBox33_CheckedChanged);
            // 
            // checkBox34
            // 
            this.checkBox34.AutoSize = true;
            this.checkBox34.Location = new System.Drawing.Point(80, 210);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new System.Drawing.Size(15, 14);
            this.checkBox34.TabIndex = 208;
            this.checkBox34.UseVisualStyleBackColor = true;
            this.checkBox34.CheckedChanged += new System.EventHandler(this.checkBox34_CheckedChanged);
            // 
            // checkBox35
            // 
            this.checkBox35.AutoSize = true;
            this.checkBox35.Location = new System.Drawing.Point(100, 210);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new System.Drawing.Size(15, 14);
            this.checkBox35.TabIndex = 207;
            this.checkBox35.UseVisualStyleBackColor = true;
            this.checkBox35.CheckedChanged += new System.EventHandler(this.checkBox35_CheckedChanged);
            // 
            // checkBox36
            // 
            this.checkBox36.AutoSize = true;
            this.checkBox36.Location = new System.Drawing.Point(122, 210);
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.Size = new System.Drawing.Size(15, 14);
            this.checkBox36.TabIndex = 206;
            this.checkBox36.UseVisualStyleBackColor = true;
            this.checkBox36.CheckedChanged += new System.EventHandler(this.checkBox36_CheckedChanged);
            // 
            // checkBox37
            // 
            this.checkBox37.AutoSize = true;
            this.checkBox37.Location = new System.Drawing.Point(141, 210);
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.Size = new System.Drawing.Size(15, 14);
            this.checkBox37.TabIndex = 205;
            this.checkBox37.UseVisualStyleBackColor = true;
            this.checkBox37.CheckedChanged += new System.EventHandler(this.checkBox37_CheckedChanged);
            // 
            // checkBox38
            // 
            this.checkBox38.AutoSize = true;
            this.checkBox38.Location = new System.Drawing.Point(161, 210);
            this.checkBox38.Name = "checkBox38";
            this.checkBox38.Size = new System.Drawing.Size(15, 14);
            this.checkBox38.TabIndex = 204;
            this.checkBox38.UseVisualStyleBackColor = true;
            this.checkBox38.CheckedChanged += new System.EventHandler(this.checkBox38_CheckedChanged);
            // 
            // checkBox39
            // 
            this.checkBox39.AutoSize = true;
            this.checkBox39.Location = new System.Drawing.Point(180, 210);
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.Size = new System.Drawing.Size(15, 14);
            this.checkBox39.TabIndex = 203;
            this.checkBox39.UseVisualStyleBackColor = true;
            this.checkBox39.CheckedChanged += new System.EventHandler(this.checkBox39_CheckedChanged);
            // 
            // checkBox40
            // 
            this.checkBox40.AutoSize = true;
            this.checkBox40.Location = new System.Drawing.Point(200, 210);
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.Size = new System.Drawing.Size(15, 14);
            this.checkBox40.TabIndex = 202;
            this.checkBox40.UseVisualStyleBackColor = true;
            this.checkBox40.CheckedChanged += new System.EventHandler(this.checkBox40_CheckedChanged);
            // 
            // checkBox41
            // 
            this.checkBox41.AutoSize = true;
            this.checkBox41.Location = new System.Drawing.Point(276, 150);
            this.checkBox41.Name = "checkBox41";
            this.checkBox41.Size = new System.Drawing.Size(15, 14);
            this.checkBox41.TabIndex = 261;
            this.checkBox41.UseVisualStyleBackColor = true;
            this.checkBox41.CheckedChanged += new System.EventHandler(this.checkBox41_CheckedChanged);
            // 
            // checkBox42
            // 
            this.checkBox42.AutoSize = true;
            this.checkBox42.Location = new System.Drawing.Point(297, 150);
            this.checkBox42.Name = "checkBox42";
            this.checkBox42.Size = new System.Drawing.Size(15, 14);
            this.checkBox42.TabIndex = 260;
            this.checkBox42.UseVisualStyleBackColor = true;
            this.checkBox42.CheckedChanged += new System.EventHandler(this.checkBox42_CheckedChanged);
            // 
            // checkBox43
            // 
            this.checkBox43.AutoSize = true;
            this.checkBox43.Location = new System.Drawing.Point(317, 150);
            this.checkBox43.Name = "checkBox43";
            this.checkBox43.Size = new System.Drawing.Size(15, 14);
            this.checkBox43.TabIndex = 259;
            this.checkBox43.UseVisualStyleBackColor = true;
            this.checkBox43.CheckedChanged += new System.EventHandler(this.checkBox43_CheckedChanged);
            // 
            // checkBox44
            // 
            this.checkBox44.AutoSize = true;
            this.checkBox44.Location = new System.Drawing.Point(337, 150);
            this.checkBox44.Name = "checkBox44";
            this.checkBox44.Size = new System.Drawing.Size(15, 14);
            this.checkBox44.TabIndex = 258;
            this.checkBox44.UseVisualStyleBackColor = true;
            this.checkBox44.CheckedChanged += new System.EventHandler(this.checkBox44_CheckedChanged);
            // 
            // checkBox45
            // 
            this.checkBox45.AutoSize = true;
            this.checkBox45.Location = new System.Drawing.Point(357, 150);
            this.checkBox45.Name = "checkBox45";
            this.checkBox45.Size = new System.Drawing.Size(15, 14);
            this.checkBox45.TabIndex = 257;
            this.checkBox45.UseVisualStyleBackColor = true;
            this.checkBox45.CheckedChanged += new System.EventHandler(this.checkBox45_CheckedChanged);
            // 
            // checkBox46
            // 
            this.checkBox46.AutoSize = true;
            this.checkBox46.Location = new System.Drawing.Point(378, 150);
            this.checkBox46.Name = "checkBox46";
            this.checkBox46.Size = new System.Drawing.Size(15, 14);
            this.checkBox46.TabIndex = 256;
            this.checkBox46.UseVisualStyleBackColor = true;
            this.checkBox46.CheckedChanged += new System.EventHandler(this.checkBox46_CheckedChanged);
            // 
            // checkBox47
            // 
            this.checkBox47.AutoSize = true;
            this.checkBox47.Location = new System.Drawing.Point(397, 150);
            this.checkBox47.Name = "checkBox47";
            this.checkBox47.Size = new System.Drawing.Size(15, 14);
            this.checkBox47.TabIndex = 255;
            this.checkBox47.UseVisualStyleBackColor = true;
            this.checkBox47.CheckedChanged += new System.EventHandler(this.checkBox47_CheckedChanged);
            // 
            // checkBox48
            // 
            this.checkBox48.AutoSize = true;
            this.checkBox48.Location = new System.Drawing.Point(417, 150);
            this.checkBox48.Name = "checkBox48";
            this.checkBox48.Size = new System.Drawing.Size(15, 14);
            this.checkBox48.TabIndex = 254;
            this.checkBox48.UseVisualStyleBackColor = true;
            this.checkBox48.CheckedChanged += new System.EventHandler(this.checkBox48_CheckedChanged);
            // 
            // checkBox49
            // 
            this.checkBox49.AutoSize = true;
            this.checkBox49.Location = new System.Drawing.Point(436, 150);
            this.checkBox49.Name = "checkBox49";
            this.checkBox49.Size = new System.Drawing.Size(15, 14);
            this.checkBox49.TabIndex = 253;
            this.checkBox49.UseVisualStyleBackColor = true;
            this.checkBox49.CheckedChanged += new System.EventHandler(this.checkBox49_CheckedChanged);
            // 
            // checkBox50
            // 
            this.checkBox50.AutoSize = true;
            this.checkBox50.Location = new System.Drawing.Point(457, 150);
            this.checkBox50.Name = "checkBox50";
            this.checkBox50.Size = new System.Drawing.Size(15, 14);
            this.checkBox50.TabIndex = 252;
            this.checkBox50.UseVisualStyleBackColor = true;
            this.checkBox50.CheckedChanged += new System.EventHandler(this.checkBox50_CheckedChanged);
            // 
            // checkBox51
            // 
            this.checkBox51.AutoSize = true;
            this.checkBox51.Location = new System.Drawing.Point(276, 170);
            this.checkBox51.Name = "checkBox51";
            this.checkBox51.Size = new System.Drawing.Size(15, 14);
            this.checkBox51.TabIndex = 251;
            this.checkBox51.UseVisualStyleBackColor = true;
            this.checkBox51.CheckedChanged += new System.EventHandler(this.checkBox51_CheckedChanged);
            // 
            // checkBox52
            // 
            this.checkBox52.AutoSize = true;
            this.checkBox52.Location = new System.Drawing.Point(296, 170);
            this.checkBox52.Name = "checkBox52";
            this.checkBox52.Size = new System.Drawing.Size(15, 14);
            this.checkBox52.TabIndex = 250;
            this.checkBox52.UseVisualStyleBackColor = true;
            this.checkBox52.CheckedChanged += new System.EventHandler(this.checkBox52_CheckedChanged);
            // 
            // checkBox53
            // 
            this.checkBox53.AutoSize = true;
            this.checkBox53.Location = new System.Drawing.Point(316, 170);
            this.checkBox53.Name = "checkBox53";
            this.checkBox53.Size = new System.Drawing.Size(15, 14);
            this.checkBox53.TabIndex = 249;
            this.checkBox53.UseVisualStyleBackColor = true;
            this.checkBox53.CheckedChanged += new System.EventHandler(this.checkBox53_CheckedChanged);
            // 
            // checkBox54
            // 
            this.checkBox54.AutoSize = true;
            this.checkBox54.Location = new System.Drawing.Point(336, 170);
            this.checkBox54.Name = "checkBox54";
            this.checkBox54.Size = new System.Drawing.Size(15, 14);
            this.checkBox54.TabIndex = 248;
            this.checkBox54.UseVisualStyleBackColor = true;
            this.checkBox54.CheckedChanged += new System.EventHandler(this.checkBox54_CheckedChanged);
            // 
            // checkBox55
            // 
            this.checkBox55.AutoSize = true;
            this.checkBox55.Location = new System.Drawing.Point(356, 170);
            this.checkBox55.Name = "checkBox55";
            this.checkBox55.Size = new System.Drawing.Size(15, 14);
            this.checkBox55.TabIndex = 247;
            this.checkBox55.UseVisualStyleBackColor = true;
            this.checkBox55.CheckedChanged += new System.EventHandler(this.checkBox55_CheckedChanged);
            // 
            // checkBox56
            // 
            this.checkBox56.AutoSize = true;
            this.checkBox56.Location = new System.Drawing.Point(378, 170);
            this.checkBox56.Name = "checkBox56";
            this.checkBox56.Size = new System.Drawing.Size(15, 14);
            this.checkBox56.TabIndex = 246;
            this.checkBox56.UseVisualStyleBackColor = true;
            this.checkBox56.CheckedChanged += new System.EventHandler(this.checkBox56_CheckedChanged);
            // 
            // checkBox57
            // 
            this.checkBox57.AutoSize = true;
            this.checkBox57.Location = new System.Drawing.Point(397, 170);
            this.checkBox57.Name = "checkBox57";
            this.checkBox57.Size = new System.Drawing.Size(15, 14);
            this.checkBox57.TabIndex = 245;
            this.checkBox57.UseVisualStyleBackColor = true;
            this.checkBox57.CheckedChanged += new System.EventHandler(this.checkBox57_CheckedChanged);
            // 
            // checkBox58
            // 
            this.checkBox58.AutoSize = true;
            this.checkBox58.Location = new System.Drawing.Point(417, 170);
            this.checkBox58.Name = "checkBox58";
            this.checkBox58.Size = new System.Drawing.Size(15, 14);
            this.checkBox58.TabIndex = 244;
            this.checkBox58.UseVisualStyleBackColor = true;
            this.checkBox58.CheckedChanged += new System.EventHandler(this.checkBox58_CheckedChanged);
            // 
            // checkBox59
            // 
            this.checkBox59.AutoSize = true;
            this.checkBox59.Location = new System.Drawing.Point(436, 170);
            this.checkBox59.Name = "checkBox59";
            this.checkBox59.Size = new System.Drawing.Size(15, 14);
            this.checkBox59.TabIndex = 243;
            this.checkBox59.UseVisualStyleBackColor = true;
            this.checkBox59.CheckedChanged += new System.EventHandler(this.checkBox59_CheckedChanged);
            // 
            // checkBox60
            // 
            this.checkBox60.AutoSize = true;
            this.checkBox60.Location = new System.Drawing.Point(456, 170);
            this.checkBox60.Name = "checkBox60";
            this.checkBox60.Size = new System.Drawing.Size(15, 14);
            this.checkBox60.TabIndex = 242;
            this.checkBox60.UseVisualStyleBackColor = true;
            this.checkBox60.CheckedChanged += new System.EventHandler(this.checkBox60_CheckedChanged);
            // 
            // checkBox61
            // 
            this.checkBox61.AutoSize = true;
            this.checkBox61.Location = new System.Drawing.Point(276, 190);
            this.checkBox61.Name = "checkBox61";
            this.checkBox61.Size = new System.Drawing.Size(15, 14);
            this.checkBox61.TabIndex = 241;
            this.checkBox61.UseVisualStyleBackColor = true;
            this.checkBox61.CheckedChanged += new System.EventHandler(this.checkBox61_CheckedChanged);
            // 
            // checkBox62
            // 
            this.checkBox62.AutoSize = true;
            this.checkBox62.Location = new System.Drawing.Point(296, 190);
            this.checkBox62.Name = "checkBox62";
            this.checkBox62.Size = new System.Drawing.Size(15, 14);
            this.checkBox62.TabIndex = 240;
            this.checkBox62.UseVisualStyleBackColor = true;
            this.checkBox62.CheckedChanged += new System.EventHandler(this.checkBox62_CheckedChanged);
            // 
            // checkBox63
            // 
            this.checkBox63.AutoSize = true;
            this.checkBox63.Location = new System.Drawing.Point(316, 190);
            this.checkBox63.Name = "checkBox63";
            this.checkBox63.Size = new System.Drawing.Size(15, 14);
            this.checkBox63.TabIndex = 239;
            this.checkBox63.UseVisualStyleBackColor = true;
            this.checkBox63.CheckedChanged += new System.EventHandler(this.checkBox63_CheckedChanged);
            // 
            // checkBox64
            // 
            this.checkBox64.AutoSize = true;
            this.checkBox64.Location = new System.Drawing.Point(336, 190);
            this.checkBox64.Name = "checkBox64";
            this.checkBox64.Size = new System.Drawing.Size(15, 14);
            this.checkBox64.TabIndex = 238;
            this.checkBox64.UseVisualStyleBackColor = true;
            this.checkBox64.CheckedChanged += new System.EventHandler(this.checkBox64_CheckedChanged);
            // 
            // checkBox65
            // 
            this.checkBox65.AutoSize = true;
            this.checkBox65.Location = new System.Drawing.Point(356, 190);
            this.checkBox65.Name = "checkBox65";
            this.checkBox65.Size = new System.Drawing.Size(15, 14);
            this.checkBox65.TabIndex = 237;
            this.checkBox65.UseVisualStyleBackColor = true;
            this.checkBox65.CheckedChanged += new System.EventHandler(this.checkBox65_CheckedChanged);
            // 
            // checkBox66
            // 
            this.checkBox66.AutoSize = true;
            this.checkBox66.Location = new System.Drawing.Point(377, 190);
            this.checkBox66.Name = "checkBox66";
            this.checkBox66.Size = new System.Drawing.Size(15, 14);
            this.checkBox66.TabIndex = 236;
            this.checkBox66.UseVisualStyleBackColor = true;
            this.checkBox66.CheckedChanged += new System.EventHandler(this.checkBox66_CheckedChanged);
            // 
            // checkBox67
            // 
            this.checkBox67.AutoSize = true;
            this.checkBox67.Location = new System.Drawing.Point(396, 190);
            this.checkBox67.Name = "checkBox67";
            this.checkBox67.Size = new System.Drawing.Size(15, 14);
            this.checkBox67.TabIndex = 235;
            this.checkBox67.UseVisualStyleBackColor = true;
            this.checkBox67.CheckedChanged += new System.EventHandler(this.checkBox67_CheckedChanged);
            // 
            // checkBox68
            // 
            this.checkBox68.AutoSize = true;
            this.checkBox68.Location = new System.Drawing.Point(416, 190);
            this.checkBox68.Name = "checkBox68";
            this.checkBox68.Size = new System.Drawing.Size(15, 14);
            this.checkBox68.TabIndex = 234;
            this.checkBox68.UseVisualStyleBackColor = true;
            this.checkBox68.CheckedChanged += new System.EventHandler(this.checkBox68_CheckedChanged);
            // 
            // checkBox69
            // 
            this.checkBox69.AutoSize = true;
            this.checkBox69.Location = new System.Drawing.Point(435, 190);
            this.checkBox69.Name = "checkBox69";
            this.checkBox69.Size = new System.Drawing.Size(15, 14);
            this.checkBox69.TabIndex = 233;
            this.checkBox69.UseVisualStyleBackColor = true;
            this.checkBox69.CheckedChanged += new System.EventHandler(this.checkBox69_CheckedChanged);
            // 
            // checkBox70
            // 
            this.checkBox70.AutoSize = true;
            this.checkBox70.Location = new System.Drawing.Point(456, 190);
            this.checkBox70.Name = "checkBox70";
            this.checkBox70.Size = new System.Drawing.Size(15, 14);
            this.checkBox70.TabIndex = 232;
            this.checkBox70.UseVisualStyleBackColor = true;
            this.checkBox70.CheckedChanged += new System.EventHandler(this.checkBox70_CheckedChanged);
            // 
            // checkBox71
            // 
            this.checkBox71.AutoSize = true;
            this.checkBox71.Location = new System.Drawing.Point(276, 210);
            this.checkBox71.Name = "checkBox71";
            this.checkBox71.Size = new System.Drawing.Size(15, 14);
            this.checkBox71.TabIndex = 231;
            this.checkBox71.UseVisualStyleBackColor = true;
            this.checkBox71.CheckedChanged += new System.EventHandler(this.checkBox71_CheckedChanged);
            // 
            // checkBox72
            // 
            this.checkBox72.AutoSize = true;
            this.checkBox72.Location = new System.Drawing.Point(296, 210);
            this.checkBox72.Name = "checkBox72";
            this.checkBox72.Size = new System.Drawing.Size(15, 14);
            this.checkBox72.TabIndex = 230;
            this.checkBox72.UseVisualStyleBackColor = true;
            this.checkBox72.CheckedChanged += new System.EventHandler(this.checkBox72_CheckedChanged);
            // 
            // checkBox73
            // 
            this.checkBox73.AutoSize = true;
            this.checkBox73.Location = new System.Drawing.Point(316, 210);
            this.checkBox73.Name = "checkBox73";
            this.checkBox73.Size = new System.Drawing.Size(15, 14);
            this.checkBox73.TabIndex = 229;
            this.checkBox73.UseVisualStyleBackColor = true;
            this.checkBox73.CheckedChanged += new System.EventHandler(this.checkBox73_CheckedChanged);
            // 
            // checkBox74
            // 
            this.checkBox74.AutoSize = true;
            this.checkBox74.Location = new System.Drawing.Point(337, 210);
            this.checkBox74.Name = "checkBox74";
            this.checkBox74.Size = new System.Drawing.Size(15, 14);
            this.checkBox74.TabIndex = 228;
            this.checkBox74.UseVisualStyleBackColor = true;
            this.checkBox74.CheckedChanged += new System.EventHandler(this.checkBox74_CheckedChanged);
            // 
            // checkBox75
            // 
            this.checkBox75.AutoSize = true;
            this.checkBox75.Location = new System.Drawing.Point(356, 210);
            this.checkBox75.Name = "checkBox75";
            this.checkBox75.Size = new System.Drawing.Size(15, 14);
            this.checkBox75.TabIndex = 227;
            this.checkBox75.UseVisualStyleBackColor = true;
            this.checkBox75.CheckedChanged += new System.EventHandler(this.checkBox75_CheckedChanged);
            // 
            // checkBox76
            // 
            this.checkBox76.AutoSize = true;
            this.checkBox76.Location = new System.Drawing.Point(377, 210);
            this.checkBox76.Name = "checkBox76";
            this.checkBox76.Size = new System.Drawing.Size(15, 14);
            this.checkBox76.TabIndex = 226;
            this.checkBox76.UseVisualStyleBackColor = true;
            this.checkBox76.CheckedChanged += new System.EventHandler(this.checkBox76_CheckedChanged);
            // 
            // checkBox77
            // 
            this.checkBox77.AutoSize = true;
            this.checkBox77.Location = new System.Drawing.Point(396, 210);
            this.checkBox77.Name = "checkBox77";
            this.checkBox77.Size = new System.Drawing.Size(15, 14);
            this.checkBox77.TabIndex = 225;
            this.checkBox77.UseVisualStyleBackColor = true;
            this.checkBox77.CheckedChanged += new System.EventHandler(this.checkBox77_CheckedChanged);
            // 
            // checkBox78
            // 
            this.checkBox78.AutoSize = true;
            this.checkBox78.Location = new System.Drawing.Point(417, 210);
            this.checkBox78.Name = "checkBox78";
            this.checkBox78.Size = new System.Drawing.Size(15, 14);
            this.checkBox78.TabIndex = 224;
            this.checkBox78.UseVisualStyleBackColor = true;
            this.checkBox78.CheckedChanged += new System.EventHandler(this.checkBox78_CheckedChanged);
            // 
            // checkBox79
            // 
            this.checkBox79.AutoSize = true;
            this.checkBox79.Location = new System.Drawing.Point(435, 210);
            this.checkBox79.Name = "checkBox79";
            this.checkBox79.Size = new System.Drawing.Size(15, 14);
            this.checkBox79.TabIndex = 223;
            this.checkBox79.UseVisualStyleBackColor = true;
            this.checkBox79.CheckedChanged += new System.EventHandler(this.checkBox79_CheckedChanged);
            // 
            // checkBox80
            // 
            this.checkBox80.AutoSize = true;
            this.checkBox80.Location = new System.Drawing.Point(456, 210);
            this.checkBox80.Name = "checkBox80";
            this.checkBox80.Size = new System.Drawing.Size(15, 14);
            this.checkBox80.TabIndex = 222;
            this.checkBox80.UseVisualStyleBackColor = true;
            this.checkBox80.CheckedChanged += new System.EventHandler(this.checkBox80_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 13);
            this.label1.TabIndex = 262;
            this.label1.Text = "1    2    3     4     5     6     7     8     9    10";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(274, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 13);
            this.label2.TabIndex = 263;
            this.label2.Text = "1    2    3     4     5     6     7     8     9    10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(97, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 264;
            this.label3.Text = "Card 1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(353, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 265;
            this.label4.Text = "Card 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(96, 238);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 349;
            this.label5.Text = "Card 3";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(353, 238);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 348;
            this.label6.Text = "Card 4";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 253);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(199, 13);
            this.label7.TabIndex = 347;
            this.label7.Text = "1    2    3     4     5     6     7     8     9    10";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(273, 253);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(199, 13);
            this.label8.TabIndex = 346;
            this.label8.Text = "1    2    3     4     5     6     7     8     9    10";
            // 
            // checkBox81
            // 
            this.checkBox81.AutoSize = true;
            this.checkBox81.Location = new System.Drawing.Point(19, 269);
            this.checkBox81.Name = "checkBox81";
            this.checkBox81.Size = new System.Drawing.Size(15, 14);
            this.checkBox81.TabIndex = 345;
            this.checkBox81.UseVisualStyleBackColor = true;
            this.checkBox81.CheckedChanged += new System.EventHandler(this.checkBox81_CheckedChanged);
            // 
            // checkBox82
            // 
            this.checkBox82.AutoSize = true;
            this.checkBox82.Location = new System.Drawing.Point(40, 269);
            this.checkBox82.Name = "checkBox82";
            this.checkBox82.Size = new System.Drawing.Size(15, 14);
            this.checkBox82.TabIndex = 344;
            this.checkBox82.UseVisualStyleBackColor = true;
            this.checkBox82.CheckedChanged += new System.EventHandler(this.checkBox82_CheckedChanged);
            // 
            // checkBox83
            // 
            this.checkBox83.AutoSize = true;
            this.checkBox83.Location = new System.Drawing.Point(60, 269);
            this.checkBox83.Name = "checkBox83";
            this.checkBox83.Size = new System.Drawing.Size(15, 14);
            this.checkBox83.TabIndex = 343;
            this.checkBox83.UseVisualStyleBackColor = true;
            this.checkBox83.CheckedChanged += new System.EventHandler(this.checkBox83_CheckedChanged);
            // 
            // checkBox84
            // 
            this.checkBox84.AutoSize = true;
            this.checkBox84.Location = new System.Drawing.Point(80, 269);
            this.checkBox84.Name = "checkBox84";
            this.checkBox84.Size = new System.Drawing.Size(15, 14);
            this.checkBox84.TabIndex = 342;
            this.checkBox84.UseVisualStyleBackColor = true;
            this.checkBox84.CheckedChanged += new System.EventHandler(this.checkBox84_CheckedChanged);
            // 
            // checkBox85
            // 
            this.checkBox85.AutoSize = true;
            this.checkBox85.Location = new System.Drawing.Point(100, 269);
            this.checkBox85.Name = "checkBox85";
            this.checkBox85.Size = new System.Drawing.Size(15, 14);
            this.checkBox85.TabIndex = 341;
            this.checkBox85.UseVisualStyleBackColor = true;
            this.checkBox85.CheckedChanged += new System.EventHandler(this.checkBox85_CheckedChanged);
            // 
            // checkBox86
            // 
            this.checkBox86.AutoSize = true;
            this.checkBox86.Location = new System.Drawing.Point(121, 269);
            this.checkBox86.Name = "checkBox86";
            this.checkBox86.Size = new System.Drawing.Size(15, 14);
            this.checkBox86.TabIndex = 340;
            this.checkBox86.UseVisualStyleBackColor = true;
            this.checkBox86.CheckedChanged += new System.EventHandler(this.checkBox86_CheckedChanged);
            // 
            // checkBox87
            // 
            this.checkBox87.AutoSize = true;
            this.checkBox87.Location = new System.Drawing.Point(140, 269);
            this.checkBox87.Name = "checkBox87";
            this.checkBox87.Size = new System.Drawing.Size(15, 14);
            this.checkBox87.TabIndex = 339;
            this.checkBox87.UseVisualStyleBackColor = true;
            this.checkBox87.CheckedChanged += new System.EventHandler(this.checkBox87_CheckedChanged);
            // 
            // checkBox88
            // 
            this.checkBox88.AutoSize = true;
            this.checkBox88.Location = new System.Drawing.Point(160, 269);
            this.checkBox88.Name = "checkBox88";
            this.checkBox88.Size = new System.Drawing.Size(15, 14);
            this.checkBox88.TabIndex = 338;
            this.checkBox88.UseVisualStyleBackColor = true;
            this.checkBox88.CheckedChanged += new System.EventHandler(this.checkBox88_CheckedChanged);
            // 
            // checkBox89
            // 
            this.checkBox89.AutoSize = true;
            this.checkBox89.Location = new System.Drawing.Point(179, 269);
            this.checkBox89.Name = "checkBox89";
            this.checkBox89.Size = new System.Drawing.Size(15, 14);
            this.checkBox89.TabIndex = 337;
            this.checkBox89.UseVisualStyleBackColor = true;
            this.checkBox89.CheckedChanged += new System.EventHandler(this.checkBox89_CheckedChanged);
            // 
            // checkBox90
            // 
            this.checkBox90.AutoSize = true;
            this.checkBox90.Location = new System.Drawing.Point(200, 269);
            this.checkBox90.Name = "checkBox90";
            this.checkBox90.Size = new System.Drawing.Size(15, 14);
            this.checkBox90.TabIndex = 336;
            this.checkBox90.UseVisualStyleBackColor = true;
            this.checkBox90.CheckedChanged += new System.EventHandler(this.checkBox90_CheckedChanged);
            // 
            // checkBox91
            // 
            this.checkBox91.AutoSize = true;
            this.checkBox91.Location = new System.Drawing.Point(19, 289);
            this.checkBox91.Name = "checkBox91";
            this.checkBox91.Size = new System.Drawing.Size(15, 14);
            this.checkBox91.TabIndex = 335;
            this.checkBox91.UseVisualStyleBackColor = true;
            this.checkBox91.CheckedChanged += new System.EventHandler(this.checkBox91_CheckedChanged);
            // 
            // checkBox92
            // 
            this.checkBox92.AutoSize = true;
            this.checkBox92.Location = new System.Drawing.Point(39, 289);
            this.checkBox92.Name = "checkBox92";
            this.checkBox92.Size = new System.Drawing.Size(15, 14);
            this.checkBox92.TabIndex = 334;
            this.checkBox92.UseVisualStyleBackColor = true;
            this.checkBox92.CheckedChanged += new System.EventHandler(this.checkBox92_CheckedChanged);
            // 
            // checkBox93
            // 
            this.checkBox93.AutoSize = true;
            this.checkBox93.Location = new System.Drawing.Point(59, 289);
            this.checkBox93.Name = "checkBox93";
            this.checkBox93.Size = new System.Drawing.Size(15, 14);
            this.checkBox93.TabIndex = 333;
            this.checkBox93.UseVisualStyleBackColor = true;
            this.checkBox93.CheckedChanged += new System.EventHandler(this.checkBox93_CheckedChanged);
            // 
            // checkBox94
            // 
            this.checkBox94.AutoSize = true;
            this.checkBox94.Location = new System.Drawing.Point(79, 289);
            this.checkBox94.Name = "checkBox94";
            this.checkBox94.Size = new System.Drawing.Size(15, 14);
            this.checkBox94.TabIndex = 332;
            this.checkBox94.UseVisualStyleBackColor = true;
            this.checkBox94.CheckedChanged += new System.EventHandler(this.checkBox94_CheckedChanged);
            // 
            // checkBox95
            // 
            this.checkBox95.AutoSize = true;
            this.checkBox95.Location = new System.Drawing.Point(99, 289);
            this.checkBox95.Name = "checkBox95";
            this.checkBox95.Size = new System.Drawing.Size(15, 14);
            this.checkBox95.TabIndex = 331;
            this.checkBox95.UseVisualStyleBackColor = true;
            this.checkBox95.CheckedChanged += new System.EventHandler(this.checkBox95_CheckedChanged);
            // 
            // checkBox96
            // 
            this.checkBox96.AutoSize = true;
            this.checkBox96.Location = new System.Drawing.Point(121, 289);
            this.checkBox96.Name = "checkBox96";
            this.checkBox96.Size = new System.Drawing.Size(15, 14);
            this.checkBox96.TabIndex = 330;
            this.checkBox96.UseVisualStyleBackColor = true;
            this.checkBox96.CheckedChanged += new System.EventHandler(this.checkBox96_CheckedChanged);
            // 
            // checkBox97
            // 
            this.checkBox97.AutoSize = true;
            this.checkBox97.Location = new System.Drawing.Point(140, 289);
            this.checkBox97.Name = "checkBox97";
            this.checkBox97.Size = new System.Drawing.Size(15, 14);
            this.checkBox97.TabIndex = 329;
            this.checkBox97.UseVisualStyleBackColor = true;
            this.checkBox97.CheckedChanged += new System.EventHandler(this.checkBox97_CheckedChanged);
            // 
            // checkBox98
            // 
            this.checkBox98.AutoSize = true;
            this.checkBox98.Location = new System.Drawing.Point(160, 289);
            this.checkBox98.Name = "checkBox98";
            this.checkBox98.Size = new System.Drawing.Size(15, 14);
            this.checkBox98.TabIndex = 328;
            this.checkBox98.UseVisualStyleBackColor = true;
            this.checkBox98.CheckedChanged += new System.EventHandler(this.checkBox98_CheckedChanged);
            // 
            // checkBox99
            // 
            this.checkBox99.AutoSize = true;
            this.checkBox99.Location = new System.Drawing.Point(179, 289);
            this.checkBox99.Name = "checkBox99";
            this.checkBox99.Size = new System.Drawing.Size(15, 14);
            this.checkBox99.TabIndex = 327;
            this.checkBox99.UseVisualStyleBackColor = true;
            this.checkBox99.CheckedChanged += new System.EventHandler(this.checkBox99_CheckedChanged);
            // 
            // checkBox100
            // 
            this.checkBox100.AutoSize = true;
            this.checkBox100.Location = new System.Drawing.Point(199, 289);
            this.checkBox100.Name = "checkBox100";
            this.checkBox100.Size = new System.Drawing.Size(15, 14);
            this.checkBox100.TabIndex = 326;
            this.checkBox100.UseVisualStyleBackColor = true;
            this.checkBox100.CheckedChanged += new System.EventHandler(this.checkBox100_CheckedChanged);
            // 
            // checkBox101
            // 
            this.checkBox101.AutoSize = true;
            this.checkBox101.Location = new System.Drawing.Point(19, 309);
            this.checkBox101.Name = "checkBox101";
            this.checkBox101.Size = new System.Drawing.Size(15, 14);
            this.checkBox101.TabIndex = 325;
            this.checkBox101.UseVisualStyleBackColor = true;
            this.checkBox101.CheckedChanged += new System.EventHandler(this.checkBox101_CheckedChanged);
            // 
            // checkBox102
            // 
            this.checkBox102.AutoSize = true;
            this.checkBox102.Location = new System.Drawing.Point(39, 309);
            this.checkBox102.Name = "checkBox102";
            this.checkBox102.Size = new System.Drawing.Size(15, 14);
            this.checkBox102.TabIndex = 324;
            this.checkBox102.UseVisualStyleBackColor = true;
            this.checkBox102.CheckedChanged += new System.EventHandler(this.checkBox102_CheckedChanged);
            // 
            // checkBox103
            // 
            this.checkBox103.AutoSize = true;
            this.checkBox103.Location = new System.Drawing.Point(59, 309);
            this.checkBox103.Name = "checkBox103";
            this.checkBox103.Size = new System.Drawing.Size(15, 14);
            this.checkBox103.TabIndex = 323;
            this.checkBox103.UseVisualStyleBackColor = true;
            this.checkBox103.CheckedChanged += new System.EventHandler(this.checkBox103_CheckedChanged);
            // 
            // checkBox104
            // 
            this.checkBox104.AutoSize = true;
            this.checkBox104.Location = new System.Drawing.Point(79, 309);
            this.checkBox104.Name = "checkBox104";
            this.checkBox104.Size = new System.Drawing.Size(15, 14);
            this.checkBox104.TabIndex = 322;
            this.checkBox104.UseVisualStyleBackColor = true;
            this.checkBox104.CheckedChanged += new System.EventHandler(this.checkBox104_CheckedChanged);
            // 
            // checkBox105
            // 
            this.checkBox105.AutoSize = true;
            this.checkBox105.Location = new System.Drawing.Point(99, 309);
            this.checkBox105.Name = "checkBox105";
            this.checkBox105.Size = new System.Drawing.Size(15, 14);
            this.checkBox105.TabIndex = 321;
            this.checkBox105.UseVisualStyleBackColor = true;
            this.checkBox105.CheckedChanged += new System.EventHandler(this.checkBox105_CheckedChanged);
            // 
            // checkBox106
            // 
            this.checkBox106.AutoSize = true;
            this.checkBox106.Location = new System.Drawing.Point(120, 309);
            this.checkBox106.Name = "checkBox106";
            this.checkBox106.Size = new System.Drawing.Size(15, 14);
            this.checkBox106.TabIndex = 320;
            this.checkBox106.UseVisualStyleBackColor = true;
            this.checkBox106.CheckedChanged += new System.EventHandler(this.checkBox106_CheckedChanged);
            // 
            // checkBox107
            // 
            this.checkBox107.AutoSize = true;
            this.checkBox107.Location = new System.Drawing.Point(139, 309);
            this.checkBox107.Name = "checkBox107";
            this.checkBox107.Size = new System.Drawing.Size(15, 14);
            this.checkBox107.TabIndex = 319;
            this.checkBox107.UseVisualStyleBackColor = true;
            this.checkBox107.CheckedChanged += new System.EventHandler(this.checkBox107_CheckedChanged);
            // 
            // checkBox108
            // 
            this.checkBox108.AutoSize = true;
            this.checkBox108.Location = new System.Drawing.Point(159, 309);
            this.checkBox108.Name = "checkBox108";
            this.checkBox108.Size = new System.Drawing.Size(15, 14);
            this.checkBox108.TabIndex = 318;
            this.checkBox108.UseVisualStyleBackColor = true;
            this.checkBox108.CheckedChanged += new System.EventHandler(this.checkBox108_CheckedChanged);
            // 
            // checkBox109
            // 
            this.checkBox109.AutoSize = true;
            this.checkBox109.Location = new System.Drawing.Point(178, 309);
            this.checkBox109.Name = "checkBox109";
            this.checkBox109.Size = new System.Drawing.Size(15, 14);
            this.checkBox109.TabIndex = 317;
            this.checkBox109.UseVisualStyleBackColor = true;
            this.checkBox109.CheckedChanged += new System.EventHandler(this.checkBox109_CheckedChanged);
            // 
            // checkBox110
            // 
            this.checkBox110.AutoSize = true;
            this.checkBox110.Location = new System.Drawing.Point(199, 309);
            this.checkBox110.Name = "checkBox110";
            this.checkBox110.Size = new System.Drawing.Size(15, 14);
            this.checkBox110.TabIndex = 316;
            this.checkBox110.UseVisualStyleBackColor = true;
            this.checkBox110.CheckedChanged += new System.EventHandler(this.checkBox110_CheckedChanged);
            // 
            // checkBox111
            // 
            this.checkBox111.AutoSize = true;
            this.checkBox111.Location = new System.Drawing.Point(19, 329);
            this.checkBox111.Name = "checkBox111";
            this.checkBox111.Size = new System.Drawing.Size(15, 14);
            this.checkBox111.TabIndex = 315;
            this.checkBox111.UseVisualStyleBackColor = true;
            this.checkBox111.CheckedChanged += new System.EventHandler(this.checkBox111_CheckedChanged);
            // 
            // checkBox112
            // 
            this.checkBox112.AutoSize = true;
            this.checkBox112.Location = new System.Drawing.Point(39, 329);
            this.checkBox112.Name = "checkBox112";
            this.checkBox112.Size = new System.Drawing.Size(15, 14);
            this.checkBox112.TabIndex = 314;
            this.checkBox112.UseVisualStyleBackColor = true;
            this.checkBox112.CheckedChanged += new System.EventHandler(this.checkBox112_CheckedChanged);
            // 
            // checkBox113
            // 
            this.checkBox113.AutoSize = true;
            this.checkBox113.Location = new System.Drawing.Point(59, 329);
            this.checkBox113.Name = "checkBox113";
            this.checkBox113.Size = new System.Drawing.Size(15, 14);
            this.checkBox113.TabIndex = 313;
            this.checkBox113.UseVisualStyleBackColor = true;
            this.checkBox113.CheckedChanged += new System.EventHandler(this.checkBox113_CheckedChanged);
            // 
            // checkBox114
            // 
            this.checkBox114.AutoSize = true;
            this.checkBox114.Location = new System.Drawing.Point(80, 329);
            this.checkBox114.Name = "checkBox114";
            this.checkBox114.Size = new System.Drawing.Size(15, 14);
            this.checkBox114.TabIndex = 312;
            this.checkBox114.UseVisualStyleBackColor = true;
            this.checkBox114.CheckedChanged += new System.EventHandler(this.checkBox114_CheckedChanged);
            // 
            // checkBox115
            // 
            this.checkBox115.AutoSize = true;
            this.checkBox115.Location = new System.Drawing.Point(99, 329);
            this.checkBox115.Name = "checkBox115";
            this.checkBox115.Size = new System.Drawing.Size(15, 14);
            this.checkBox115.TabIndex = 311;
            this.checkBox115.UseVisualStyleBackColor = true;
            this.checkBox115.CheckedChanged += new System.EventHandler(this.checkBox115_CheckedChanged);
            // 
            // checkBox116
            // 
            this.checkBox116.AutoSize = true;
            this.checkBox116.Location = new System.Drawing.Point(120, 329);
            this.checkBox116.Name = "checkBox116";
            this.checkBox116.Size = new System.Drawing.Size(15, 14);
            this.checkBox116.TabIndex = 310;
            this.checkBox116.UseVisualStyleBackColor = true;
            this.checkBox116.CheckedChanged += new System.EventHandler(this.checkBox116_CheckedChanged);
            // 
            // checkBox117
            // 
            this.checkBox117.AutoSize = true;
            this.checkBox117.Location = new System.Drawing.Point(139, 329);
            this.checkBox117.Name = "checkBox117";
            this.checkBox117.Size = new System.Drawing.Size(15, 14);
            this.checkBox117.TabIndex = 309;
            this.checkBox117.UseVisualStyleBackColor = true;
            this.checkBox117.CheckedChanged += new System.EventHandler(this.checkBox117_CheckedChanged);
            // 
            // checkBox118
            // 
            this.checkBox118.AutoSize = true;
            this.checkBox118.Location = new System.Drawing.Point(160, 329);
            this.checkBox118.Name = "checkBox118";
            this.checkBox118.Size = new System.Drawing.Size(15, 14);
            this.checkBox118.TabIndex = 308;
            this.checkBox118.UseVisualStyleBackColor = true;
            this.checkBox118.CheckedChanged += new System.EventHandler(this.checkBox118_CheckedChanged);
            // 
            // checkBox119
            // 
            this.checkBox119.AutoSize = true;
            this.checkBox119.Location = new System.Drawing.Point(178, 329);
            this.checkBox119.Name = "checkBox119";
            this.checkBox119.Size = new System.Drawing.Size(15, 14);
            this.checkBox119.TabIndex = 307;
            this.checkBox119.UseVisualStyleBackColor = true;
            this.checkBox119.CheckedChanged += new System.EventHandler(this.checkBox119_CheckedChanged);
            // 
            // checkBox120
            // 
            this.checkBox120.AutoSize = true;
            this.checkBox120.Location = new System.Drawing.Point(199, 329);
            this.checkBox120.Name = "checkBox120";
            this.checkBox120.Size = new System.Drawing.Size(15, 14);
            this.checkBox120.TabIndex = 306;
            this.checkBox120.UseVisualStyleBackColor = true;
            this.checkBox120.CheckedChanged += new System.EventHandler(this.checkBox120_CheckedChanged);
            // 
            // checkBox121
            // 
            this.checkBox121.AutoSize = true;
            this.checkBox121.Location = new System.Drawing.Point(270, 269);
            this.checkBox121.Name = "checkBox121";
            this.checkBox121.Size = new System.Drawing.Size(15, 14);
            this.checkBox121.TabIndex = 305;
            this.checkBox121.UseVisualStyleBackColor = true;
            this.checkBox121.CheckedChanged += new System.EventHandler(this.checkBox121_CheckedChanged);
            // 
            // checkBox122
            // 
            this.checkBox122.AutoSize = true;
            this.checkBox122.Location = new System.Drawing.Point(291, 269);
            this.checkBox122.Name = "checkBox122";
            this.checkBox122.Size = new System.Drawing.Size(15, 14);
            this.checkBox122.TabIndex = 304;
            this.checkBox122.UseVisualStyleBackColor = true;
            this.checkBox122.CheckedChanged += new System.EventHandler(this.checkBox122_CheckedChanged);
            // 
            // checkBox123
            // 
            this.checkBox123.AutoSize = true;
            this.checkBox123.Location = new System.Drawing.Point(311, 269);
            this.checkBox123.Name = "checkBox123";
            this.checkBox123.Size = new System.Drawing.Size(15, 14);
            this.checkBox123.TabIndex = 303;
            this.checkBox123.UseVisualStyleBackColor = true;
            this.checkBox123.CheckedChanged += new System.EventHandler(this.checkBox123_CheckedChanged);
            // 
            // checkBox124
            // 
            this.checkBox124.AutoSize = true;
            this.checkBox124.Location = new System.Drawing.Point(331, 269);
            this.checkBox124.Name = "checkBox124";
            this.checkBox124.Size = new System.Drawing.Size(15, 14);
            this.checkBox124.TabIndex = 302;
            this.checkBox124.UseVisualStyleBackColor = true;
            this.checkBox124.CheckedChanged += new System.EventHandler(this.checkBox124_CheckedChanged);
            // 
            // checkBox125
            // 
            this.checkBox125.AutoSize = true;
            this.checkBox125.Location = new System.Drawing.Point(351, 269);
            this.checkBox125.Name = "checkBox125";
            this.checkBox125.Size = new System.Drawing.Size(15, 14);
            this.checkBox125.TabIndex = 301;
            this.checkBox125.UseVisualStyleBackColor = true;
            this.checkBox125.CheckedChanged += new System.EventHandler(this.checkBox125_CheckedChanged);
            // 
            // checkBox126
            // 
            this.checkBox126.AutoSize = true;
            this.checkBox126.Location = new System.Drawing.Point(372, 269);
            this.checkBox126.Name = "checkBox126";
            this.checkBox126.Size = new System.Drawing.Size(15, 14);
            this.checkBox126.TabIndex = 300;
            this.checkBox126.UseVisualStyleBackColor = true;
            this.checkBox126.CheckedChanged += new System.EventHandler(this.checkBox126_CheckedChanged);
            // 
            // checkBox127
            // 
            this.checkBox127.AutoSize = true;
            this.checkBox127.Location = new System.Drawing.Point(391, 269);
            this.checkBox127.Name = "checkBox127";
            this.checkBox127.Size = new System.Drawing.Size(15, 14);
            this.checkBox127.TabIndex = 299;
            this.checkBox127.UseVisualStyleBackColor = true;
            this.checkBox127.CheckedChanged += new System.EventHandler(this.checkBox127_CheckedChanged);
            // 
            // checkBox128
            // 
            this.checkBox128.AutoSize = true;
            this.checkBox128.Location = new System.Drawing.Point(411, 269);
            this.checkBox128.Name = "checkBox128";
            this.checkBox128.Size = new System.Drawing.Size(15, 14);
            this.checkBox128.TabIndex = 298;
            this.checkBox128.UseVisualStyleBackColor = true;
            this.checkBox128.CheckedChanged += new System.EventHandler(this.checkBox128_CheckedChanged);
            // 
            // checkBox129
            // 
            this.checkBox129.AutoSize = true;
            this.checkBox129.Location = new System.Drawing.Point(430, 269);
            this.checkBox129.Name = "checkBox129";
            this.checkBox129.Size = new System.Drawing.Size(15, 14);
            this.checkBox129.TabIndex = 297;
            this.checkBox129.UseVisualStyleBackColor = true;
            this.checkBox129.CheckedChanged += new System.EventHandler(this.checkBox129_CheckedChanged);
            // 
            // checkBox130
            // 
            this.checkBox130.AutoSize = true;
            this.checkBox130.Location = new System.Drawing.Point(451, 269);
            this.checkBox130.Name = "checkBox130";
            this.checkBox130.Size = new System.Drawing.Size(15, 14);
            this.checkBox130.TabIndex = 296;
            this.checkBox130.UseVisualStyleBackColor = true;
            this.checkBox130.CheckedChanged += new System.EventHandler(this.checkBox130_CheckedChanged);
            // 
            // checkBox131
            // 
            this.checkBox131.AutoSize = true;
            this.checkBox131.Location = new System.Drawing.Point(270, 289);
            this.checkBox131.Name = "checkBox131";
            this.checkBox131.Size = new System.Drawing.Size(15, 14);
            this.checkBox131.TabIndex = 295;
            this.checkBox131.UseVisualStyleBackColor = true;
            this.checkBox131.CheckedChanged += new System.EventHandler(this.checkBox131_CheckedChanged);
            // 
            // checkBox132
            // 
            this.checkBox132.AutoSize = true;
            this.checkBox132.Location = new System.Drawing.Point(291, 289);
            this.checkBox132.Name = "checkBox132";
            this.checkBox132.Size = new System.Drawing.Size(15, 14);
            this.checkBox132.TabIndex = 294;
            this.checkBox132.UseVisualStyleBackColor = true;
            this.checkBox132.CheckedChanged += new System.EventHandler(this.checkBox132_CheckedChanged);
            // 
            // checkBox133
            // 
            this.checkBox133.AutoSize = true;
            this.checkBox133.Location = new System.Drawing.Point(310, 289);
            this.checkBox133.Name = "checkBox133";
            this.checkBox133.Size = new System.Drawing.Size(15, 14);
            this.checkBox133.TabIndex = 293;
            this.checkBox133.UseVisualStyleBackColor = true;
            this.checkBox133.CheckedChanged += new System.EventHandler(this.checkBox133_CheckedChanged);
            // 
            // checkBox134
            // 
            this.checkBox134.AutoSize = true;
            this.checkBox134.Location = new System.Drawing.Point(330, 289);
            this.checkBox134.Name = "checkBox134";
            this.checkBox134.Size = new System.Drawing.Size(15, 14);
            this.checkBox134.TabIndex = 292;
            this.checkBox134.UseVisualStyleBackColor = true;
            this.checkBox134.CheckedChanged += new System.EventHandler(this.checkBox134_CheckedChanged);
            // 
            // checkBox135
            // 
            this.checkBox135.AutoSize = true;
            this.checkBox135.Location = new System.Drawing.Point(350, 289);
            this.checkBox135.Name = "checkBox135";
            this.checkBox135.Size = new System.Drawing.Size(15, 14);
            this.checkBox135.TabIndex = 291;
            this.checkBox135.UseVisualStyleBackColor = true;
            this.checkBox135.CheckedChanged += new System.EventHandler(this.checkBox135_CheckedChanged);
            // 
            // checkBox136
            // 
            this.checkBox136.AutoSize = true;
            this.checkBox136.Location = new System.Drawing.Point(372, 289);
            this.checkBox136.Name = "checkBox136";
            this.checkBox136.Size = new System.Drawing.Size(15, 14);
            this.checkBox136.TabIndex = 290;
            this.checkBox136.UseVisualStyleBackColor = true;
            this.checkBox136.CheckedChanged += new System.EventHandler(this.checkBox136_CheckedChanged);
            // 
            // checkBox137
            // 
            this.checkBox137.AutoSize = true;
            this.checkBox137.Location = new System.Drawing.Point(391, 289);
            this.checkBox137.Name = "checkBox137";
            this.checkBox137.Size = new System.Drawing.Size(15, 14);
            this.checkBox137.TabIndex = 289;
            this.checkBox137.UseVisualStyleBackColor = true;
            this.checkBox137.CheckedChanged += new System.EventHandler(this.checkBox137_CheckedChanged);
            // 
            // checkBox138
            // 
            this.checkBox138.AutoSize = true;
            this.checkBox138.Location = new System.Drawing.Point(411, 289);
            this.checkBox138.Name = "checkBox138";
            this.checkBox138.Size = new System.Drawing.Size(15, 14);
            this.checkBox138.TabIndex = 288;
            this.checkBox138.UseVisualStyleBackColor = true;
            this.checkBox138.CheckedChanged += new System.EventHandler(this.checkBox138_CheckedChanged);
            // 
            // checkBox139
            // 
            this.checkBox139.AutoSize = true;
            this.checkBox139.Location = new System.Drawing.Point(430, 289);
            this.checkBox139.Name = "checkBox139";
            this.checkBox139.Size = new System.Drawing.Size(15, 14);
            this.checkBox139.TabIndex = 287;
            this.checkBox139.UseVisualStyleBackColor = true;
            this.checkBox139.CheckedChanged += new System.EventHandler(this.checkBox139_CheckedChanged);
            // 
            // checkBox140
            // 
            this.checkBox140.AutoSize = true;
            this.checkBox140.Location = new System.Drawing.Point(450, 289);
            this.checkBox140.Name = "checkBox140";
            this.checkBox140.Size = new System.Drawing.Size(15, 14);
            this.checkBox140.TabIndex = 286;
            this.checkBox140.UseVisualStyleBackColor = true;
            this.checkBox140.CheckedChanged += new System.EventHandler(this.checkBox140_CheckedChanged);
            // 
            // checkBox141
            // 
            this.checkBox141.AutoSize = true;
            this.checkBox141.Location = new System.Drawing.Point(269, 309);
            this.checkBox141.Name = "checkBox141";
            this.checkBox141.Size = new System.Drawing.Size(15, 14);
            this.checkBox141.TabIndex = 285;
            this.checkBox141.UseVisualStyleBackColor = true;
            this.checkBox141.CheckedChanged += new System.EventHandler(this.checkBox141_CheckedChanged);
            // 
            // checkBox142
            // 
            this.checkBox142.AutoSize = true;
            this.checkBox142.Location = new System.Drawing.Point(289, 309);
            this.checkBox142.Name = "checkBox142";
            this.checkBox142.Size = new System.Drawing.Size(15, 14);
            this.checkBox142.TabIndex = 284;
            this.checkBox142.UseVisualStyleBackColor = true;
            this.checkBox142.CheckedChanged += new System.EventHandler(this.checkBox142_CheckedChanged);
            // 
            // checkBox143
            // 
            this.checkBox143.AutoSize = true;
            this.checkBox143.Location = new System.Drawing.Point(309, 309);
            this.checkBox143.Name = "checkBox143";
            this.checkBox143.Size = new System.Drawing.Size(15, 14);
            this.checkBox143.TabIndex = 283;
            this.checkBox143.UseVisualStyleBackColor = true;
            this.checkBox143.CheckedChanged += new System.EventHandler(this.checkBox143_CheckedChanged);
            // 
            // checkBox144
            // 
            this.checkBox144.AutoSize = true;
            this.checkBox144.Location = new System.Drawing.Point(329, 309);
            this.checkBox144.Name = "checkBox144";
            this.checkBox144.Size = new System.Drawing.Size(15, 14);
            this.checkBox144.TabIndex = 282;
            this.checkBox144.UseVisualStyleBackColor = true;
            this.checkBox144.CheckedChanged += new System.EventHandler(this.checkBox144_CheckedChanged);
            // 
            // checkBox145
            // 
            this.checkBox145.AutoSize = true;
            this.checkBox145.Location = new System.Drawing.Point(349, 309);
            this.checkBox145.Name = "checkBox145";
            this.checkBox145.Size = new System.Drawing.Size(15, 14);
            this.checkBox145.TabIndex = 281;
            this.checkBox145.UseVisualStyleBackColor = true;
            this.checkBox145.CheckedChanged += new System.EventHandler(this.checkBox145_CheckedChanged);
            // 
            // checkBox146
            // 
            this.checkBox146.AutoSize = true;
            this.checkBox146.Location = new System.Drawing.Point(370, 309);
            this.checkBox146.Name = "checkBox146";
            this.checkBox146.Size = new System.Drawing.Size(15, 14);
            this.checkBox146.TabIndex = 280;
            this.checkBox146.UseVisualStyleBackColor = true;
            this.checkBox146.CheckedChanged += new System.EventHandler(this.checkBox146_CheckedChanged);
            // 
            // checkBox147
            // 
            this.checkBox147.AutoSize = true;
            this.checkBox147.Location = new System.Drawing.Point(389, 309);
            this.checkBox147.Name = "checkBox147";
            this.checkBox147.Size = new System.Drawing.Size(15, 14);
            this.checkBox147.TabIndex = 279;
            this.checkBox147.UseVisualStyleBackColor = true;
            this.checkBox147.CheckedChanged += new System.EventHandler(this.checkBox147_CheckedChanged);
            // 
            // checkBox148
            // 
            this.checkBox148.AutoSize = true;
            this.checkBox148.Location = new System.Drawing.Point(409, 309);
            this.checkBox148.Name = "checkBox148";
            this.checkBox148.Size = new System.Drawing.Size(15, 14);
            this.checkBox148.TabIndex = 278;
            this.checkBox148.UseVisualStyleBackColor = true;
            this.checkBox148.CheckedChanged += new System.EventHandler(this.checkBox148_CheckedChanged);
            // 
            // checkBox149
            // 
            this.checkBox149.AutoSize = true;
            this.checkBox149.Location = new System.Drawing.Point(428, 309);
            this.checkBox149.Name = "checkBox149";
            this.checkBox149.Size = new System.Drawing.Size(15, 14);
            this.checkBox149.TabIndex = 277;
            this.checkBox149.UseVisualStyleBackColor = true;
            this.checkBox149.CheckedChanged += new System.EventHandler(this.checkBox149_CheckedChanged);
            // 
            // checkBox150
            // 
            this.checkBox150.AutoSize = true;
            this.checkBox150.Location = new System.Drawing.Point(449, 309);
            this.checkBox150.Name = "checkBox150";
            this.checkBox150.Size = new System.Drawing.Size(15, 14);
            this.checkBox150.TabIndex = 276;
            this.checkBox150.UseVisualStyleBackColor = true;
            this.checkBox150.CheckedChanged += new System.EventHandler(this.checkBox150_CheckedChanged);
            // 
            // checkBox151
            // 
            this.checkBox151.AutoSize = true;
            this.checkBox151.Location = new System.Drawing.Point(269, 329);
            this.checkBox151.Name = "checkBox151";
            this.checkBox151.Size = new System.Drawing.Size(15, 14);
            this.checkBox151.TabIndex = 275;
            this.checkBox151.UseVisualStyleBackColor = true;
            this.checkBox151.CheckedChanged += new System.EventHandler(this.checkBox151_CheckedChanged);
            // 
            // checkBox152
            // 
            this.checkBox152.AutoSize = true;
            this.checkBox152.Location = new System.Drawing.Point(291, 329);
            this.checkBox152.Name = "checkBox152";
            this.checkBox152.Size = new System.Drawing.Size(15, 14);
            this.checkBox152.TabIndex = 274;
            this.checkBox152.UseVisualStyleBackColor = true;
            this.checkBox152.CheckedChanged += new System.EventHandler(this.checkBox152_CheckedChanged);
            // 
            // checkBox153
            // 
            this.checkBox153.AutoSize = true;
            this.checkBox153.Location = new System.Drawing.Point(311, 329);
            this.checkBox153.Name = "checkBox153";
            this.checkBox153.Size = new System.Drawing.Size(15, 14);
            this.checkBox153.TabIndex = 273;
            this.checkBox153.UseVisualStyleBackColor = true;
            this.checkBox153.CheckedChanged += new System.EventHandler(this.checkBox153_CheckedChanged);
            // 
            // checkBox154
            // 
            this.checkBox154.AutoSize = true;
            this.checkBox154.Location = new System.Drawing.Point(329, 329);
            this.checkBox154.Name = "checkBox154";
            this.checkBox154.Size = new System.Drawing.Size(15, 14);
            this.checkBox154.TabIndex = 272;
            this.checkBox154.UseVisualStyleBackColor = true;
            this.checkBox154.CheckedChanged += new System.EventHandler(this.checkBox154_CheckedChanged);
            // 
            // checkBox155
            // 
            this.checkBox155.AutoSize = true;
            this.checkBox155.Location = new System.Drawing.Point(349, 329);
            this.checkBox155.Name = "checkBox155";
            this.checkBox155.Size = new System.Drawing.Size(15, 14);
            this.checkBox155.TabIndex = 271;
            this.checkBox155.UseVisualStyleBackColor = true;
            this.checkBox155.CheckedChanged += new System.EventHandler(this.checkBox155_CheckedChanged);
            // 
            // checkBox156
            // 
            this.checkBox156.AutoSize = true;
            this.checkBox156.Location = new System.Drawing.Point(370, 329);
            this.checkBox156.Name = "checkBox156";
            this.checkBox156.Size = new System.Drawing.Size(15, 14);
            this.checkBox156.TabIndex = 270;
            this.checkBox156.UseVisualStyleBackColor = true;
            this.checkBox156.CheckedChanged += new System.EventHandler(this.checkBox156_CheckedChanged);
            // 
            // checkBox157
            // 
            this.checkBox157.AutoSize = true;
            this.checkBox157.Location = new System.Drawing.Point(391, 329);
            this.checkBox157.Name = "checkBox157";
            this.checkBox157.Size = new System.Drawing.Size(15, 14);
            this.checkBox157.TabIndex = 269;
            this.checkBox157.UseVisualStyleBackColor = true;
            this.checkBox157.CheckedChanged += new System.EventHandler(this.checkBox157_CheckedChanged);
            // 
            // checkBox158
            // 
            this.checkBox158.AutoSize = true;
            this.checkBox158.Location = new System.Drawing.Point(409, 329);
            this.checkBox158.Name = "checkBox158";
            this.checkBox158.Size = new System.Drawing.Size(15, 14);
            this.checkBox158.TabIndex = 268;
            this.checkBox158.UseVisualStyleBackColor = true;
            this.checkBox158.CheckedChanged += new System.EventHandler(this.checkBox158_CheckedChanged);
            // 
            // checkBox159
            // 
            this.checkBox159.AutoSize = true;
            this.checkBox159.Location = new System.Drawing.Point(430, 329);
            this.checkBox159.Name = "checkBox159";
            this.checkBox159.Size = new System.Drawing.Size(15, 14);
            this.checkBox159.TabIndex = 267;
            this.checkBox159.UseVisualStyleBackColor = true;
            this.checkBox159.CheckedChanged += new System.EventHandler(this.checkBox159_CheckedChanged);
            // 
            // checkBox160
            // 
            this.checkBox160.AutoSize = true;
            this.checkBox160.Location = new System.Drawing.Point(450, 329);
            this.checkBox160.Name = "checkBox160";
            this.checkBox160.Size = new System.Drawing.Size(15, 14);
            this.checkBox160.TabIndex = 266;
            this.checkBox160.UseVisualStyleBackColor = true;
            this.checkBox160.CheckedChanged += new System.EventHandler(this.checkBox160_CheckedChanged);
            // 
            // resultImageBox
            // 
            this.resultImageBox.Location = new System.Drawing.Point(480, 1);
            this.resultImageBox.Name = "resultImageBox";
            this.resultImageBox.Size = new System.Drawing.Size(800, 775);
            this.resultImageBox.TabIndex = 2;
            this.resultImageBox.TabStop = false;
            // 
            // buttonVDC
            // 
            this.buttonVDC.BackColor = System.Drawing.Color.DodgerBlue;
            this.buttonVDC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonVDC.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonVDC.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVDC.ForeColor = System.Drawing.Color.White;
            this.buttonVDC.Location = new System.Drawing.Point(8, 350);
            this.buttonVDC.Margin = new System.Windows.Forms.Padding(2);
            this.buttonVDC.Name = "buttonVDC";
            this.buttonVDC.Size = new System.Drawing.Size(72, 27);
            this.buttonVDC.TabIndex = 350;
            this.buttonVDC.Text = "ON VDC";
            this.buttonVDC.UseVisualStyleBackColor = false;
            this.buttonVDC.Click += new System.EventHandler(this.buttonVDC_ON_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.IndianRed;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(158, 350);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(73, 27);
            this.button1.TabIndex = 351;
            this.button1.Text = "OFF VDC";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.buttonVDC_OFF_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(83, 350);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(71, 27);
            this.button2.TabIndex = 352;
            this.button2.Text = "Read VDC";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.buttonReadVDC_Click);
            // 
            // richTextPowerMeter
            // 
            this.richTextPowerMeter.BackColor = System.Drawing.SystemColors.Info;
            this.richTextPowerMeter.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextPowerMeter.ForeColor = System.Drawing.Color.Red;
            this.richTextPowerMeter.Location = new System.Drawing.Point(341, 360);
            this.richTextPowerMeter.Name = "richTextPowerMeter";
            this.richTextPowerMeter.Size = new System.Drawing.Size(125, 28);
            this.richTextPowerMeter.TabIndex = 353;
            this.richTextPowerMeter.Text = "0.0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Green;
            this.label9.Location = new System.Drawing.Point(228, 406);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 354;
            this.label9.Text = "Power Meter (pW):";
            // 
            // textBoxDelay
            // 
            this.textBoxDelay.BackColor = System.Drawing.Color.White;
            this.textBoxDelay.Location = new System.Drawing.Point(265, 379);
            this.textBoxDelay.Name = "textBoxDelay";
            this.textBoxDelay.Size = new System.Drawing.Size(39, 20);
            this.textBoxDelay.TabIndex = 355;
            this.textBoxDelay.Text = "100";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Green;
            this.label10.Location = new System.Drawing.Point(250, 363);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 356;
            this.label10.Text = "Test Delay (ms)";
            // 
            // buttonPMSettings
            // 
            this.buttonPMSettings.BackColor = System.Drawing.Color.Yellow;
            this.buttonPMSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPMSettings.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPMSettings.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPMSettings.ForeColor = System.Drawing.Color.Black;
            this.buttonPMSettings.Location = new System.Drawing.Point(349, 406);
            this.buttonPMSettings.Margin = new System.Windows.Forms.Padding(2);
            this.buttonPMSettings.Name = "buttonPMSettings";
            this.buttonPMSettings.Size = new System.Drawing.Size(125, 20);
            this.buttonPMSettings.TabIndex = 357;
            this.buttonPMSettings.Text = "Read Power ";
            this.buttonPMSettings.UseVisualStyleBackColor = false;
            this.buttonPMSettings.Click += new System.EventHandler(this.buttonPMSettings_Click);
            // 
            // radioButtonKL
            // 
            this.radioButtonKL.AutoSize = true;
            this.radioButtonKL.Location = new System.Drawing.Point(19, 398);
            this.radioButtonKL.Name = "radioButtonKL";
            this.radioButtonKL.Size = new System.Drawing.Size(119, 17);
            this.radioButtonKL.TabIndex = 358;
            this.radioButtonKL.Text = "Keithley 2400 (Amp)";
            this.radioButtonKL.UseVisualStyleBackColor = true;
            // 
            // radioButtonBK
            // 
            this.radioButtonBK.AutoSize = true;
            this.radioButtonBK.Location = new System.Drawing.Point(18, 447);
            this.radioButtonBK.Name = "radioButtonBK";
            this.radioButtonBK.Size = new System.Drawing.Size(105, 17);
            this.radioButtonBK.TabIndex = 359;
            this.radioButtonBK.Text = "BK 1688B (Volts)";
            this.radioButtonBK.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DecimalPlaces = 1;
            this.numericUpDown1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown1.Location = new System.Drawing.Point(139, 447);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            75,
            0,
            0,
            65536});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            65536});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(54, 20);
            this.numericUpDown1.TabIndex = 360;
            this.numericUpDown1.Value = new decimal(new int[] {
            40,
            0,
            0,
            65536});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.NumericUpDownBK_ValueChanged);
            // 
            // radioButtonPulse
            // 
            this.radioButtonPulse.AutoSize = true;
            this.radioButtonPulse.Location = new System.Drawing.Point(19, 423);
            this.radioButtonPulse.Name = "radioButtonPulse";
            this.radioButtonPulse.Size = new System.Drawing.Size(118, 17);
            this.radioButtonPulse.TabIndex = 362;
            this.radioButtonPulse.Text = "Keithley 2400 Pulse";
            this.radioButtonPulse.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Yellow;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(139, 423);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(46, 20);
            this.button3.TabIndex = 363;
            this.button3.Text = "Pulse";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // numericUpDownCurrent
            // 
            this.numericUpDownCurrent.DecimalPlaces = 4;
            this.numericUpDownCurrent.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numericUpDownCurrent.Location = new System.Drawing.Point(139, 398);
            this.numericUpDownCurrent.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            131072});
            this.numericUpDownCurrent.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.numericUpDownCurrent.Name = "numericUpDownCurrent";
            this.numericUpDownCurrent.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownCurrent.TabIndex = 364;
            this.numericUpDownCurrent.Value = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.numericUpDownCurrent.ValueChanged += new System.EventHandler(this.NumericUpDownCurrent_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(40, 543);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(168, 13);
            this.label12.TabIndex = 365;
            this.label12.Text = "Card1: Cathode 15, 13, 11, 9, 7, 5";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(39, 569);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(183, 13);
            this.label13.TabIndex = 366;
            this.label13.Text = "Card2: Anode 1, 5, 9, 13, 14, 10, 6, 2";
            // 
            // radioButtonVT
            // 
            this.radioButtonVT.AutoSize = true;
            this.radioButtonVT.Checked = true;
            this.radioButtonVT.Location = new System.Drawing.Point(18, 471);
            this.radioButtonVT.Name = "radioButtonVT";
            this.radioButtonVT.Size = new System.Drawing.Size(91, 17);
            this.radioButtonVT.TabIndex = 367;
            this.radioButtonVT.TabStop = true;
            this.radioButtonVT.Text = "Vektrex (Amp)";
            this.radioButtonVT.UseVisualStyleBackColor = true;
            // 
            // numericUpDownCurrentVT
            // 
            this.numericUpDownCurrentVT.DecimalPlaces = 4;
            this.numericUpDownCurrentVT.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDownCurrentVT.Location = new System.Drawing.Point(137, 471);
            this.numericUpDownCurrentVT.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            131072});
            this.numericUpDownCurrentVT.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDownCurrentVT.Name = "numericUpDownCurrentVT";
            this.numericUpDownCurrentVT.Size = new System.Drawing.Size(54, 20);
            this.numericUpDownCurrentVT.TabIndex = 368;
            this.numericUpDownCurrentVT.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // Live
            // 
            this.Live.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Live.Location = new System.Drawing.Point(1764, 60);
            this.Live.Name = "Live";
            this.Live.Size = new System.Drawing.Size(100, 30);
            this.Live.TabIndex = 373;
            this.Live.Text = "Live";
            this.Live.UseVisualStyleBackColor = true;
            // 
            // Refresh
            // 
            this.Refresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Refresh.Location = new System.Drawing.Point(1764, 25);
            this.Refresh.Name = "Refresh";
            this.Refresh.Size = new System.Drawing.Size(100, 30);
            this.Refresh.TabIndex = 372;
            this.Refresh.Text = "Single Frame";
            this.Refresh.UseVisualStyleBackColor = true;
            this.Refresh.Click += new System.EventHandler(this.Refresh_Click_1);
            // 
            // Button_Quit
            // 
            this.Button_Quit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Quit.Location = new System.Drawing.Point(1764, 96);
            this.Button_Quit.Name = "Button_Quit";
            this.Button_Quit.Size = new System.Drawing.Size(100, 30);
            this.Button_Quit.TabIndex = 371;
            this.Button_Quit.Text = "Quit";
            this.Button_Quit.UseVisualStyleBackColor = true;
            // 
            // DisplayWindow
            // 
            this.DisplayWindow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DisplayWindow.Location = new System.Drawing.Point(552, 1);
            this.DisplayWindow.Name = "DisplayWindow";
            this.DisplayWindow.Size = new System.Drawing.Size(686, 764);
            this.DisplayWindow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.DisplayWindow.TabIndex = 370;
            this.DisplayWindow.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(442, 30);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(3, 3);
            this.pictureBox2.TabIndex = 375;
            this.pictureBox2.TabStop = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Yellow;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(326, 487);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(125, 20);
            this.button4.TabIndex = 376;
            this.button4.Text = "TEST_REVESE_BIAS";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(357, 447);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 377;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1407, 788);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.Live);
            this.Controls.Add(this.Refresh);
            this.Controls.Add(this.Button_Quit);
            this.Controls.Add(this.DisplayWindow);
            this.Controls.Add(this.numericUpDownCurrentVT);
            this.Controls.Add(this.radioButtonVT);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.numericUpDownCurrent);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.radioButtonPulse);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.radioButtonBK);
            this.Controls.Add(this.radioButtonKL);
            this.Controls.Add(this.buttonPMSettings);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxDelay);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.richTextPowerMeter);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonVDC);
            this.Controls.Add(this.resultImageBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.checkBox81);
            this.Controls.Add(this.checkBox82);
            this.Controls.Add(this.checkBox83);
            this.Controls.Add(this.checkBox84);
            this.Controls.Add(this.checkBox85);
            this.Controls.Add(this.checkBox86);
            this.Controls.Add(this.checkBox87);
            this.Controls.Add(this.checkBox88);
            this.Controls.Add(this.checkBox89);
            this.Controls.Add(this.checkBox90);
            this.Controls.Add(this.checkBox91);
            this.Controls.Add(this.checkBox92);
            this.Controls.Add(this.checkBox93);
            this.Controls.Add(this.checkBox94);
            this.Controls.Add(this.checkBox95);
            this.Controls.Add(this.checkBox96);
            this.Controls.Add(this.checkBox97);
            this.Controls.Add(this.checkBox98);
            this.Controls.Add(this.checkBox99);
            this.Controls.Add(this.checkBox100);
            this.Controls.Add(this.checkBox101);
            this.Controls.Add(this.checkBox102);
            this.Controls.Add(this.checkBox103);
            this.Controls.Add(this.checkBox104);
            this.Controls.Add(this.checkBox105);
            this.Controls.Add(this.checkBox106);
            this.Controls.Add(this.checkBox107);
            this.Controls.Add(this.checkBox108);
            this.Controls.Add(this.checkBox109);
            this.Controls.Add(this.checkBox110);
            this.Controls.Add(this.checkBox111);
            this.Controls.Add(this.checkBox112);
            this.Controls.Add(this.checkBox113);
            this.Controls.Add(this.checkBox114);
            this.Controls.Add(this.checkBox115);
            this.Controls.Add(this.checkBox116);
            this.Controls.Add(this.checkBox117);
            this.Controls.Add(this.checkBox118);
            this.Controls.Add(this.checkBox119);
            this.Controls.Add(this.checkBox120);
            this.Controls.Add(this.checkBox121);
            this.Controls.Add(this.checkBox122);
            this.Controls.Add(this.checkBox123);
            this.Controls.Add(this.checkBox124);
            this.Controls.Add(this.checkBox125);
            this.Controls.Add(this.checkBox126);
            this.Controls.Add(this.checkBox127);
            this.Controls.Add(this.checkBox128);
            this.Controls.Add(this.checkBox129);
            this.Controls.Add(this.checkBox130);
            this.Controls.Add(this.checkBox131);
            this.Controls.Add(this.checkBox132);
            this.Controls.Add(this.checkBox133);
            this.Controls.Add(this.checkBox134);
            this.Controls.Add(this.checkBox135);
            this.Controls.Add(this.checkBox136);
            this.Controls.Add(this.checkBox137);
            this.Controls.Add(this.checkBox138);
            this.Controls.Add(this.checkBox139);
            this.Controls.Add(this.checkBox140);
            this.Controls.Add(this.checkBox141);
            this.Controls.Add(this.checkBox142);
            this.Controls.Add(this.checkBox143);
            this.Controls.Add(this.checkBox144);
            this.Controls.Add(this.checkBox145);
            this.Controls.Add(this.checkBox146);
            this.Controls.Add(this.checkBox147);
            this.Controls.Add(this.checkBox148);
            this.Controls.Add(this.checkBox149);
            this.Controls.Add(this.checkBox150);
            this.Controls.Add(this.checkBox151);
            this.Controls.Add(this.checkBox152);
            this.Controls.Add(this.checkBox153);
            this.Controls.Add(this.checkBox154);
            this.Controls.Add(this.checkBox155);
            this.Controls.Add(this.checkBox156);
            this.Controls.Add(this.checkBox157);
            this.Controls.Add(this.checkBox158);
            this.Controls.Add(this.checkBox159);
            this.Controls.Add(this.checkBox160);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkBox41);
            this.Controls.Add(this.checkBox42);
            this.Controls.Add(this.checkBox43);
            this.Controls.Add(this.checkBox44);
            this.Controls.Add(this.checkBox45);
            this.Controls.Add(this.checkBox46);
            this.Controls.Add(this.checkBox47);
            this.Controls.Add(this.checkBox48);
            this.Controls.Add(this.checkBox49);
            this.Controls.Add(this.checkBox50);
            this.Controls.Add(this.checkBox51);
            this.Controls.Add(this.checkBox52);
            this.Controls.Add(this.checkBox53);
            this.Controls.Add(this.checkBox54);
            this.Controls.Add(this.checkBox55);
            this.Controls.Add(this.checkBox56);
            this.Controls.Add(this.checkBox57);
            this.Controls.Add(this.checkBox58);
            this.Controls.Add(this.checkBox59);
            this.Controls.Add(this.checkBox60);
            this.Controls.Add(this.checkBox61);
            this.Controls.Add(this.checkBox62);
            this.Controls.Add(this.checkBox63);
            this.Controls.Add(this.checkBox64);
            this.Controls.Add(this.checkBox65);
            this.Controls.Add(this.checkBox66);
            this.Controls.Add(this.checkBox67);
            this.Controls.Add(this.checkBox68);
            this.Controls.Add(this.checkBox69);
            this.Controls.Add(this.checkBox70);
            this.Controls.Add(this.checkBox71);
            this.Controls.Add(this.checkBox72);
            this.Controls.Add(this.checkBox73);
            this.Controls.Add(this.checkBox74);
            this.Controls.Add(this.checkBox75);
            this.Controls.Add(this.checkBox76);
            this.Controls.Add(this.checkBox77);
            this.Controls.Add(this.checkBox78);
            this.Controls.Add(this.checkBox79);
            this.Controls.Add(this.checkBox80);
            this.Controls.Add(this.checkBox21);
            this.Controls.Add(this.checkBox22);
            this.Controls.Add(this.checkBox23);
            this.Controls.Add(this.checkBox24);
            this.Controls.Add(this.checkBox25);
            this.Controls.Add(this.checkBox26);
            this.Controls.Add(this.checkBox27);
            this.Controls.Add(this.checkBox28);
            this.Controls.Add(this.checkBox29);
            this.Controls.Add(this.checkBox30);
            this.Controls.Add(this.checkBox31);
            this.Controls.Add(this.checkBox32);
            this.Controls.Add(this.checkBox33);
            this.Controls.Add(this.checkBox34);
            this.Controls.Add(this.checkBox35);
            this.Controls.Add(this.checkBox36);
            this.Controls.Add(this.checkBox37);
            this.Controls.Add(this.checkBox38);
            this.Controls.Add(this.checkBox39);
            this.Controls.Add(this.checkBox40);
            this.Controls.Add(this.checkBox11);
            this.Controls.Add(this.checkBox12);
            this.Controls.Add(this.checkBox13);
            this.Controls.Add(this.checkBox14);
            this.Controls.Add(this.checkBox15);
            this.Controls.Add(this.checkBox16);
            this.Controls.Add(this.checkBox17);
            this.Controls.Add(this.checkBox18);
            this.Controls.Add(this.checkBox19);
            this.Controls.Add(this.checkBox20);
            this.Controls.Add(this.checkBox10);
            this.Controls.Add(this.checkBox9);
            this.Controls.Add(this.checkBox8);
            this.Controls.Add(this.checkBox7);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.checkBox5);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.Messages);
            this.Controls.Add(this.Initialize);
            this.Controls.Add(this.groupBox4);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "IV TEST - VCSEL PCB";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultImageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCurrentVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayWindow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Initialize;
        private System.Windows.Forms.RichTextBox Messages;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxSN;
        private System.Windows.Forms.Label labelSN;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonOpenAll;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.CheckBox checkBox26;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.CheckBox checkBox33;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.CheckBox checkBox36;
        private System.Windows.Forms.CheckBox checkBox37;
        private System.Windows.Forms.CheckBox checkBox38;
        private System.Windows.Forms.CheckBox checkBox39;
        private System.Windows.Forms.CheckBox checkBox40;
        private System.Windows.Forms.CheckBox checkBox41;
        private System.Windows.Forms.CheckBox checkBox42;
        private System.Windows.Forms.CheckBox checkBox43;
        private System.Windows.Forms.CheckBox checkBox44;
        private System.Windows.Forms.CheckBox checkBox45;
        private System.Windows.Forms.CheckBox checkBox46;
        private System.Windows.Forms.CheckBox checkBox47;
        private System.Windows.Forms.CheckBox checkBox48;
        private System.Windows.Forms.CheckBox checkBox49;
        private System.Windows.Forms.CheckBox checkBox50;
        private System.Windows.Forms.CheckBox checkBox51;
        private System.Windows.Forms.CheckBox checkBox52;
        private System.Windows.Forms.CheckBox checkBox53;
        private System.Windows.Forms.CheckBox checkBox54;
        private System.Windows.Forms.CheckBox checkBox55;
        private System.Windows.Forms.CheckBox checkBox56;
        private System.Windows.Forms.CheckBox checkBox57;
        private System.Windows.Forms.CheckBox checkBox58;
        private System.Windows.Forms.CheckBox checkBox59;
        private System.Windows.Forms.CheckBox checkBox60;
        private System.Windows.Forms.CheckBox checkBox61;
        private System.Windows.Forms.CheckBox checkBox62;
        private System.Windows.Forms.CheckBox checkBox63;
        private System.Windows.Forms.CheckBox checkBox64;
        private System.Windows.Forms.CheckBox checkBox65;
        private System.Windows.Forms.CheckBox checkBox66;
        private System.Windows.Forms.CheckBox checkBox67;
        private System.Windows.Forms.CheckBox checkBox68;
        private System.Windows.Forms.CheckBox checkBox69;
        private System.Windows.Forms.CheckBox checkBox70;
        private System.Windows.Forms.CheckBox checkBox71;
        private System.Windows.Forms.CheckBox checkBox72;
        private System.Windows.Forms.CheckBox checkBox73;
        private System.Windows.Forms.CheckBox checkBox74;
        private System.Windows.Forms.CheckBox checkBox75;
        private System.Windows.Forms.CheckBox checkBox76;
        private System.Windows.Forms.CheckBox checkBox77;
        private System.Windows.Forms.CheckBox checkBox78;
        private System.Windows.Forms.CheckBox checkBox79;
        private System.Windows.Forms.CheckBox checkBox80;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonStartTest;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBox81;
        private System.Windows.Forms.CheckBox checkBox82;
        private System.Windows.Forms.CheckBox checkBox83;
        private System.Windows.Forms.CheckBox checkBox84;
        private System.Windows.Forms.CheckBox checkBox85;
        private System.Windows.Forms.CheckBox checkBox86;
        private System.Windows.Forms.CheckBox checkBox87;
        private System.Windows.Forms.CheckBox checkBox88;
        private System.Windows.Forms.CheckBox checkBox89;
        private System.Windows.Forms.CheckBox checkBox90;
        private System.Windows.Forms.CheckBox checkBox91;
        private System.Windows.Forms.CheckBox checkBox92;
        private System.Windows.Forms.CheckBox checkBox93;
        private System.Windows.Forms.CheckBox checkBox94;
        private System.Windows.Forms.CheckBox checkBox95;
        private System.Windows.Forms.CheckBox checkBox96;
        private System.Windows.Forms.CheckBox checkBox97;
        private System.Windows.Forms.CheckBox checkBox98;
        private System.Windows.Forms.CheckBox checkBox99;
        private System.Windows.Forms.CheckBox checkBox100;
        private System.Windows.Forms.CheckBox checkBox101;
        private System.Windows.Forms.CheckBox checkBox102;
        private System.Windows.Forms.CheckBox checkBox103;
        private System.Windows.Forms.CheckBox checkBox104;
        private System.Windows.Forms.CheckBox checkBox105;
        private System.Windows.Forms.CheckBox checkBox106;
        private System.Windows.Forms.CheckBox checkBox107;
        private System.Windows.Forms.CheckBox checkBox108;
        private System.Windows.Forms.CheckBox checkBox109;
        private System.Windows.Forms.CheckBox checkBox110;
        private System.Windows.Forms.CheckBox checkBox111;
        private System.Windows.Forms.CheckBox checkBox112;
        private System.Windows.Forms.CheckBox checkBox113;
        private System.Windows.Forms.CheckBox checkBox114;
        private System.Windows.Forms.CheckBox checkBox115;
        private System.Windows.Forms.CheckBox checkBox116;
        private System.Windows.Forms.CheckBox checkBox117;
        private System.Windows.Forms.CheckBox checkBox118;
        private System.Windows.Forms.CheckBox checkBox119;
        private System.Windows.Forms.CheckBox checkBox120;
        private System.Windows.Forms.CheckBox checkBox121;
        private System.Windows.Forms.CheckBox checkBox122;
        private System.Windows.Forms.CheckBox checkBox123;
        private System.Windows.Forms.CheckBox checkBox124;
        private System.Windows.Forms.CheckBox checkBox125;
        private System.Windows.Forms.CheckBox checkBox126;
        private System.Windows.Forms.CheckBox checkBox127;
        private System.Windows.Forms.CheckBox checkBox128;
        private System.Windows.Forms.CheckBox checkBox129;
        private System.Windows.Forms.CheckBox checkBox130;
        private System.Windows.Forms.CheckBox checkBox131;
        private System.Windows.Forms.CheckBox checkBox132;
        private System.Windows.Forms.CheckBox checkBox133;
        private System.Windows.Forms.CheckBox checkBox134;
        private System.Windows.Forms.CheckBox checkBox135;
        private System.Windows.Forms.CheckBox checkBox136;
        private System.Windows.Forms.CheckBox checkBox137;
        private System.Windows.Forms.CheckBox checkBox138;
        private System.Windows.Forms.CheckBox checkBox139;
        private System.Windows.Forms.CheckBox checkBox140;
        private System.Windows.Forms.CheckBox checkBox141;
        private System.Windows.Forms.CheckBox checkBox142;
        private System.Windows.Forms.CheckBox checkBox143;
        private System.Windows.Forms.CheckBox checkBox144;
        private System.Windows.Forms.CheckBox checkBox145;
        private System.Windows.Forms.CheckBox checkBox146;
        private System.Windows.Forms.CheckBox checkBox147;
        private System.Windows.Forms.CheckBox checkBox148;
        private System.Windows.Forms.CheckBox checkBox149;
        private System.Windows.Forms.CheckBox checkBox150;
        private System.Windows.Forms.CheckBox checkBox151;
        private System.Windows.Forms.CheckBox checkBox152;
        private System.Windows.Forms.CheckBox checkBox153;
        private System.Windows.Forms.CheckBox checkBox154;
        private System.Windows.Forms.CheckBox checkBox155;
        private System.Windows.Forms.CheckBox checkBox156;
        private System.Windows.Forms.CheckBox checkBox157;
        private System.Windows.Forms.CheckBox checkBox158;
        private System.Windows.Forms.CheckBox checkBox159;
        private System.Windows.Forms.CheckBox checkBox160;
        private Emgu.CV.UI.ImageBox resultImageBox;
        private System.Windows.Forms.Button buttonVDC;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RichTextBox richTextPowerMeter;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxDelay;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonPMSettings;
        private System.Windows.Forms.RadioButton radioButtonKL;
        private System.Windows.Forms.RadioButton radioButtonBK;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.RadioButton radioButtonPowerMeter;
        private System.Windows.Forms.RadioButton radioButtonCamera;
        private System.Windows.Forms.RadioButton radioButtonPulse;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.NumericUpDown numericUpDownCurrent;
        private System.Windows.Forms.Button buttonStartTest2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.RadioButton radioButtonVT;
        private System.Windows.Forms.NumericUpDown numericUpDownCurrentVT;
        private System.Windows.Forms.Button Live;
        private System.Windows.Forms.Button Refresh;
        private System.Windows.Forms.Button Button_Quit;
        private System.Windows.Forms.PictureBox DisplayWindow;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox1;
    }
}

