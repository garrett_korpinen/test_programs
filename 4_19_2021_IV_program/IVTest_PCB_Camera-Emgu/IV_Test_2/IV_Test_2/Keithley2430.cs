﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Newport.PowerMeterCommands;
using System;
using System.Threading;
using System.Windows.Forms;
using System.IO.Ports;

using Excel = Microsoft.Office.Interop.Excel;

namespace IV_Test
{
    class Keithley2430
    {
        public static void intalize()
        {
            NationalInstruments.VisaNS.MessageBasedSession KE2430 = new NationalInstruments.VisaNS.MessageBasedSession("GPIB0::12::INSTR");
        }
        private void KE2430Configure(NationalInstruments.VisaNS.MessageBasedSession KE2430, float numericUpDownCurrent)
        {
            KE2430.Write(":SYST:BEEP:STAT OFF");        // Set beeper ON/OFF
            KE2430.Write(":SOUR:FUNC CURR");            //Select fixed current source.
            KE2430.Write(":SOUR:CURR:MODE FIXED");      //Select fixed current source.
            KE2430.Write(":SENS:FUNC 'VOLT'");          //Select voltage measure function.
            //KE2430.Write(":SOUR:CURR 0.01");           //Set source to output 10mA.

            KE2430.Write(":SOUR:CURR " + Convert.ToString(numericUpDownCurrent));           //Set source to output 10mA.
            KE2430.Write(":SENS:VOLT:PROT 10");        //Set 10V compliance limit.
            KE2430.Write(":FORM:ELEM VOLT");            // Measure Volt only
        }


        public static void KE2430Pulse(NationalInstruments.VisaNS.MessageBasedSession KE2430, string num)
        {
            KE2430.Write(":SYST:BEEP:STAT OFF");        // Set beeper ON/OFF
            KE2430.Write(":SOUR:FUNC CURR");            //Select fixed current source.
            KE2430.Write(":SOUR:CURR:MODE FIXED");      //Select fixed current source.
            KE2430.Write(":SOUR:FUNC:SHAP PULS");       //Select Pulse Mode of operation.
            KE2430.Write(":SOUR:PULS:WIDT 0.005");      //Set pulse width in sec(n = 0.00015 to 0.0050).
            KE2430.Write(":SOUR:PULS:DEL 0000");        //Set pulse delay in sec(n = 0 to 9999.999).
            KE2430.Write(":SENS:FUNC 'VOLT'");          //Select source function(name = VOLTage or CURRent).
            KE2430.Write(":SENS:VOLT:PROT 8.0");        //Set voltage compliance (n = compliance).
            KE2430.Write(":SOUR:CURR:LEV 0.0035");
            KE2430.Write(":ARM:COUN 1");                //Set arm count (n = 1 to 2500 or INFinite).

            KE2430.Write(":READ?");                     //Start(trigger) pulse output process and acquire pulse readings.
            num = KE2430.ReadString();
        }
        public static string ReadVoltage(NationalInstruments.VisaNS.MessageBasedSession KE2430, string num)
        {
            KE2430.Write(":READ?");
            num = KE2430.ReadString();
            num = num.Substring(1, 12);
            return num; // Convert.ToDouble(num);
        }
        string[,] matrix_VCSEL = new string[16, 16];
        string[] VCSEL_number = new string[257];
        string[] VCSEL_power = new string[257];
        int VCSEL_count;
        //I need to understand how/why this is used ,
        //private void powerUp2430(int testNumber, NationalInstruments.VisaNS.MessageBasedSession KE2430)
        //{

        //    KE2430.Write(":OUTP ON");

        //    if (testNumber == 1)
        //    {
        //        Thread.Sleep(250);
        //        VCSEL_number[VCSEL_count] = ReadVoltage();
        //        SaveImage(folderName + "\\" + "SN-" + textBoxSN.Text + "-" + Convert.ToString(VCSEL_count) + ".jpeg");
        //    }
        //    if (testNumber == 2)
        //    {
        //        VCSEL_power[VCSEL_count] = PowerMeter();
        //    }

        //    KE2430.Write(":OUTP OFF");

        //}


    }
}
