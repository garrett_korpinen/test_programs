﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Newport.PowerMeterCommands;
using System;
using System.Threading;
using log4net;
using System.Windows.Forms;
using System.IO.Ports;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using log4net.Config;

namespace IV_Test
{
    class Keithley2400_Driver
    {

        public static string single_voltage(NationalInstruments.VisaNS.MessageBasedSession KE2400,double volt_val)
        {
            try
            {

                KE2400.Write("*RST");
                KE2400.Write(":SYST:BEEP:STAT OFF");
                KE2400.Write(":SOUR:FUNC VOLT");
                KE2400.Write(":SOUR:VOLT:MODE FIX");
                KE2400.Write(":SOUR:VOLT:RANGE -50");
                KE2400.Write(":SOUR:VOLT:LEV "+volt_val);
                KE2400.Write(":SENS:FUNC \"CURR\"");
                KE2400.Write(":SENS:CURR:PROT 10e-6");
                KE2400.Write(":SENS:CURR:RANG 10e-6");
                KE2400.Write(":FORM:ELEM CURR");
                KE2400.Write(":OUTP ON");
                KE2400.Write(":READ?");
                double output = Convert.ToDouble(KE2400.ReadString()) * Math.Pow(10,6);
                output = Math.Round(output, 4);
                string testotpt = output.ToString();
                Console.WriteLine(testotpt);
               // MessageBox.Show(testotpt);
                KE2400.Write(":OUTP OFF");
                return testotpt;
            }
            catch (Exception ex)
            {
                return "-5";
                MessageBox.Show(ex.Message);
            }

        }

        public static string single_Current(NationalInstruments.VisaNS.MessageBasedSession KE2400, double curr_val)
        {
            try
            {

                KE2400.Write("*RST");
                KE2400.Write(":SYST:BEEP:STAT OFF");
                KE2400.Write(":SOUR:FUNC CURR");
                KE2400.Write(":SOUR:CURR:MODE FIX");
                KE2400.Write(":SOUR:CURR:RANGE MAX");
                KE2400.Write(":SOUR:CURR:LEV "+curr_val);
                KE2400.Write(":SENS:FUNC \"VOLT\"");
                KE2400.Write(":SENS:VOLT:PROT 25");
                KE2400.Write(":SENS:VOLT:RANG 20");
                KE2400.Write(":FORM:ELEM VOLT");
                KE2400.Write(":OUTP ON");
                KE2400.Write(":READ?");
                double output = Convert.ToDouble(KE2400.ReadString());
                output = Math.Round(output, 4);
                string testotpt = output.ToString();
                Console.WriteLine(testotpt);
                MessageBox.Show(testotpt);
                KE2400.Write(":OUTP OFF");
                return testotpt;
            }
            catch (Exception ex)
            {
                return "-5";
                MessageBox.Show(ex.Message);
            }

        }

        public static string sweep(NationalInstruments.VisaNS.MessageBasedSession KE2400)
        {
            try
            {

                KE2400.Write("*RST");
                KE2400.Write(":SOUR:FUNC VOLT");
                KE2400.Write(":SOUR:VOLT:MODE FIX");
                KE2400.Write(":SOUR:VOLT:RANGE -50");
                KE2400.Write(":SOUR:VOLT:LEV -40");
                KE2400.Write(":SENS:FUNC \"CURR\"");
                KE2400.Write(":SENS:CURR:PROT 10e-3");
                KE2400.Write(":SENS:CURR:RANG 10e-3");
                KE2400.Write(":OUTP ON");
                KE2400.Write(":READ?");
                string testotpt = KE2400.ReadString();
                MessageBox.Show(testotpt);
                return testotpt;
            }
            catch (Exception ex)
            {
                return "-5";
                MessageBox.Show(ex.Message);
            }

        }
    }
}
