﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Newport.PowerMeterCommands;
using System;
using System.Threading;
using log4net;
using System.Windows.Forms;
using System.IO.Ports;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using log4net.Config;
using System.Collections.Generic;

using System.Data;
using System.Diagnostics;
using System.IO;

namespace IV_Test
{
    
    public partial class Form1 : Form
    {
        private uEye.Camera Camera = new uEye.Camera();
        IntPtr DisplayHandle = IntPtr.Zero;
        IP_Port SMUSS400;
        
   
        int numOfSpot;
       
        // Initialize the USB driver and get the first device key in the list
        PowerMeterCommands pmCmds;
        string[] line = new string[100];
        string strDeviceKey = string.Empty;

        string[,] matrix_VCSEL = new string[16, 16];
        string[] VCSEL_number = new string[257];
        string[] tempthrowaway = new string[257];
        string[] VCSEL_power = new string[257];
        string[] curr_sweep = new string[50];
        string[] volt_sweep = new string[50];
        int VCSEL_count;

        // Add references: NationalInstruments.VisaNS.dll and NationalInstruments.Common.dll 
        private NationalInstruments.VisaNS.MessageBasedSession KE7001A = new NationalInstruments.VisaNS.MessageBasedSession("GPIB0::02::INSTR");
        private NationalInstruments.VisaNS.MessageBasedSession KE7001B = new NationalInstruments.VisaNS.MessageBasedSession("GPIB0::03::INSTR");
        private NationalInstruments.VisaNS.MessageBasedSession KE2430 = new NationalInstruments.VisaNS.MessageBasedSession("GPIB0::12::INSTR");
        private NationalInstruments.VisaNS.MessageBasedSession KE2400 = new NationalInstruments.VisaNS.MessageBasedSession("GPIB0::05::INSTR");

        private System.Windows.Forms.Timer m_UpdateTimer = new System.Windows.Forms.Timer();

        SerialPort serialPort1;
        private string PortName = "COM8";
        private int BaudRate = 9600;
        private Parity Parity = Parity.None;
        private int DataBits = 8;
        private StopBits StopBits = StopBits.One;

        string num;
        DataTable dt = new DataTable();
        String filetimestamp = GetTimeStamp(DateTime.Now);
        


        DataRow dr = null;


        double[] num2 = new double[10];
        public  Form1()
        {

          
            //Camera();
            try
            {
                InitializeComponent();
                


                CvInvoke.UseOpenCL = false;
                //Add-Columns for Data table
                dt.Columns.Add("Row");
                dt.Columns.Add("Col");
                dt.Columns.Add("Bin");
                dt.Columns.Add("Voltage(V)");
                dt.Columns.Add("Current(A)");

                DisplayHandle = DisplayWindow.Handle;

                //initialize camera settings
                IDS_Driver.init(Camera);
                IDS_Driver.setgain(Camera, 100);

                IDS_Driver.setbacklevl(Camera, 116);
                IDS_Driver.memalloc(Camera);
                IDS_Driver.setexposure(Camera, 30);
                IDS_Driver.setdelay(Camera, 25);
                IDS_Driver.invertimg(Camera);
                IDS_Driver.gettriggersettings(Camera);

                //setup camera and take a picture
                Camera.EventFrame += onFrameEvent;
                IDS_Driver.takepic(Camera);
                

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
      

        private void Initialize_Click(object sender, EventArgs e)
        {
            string _nameInst;

            if (radioButtonKL.Checked == true)
            {
               
                KE2430.Write("*IDN?");
                _nameInst = KE2430.ReadString();
                Messages.AppendText(_nameInst + "\n");
            }

            if (radioButtonVT.Checked == true)
            {
                SMUSS400=Vektrex_Driver.Initial_SMUSS400("SINGLEPULSE", 10, 0.1, .0001, 0.0001);//call"SINGLEPULSE", 10,0.1,.0001,0.0001 vetrext initialize function (pulse mode,compliacne voltage,setcurrent,settime on,set time off)
            }

            if (radioButtonPulse.Checked == true)
            {
              
                KE2430.Write("*IDN?");
                _nameInst = KE2430.ReadString();
                Messages.AppendText(_nameInst + "\n");
            }

            if (radioButtonBK.Checked == true)
                BK1688BConfigure();

            KE7001A.Write("*IDN?");              // write to instrument
            _nameInst = KE7001A.ReadString();    // read from instrument
            Messages.AppendText(_nameInst + "\n");

            KE7001B.Write("*IDN?");              // write to instrument
            _nameInst = KE7001B.ReadString();    // read from instrument
            Messages.AppendText(_nameInst + "\n");
           

            KE2400.Write("*IDN?");              // write to instrument
            _nameInst = KE2400.ReadString();    // read from instrument
            Messages.AppendText(_nameInst + "\n");

            folderName = @"C:\data\PCB test";
            buttonOpenAll.PerformClick();

            //Init_PowerMeter();
           // PowerMeter();
        }

        
      

      
        private void BK1688BConfigure()
        {
            serialPort1 = new SerialPort(PortName, BaudRate, Parity, DataBits, StopBits);

            if (!serialPort1.IsOpen)
                serialPort1.Open();

            serialPort1.Write("VOLT0" + Convert.ToString(Math.Round(numericUpDown1.Value * 10, 0)) + "\r");
        }
       

       
        

        private FolderBrowserDialog folderBrowserDialog1;
        private string folderName;

        private void buttonSelectFolder_Click(object sender, EventArgs e)
        {
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                folderName = folderBrowserDialog1.SelectedPath;
            }
        }

       

      
        
       
        private string ReadVoltage()
        {
            KE2430.Write(":READ?");
            num = KE2430.ReadString();
            num = num.Substring(1, 12);
            return num; // Convert.ToDouble(num);
        }

        private void buttonStartTest_Click(object sender, EventArgs e)
        {
            dt.Clear();
            System.IO.Directory.CreateDirectory("D:/data/Images/" + filetimestamp);
            buttonOpenAll.PerformClick();
            Vektrex_Driver.switchoff(SMUSS400);
            TestProcess(1);
            if (radioButtonPowerMeter.Checked)
            {

                MessageBox.Show("Insert Power Meter fixture, then click OK !!! ", "IV_Test", MessageBoxButtons.OK);
                TestProcess(2);
            }
            VCSEL_number[194] = Vektrex_Driver.pulldata(SMUSS400);

            string img_Path = "D:/data/Images/" + filetimestamp + "/";
            Imagemerge(img_Path, filetimestamp,textBoxSN.Text);
            Leakage_test();


            saveTestData(dt);
            int fail_count = 0;
           
            foreach (DataRow row in dt.Rows)
            {
                if (row["bin"] != "1")
                {
                    fail_count = fail_count + 1;
                }
            }
            MessageBox.Show(fail_count.ToString()+" Failed Sections. Testing Complete", "IV_Test", MessageBoxButtons.OK);
        }
        public static String GetTimeStamp(DateTime value)
        {
            return value.ToString("yyyyMM-ddHHmmss");
        }
        public static void getstd(DataTable dtemp)
        {


            double sum = 0;
            int i = 0;
            double sum_of_Squares = 0;
            foreach(DataRow row in dtemp.Rows)
            {
                if(row["bin"] == "1")
                {
                    sum += Convert.ToDouble(row["voltage(V)"]);
                    i = i + 1;
                }
            }
            
            double average = sum / i;
            foreach (DataRow row2 in dtemp.Rows)
            {
                if (row2["bin"] == "1")
                {
                    sum_of_Squares = sum_of_Squares + (Convert.ToDouble(row2["voltage(V)"]) - average) * (Convert.ToDouble(row2["voltage(V)"]) - average);
                }
            }
            double std;
            std = Math.Sqrt(sum_of_Squares / i);

        }
        public static void Imagemerge(string filepath,string filetimestamp,string textbox)
        {

            //routine to merge individual images calling python script
            string inputdir = filepath;
            string finalimg = filepath+"Merged_image_"+ textbox+"_" + filetimestamp+".png";
            string heatmap_img = filepath + "HeatMap_img" + textbox + "_" + filetimestamp+".png";
            // 1) Create Process Info
            var psi = new ProcessStartInfo();
            psi.FileName = @"C:\Python\python.exe";

            // 2) Provide script
            var script = @"C:\Users\lidarlabus\PycharmProjects\pythonProject\main.py ";
            
            // 3) Provide arguments
            var arguments = @"-inputdir " + inputdir+" -Finalimg "+finalimg+" -Heatmapimg "+heatmap_img;


            psi.Arguments = $"{script}\"\"{arguments}\"";

            // 4) Process configuration
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;

            // 5) Execute process and get output
            var errors = "";
            var results = "";

            using (var process = Process.Start(psi))
            {
                
                //   errors = process.StandardError.ReadToEnd();
                // results = process.StandardOutput.ReadToEnd();
            }



        }
      

       


        private void TestProcess(int testNumber)
        {
            
            string colA, colB;
    
           
            tempthrowaway[0]=Vektrex_Driver.Single_Pulse(SMUSS400, "SINGLEPULSE", 10, 0.025, .0001, 0.0001);

   

            VCSEL_count = 0;
            Camera.Image.Save("D:/data/Images/" + filetimestamp + "/teempimg" + textBoxSN.Text + ".png");

            for (int i = 1; i <= 16; i++)
            {
                if (i <= 10)
                {
                    dr = dt.NewRow();
                    //dr["Col"] = (i - 1).ToString();
                    colA = Convert.ToString(i);
                    KE7001A.Write(":clos (@1!1!" + colA + ")");
                }
                else
                {
                    dr = dt.NewRow();
                    dr["Col"] = (i - 11).ToString();
                    colA = Convert.ToString(i - 10);
                    KE7001A.Write(":clos (@2!1!" + colA + ")");
                }

                for (int n = 1; n <= 12; n++) // change to 12 as dual-test SP2.0
                {
                    if (n <= 10)
                    {
                        dr = dt.NewRow();

                        colB = Convert.ToString(n);
                        KE7001B.Write(":clos (@1!1!" + colB + ")");
                        powerUp(testNumber, (i-1).ToString(), (n-1).ToString());
                        KE7001B.Write(":open (@1!1!" + colB + ")");
                    }
                    else
                    {
                        dr = dt.NewRow();

                        colB = Convert.ToString(n - 10);
                        KE7001B.Write(":clos (@2!1!" + colB + ")");
                        powerUp(testNumber, (i-1).ToString(), (n-1).ToString());
                        KE7001B.Write(":open (@2!1!" + colB + ")");
                    }
                }

                if (i <= 10)
                {
                    dr = dt.NewRow();
                    //dr["Col"] = (i - 1).ToString();
                    colA = Convert.ToString(i);
                    KE7001A.Write(":open (@1!1!" + colA + ")");
                }
                else
                {
                    dr = dt.NewRow();
                    //dr["Col"] = (i - 11).ToString();
                    colA = Convert.ToString(i - 10);
                    KE7001A.Write(":open (@2!1!" + colA + ")");
                }
            }
        
      
    }
        public static class CSVUtlity { }

        public static void ToCSV(DataTable dtDataTable, string strFilepath)
        {
            StreamWriter sw = new StreamWriter(strFilepath, false);
            //headers    
            for (int i = 0; i < dtDataTable.Columns.Count; i++)
            {
                sw.Write(dtDataTable.Columns[i]);
                if (i < dtDataTable.Columns.Count - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
            foreach (DataRow dr in dtDataTable.Rows)
            {
                for (int i = 0; i < dtDataTable.Columns.Count; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        string value = dr[i].ToString();
                        if (value.Contains(","))
                        {
                            value = String.Format("\"{0}\"", value);
                            sw.Write(value);
                        }
                        else
                        {
                            sw.Write(dr[i].ToString());
                        }
                    }
                    if (i < dtDataTable.Columns.Count - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();


        }
            

        private void powerUp(int testNumber,string row,string col)
        {
            if (radioButtonKL.Checked == true)
            {
                KE2430.Write(":OUTP ON");

                if (testNumber == 1)
                {
                    Thread.Sleep(250);
                    VCSEL_number[VCSEL_count] = ReadVoltage();
                  //  SaveImage(folderName + "\\" + "SN-" + textBoxSN.Text + "-" + Convert.ToString(VCSEL_count) + ".jpeg");
                }
               
                KE2430.Write(":OUTP OFF");
                
            }

            if (radioButtonVT.Checked == true && testNumber == 1)//added and for row col not1
            {
                if(row=="1" && col=="1")
                {


                    dr["Row"] = row;
                    dr["Col"] = col;

                    string tempvolt;

                    tempvolt = Vektrex_Driver.Single_Pulse(SMUSS400, "SINGLEPULSE", 10, 0.1, .0001, 0.0001);
                    Thread.Sleep(250);

                    Camera.Image.Save("D:/data/Images/" + filetimestamp + "/" + row + "_" + col + "_" + textBoxSN.Text + ".png");
                    //richTextPowerMeter.Text = VCSEL_number[VCSEL_count];
                    
                    VCSEL_number[VCSEL_count] = Vektrex_Driver.pulldata(SMUSS400);


                    dr["voltage(V)"] = VCSEL_number[VCSEL_count];
                    dr["current(A)"] = ".1";
                    dr["Bin"] = Bininig.pass_fail(Convert.ToDouble(VCSEL_number[VCSEL_count]), 7, 10, 5);
                    dt.Rows.Add(dr);
                    Console.WriteLine(dt);

                    Thread.Sleep(100);


                }
                else 
                {


                    dr["Row"] = row;
                    dr["Col"] = col;
                    string tempvolt;
                    tempvolt = Vektrex_Driver.Single_Pulse(SMUSS400, "SINGLEPULSE", 10, 0.1, .0001, 0.0001);

                    Camera.Image.Save("D:/data/Images/" + filetimestamp + "/" + row + "_" + col + "_" + textBoxSN.Text + ".png");
                   
                    VCSEL_number[VCSEL_count] = Vektrex_Driver.pulldata(SMUSS400);


                    dr["voltage(V)"] = VCSEL_number[VCSEL_count];
                    dr["current(A)"] = ".1";
                    dr["Bin"] = Bininig.pass_fail(Convert.ToDouble(VCSEL_number[VCSEL_count]), 7, 10, 5);
                    dt.Rows.Add(dr);
                    Console.WriteLine(dt);

                    Thread.Sleep(100);

                }
             

            }


            VCSEL_count++;
        }

       private void saveTestData(DataTable dt)
        {
            ToCSV(dt, "D:/data/Results/"+ textBoxSN.Text+"_"+ filetimestamp+ ".csv");
        }

        private void buttonVDC_ON_Click(object sender, EventArgs e)
        {
            if (radioButtonKL.Checked)
                KE2430.Write(":OUTP ON");

            if (radioButtonBK.Checked)
                serialPort1.Write("SOUT0" + "\r"); 
        }

        private void buttonReadVDC_Click(object sender, EventArgs e)
        {
            if (radioButtonKL.Checked)
                KE2430.Write(":INIT");

            if (radioButtonBK.Checked)
                serialPort1.Write("GETD" + "\r");

            if (radioButtonPulse.Checked)
            {
                KE2430.Write(":READ?");
                num = KE2430.ReadString();
            }
        }
        private void buttonVDC_OFF_Click(object sender, EventArgs e)
        {
            if (radioButtonKL.Checked)
                KE2430.Write(":OUTP OFF");

            if (radioButtonBK.Checked)
                serialPort1.Write("SOUT1" + "\r");
        }

        private void buttonPMSettings_Click(object sender, EventArgs e)
        {
            //PowerMeter();
            //richTextPowerMeter.Text = Vektrex_Driver.Single_Pulse(SMUSS400, "SINGLEPULSE", 10, 0.1, .0001, 0.0001);
        }

        private void NumericUpDownBK_ValueChanged(object sender, EventArgs e)
        {
            serialPort1.Write("VOLT0" + Convert.ToString(Math.Round(numericUpDown1.Value * 10, 0)) + "\r");
        }


        private void NumericUpDownCurrent_ValueChanged(object sender, EventArgs e)
        {
            KE2430.Write(":SOUR:CURR " + Convert.ToString(numericUpDownCurrent.Value));
        }
        // EXIT AND CLOSE
        private void button6_Click(object sender, EventArgs e)
        {
            buttonOpenAll.PerformClick();
            KE7001A.Dispose();
            KE7001B.Dispose();

            if (radioButtonKL.Checked)
            {
                KE2430.Write(":OUTP OFF");
                KE2430.Dispose();
            }

            if (radioButtonVT.Checked)
            {
                SMUSS400.writeLine("OUTP1 OFF");
            }

            System.Diagnostics.Process.GetCurrentProcess().Kill();
            this.Close();
        }

        private void buttonOpenAll_Click(object sender, EventArgs e)
        {
            KE7001A.Write(":open all");
            KE7001A.Write("*CLS");

            checkBox1.Checked  = false; checkBox2.Checked  = false; checkBox3.Checked  = false; checkBox4.Checked  = false; checkBox5.Checked  = false;
            checkBox6.Checked  = false; checkBox7.Checked  = false; checkBox8.Checked  = false; checkBox9.Checked  = false; checkBox10.Checked = false;
            checkBox11.Checked = false; checkBox12.Checked = false; checkBox13.Checked = false; checkBox14.Checked = false; checkBox15.Checked = false;
            checkBox16.Checked = false; checkBox17.Checked = false; checkBox18.Checked = false; checkBox19.Checked = false; checkBox20.Checked = false;
            checkBox21.Checked = false; checkBox22.Checked = false; checkBox23.Checked = false; checkBox24.Checked = false; checkBox25.Checked = false;
            checkBox26.Checked = false; checkBox27.Checked = false; checkBox28.Checked = false; checkBox29.Checked = false; checkBox30.Checked = false;
            checkBox31.Checked = false; checkBox32.Checked = false; checkBox33.Checked = false; checkBox34.Checked = false; checkBox35.Checked = false;
            checkBox36.Checked = false; checkBox37.Checked = false; checkBox38.Checked = false; checkBox39.Checked = false; checkBox40.Checked = false;
            checkBox41.Checked = false; checkBox42.Checked = false; checkBox43.Checked = false; checkBox44.Checked = false; checkBox45.Checked = false;
            checkBox46.Checked = false; checkBox47.Checked = false; checkBox48.Checked = false; checkBox49.Checked = false; checkBox50.Checked = false;
            checkBox51.Checked = false; checkBox52.Checked = false; checkBox53.Checked = false; checkBox54.Checked = false; checkBox55.Checked = false;
            checkBox56.Checked = false; checkBox57.Checked = false; checkBox58.Checked = false; checkBox59.Checked = false; checkBox60.Checked = false;
            checkBox61.Checked = false; checkBox62.Checked = false; checkBox63.Checked = false; checkBox64.Checked = false; checkBox65.Checked = false;
            checkBox66.Checked = false; checkBox67.Checked = false; checkBox68.Checked = false; checkBox69.Checked = false; checkBox70.Checked = false;
            checkBox71.Checked = false; checkBox72.Checked = false; checkBox73.Checked = false; checkBox74.Checked = false; checkBox75.Checked = false;
            checkBox76.Checked = false; checkBox77.Checked = false; checkBox78.Checked = false; checkBox79.Checked = false; checkBox80.Checked = false;

            KE7001B.Write(":open all");
            KE7001B.Write("*CLS");

            checkBox81.Checked  = false; checkBox82.Checked  = false; checkBox83.Checked  = false; checkBox84.Checked  = false; checkBox85.Checked  = false;
            checkBox86.Checked  = false; checkBox87.Checked  = false; checkBox88.Checked  = false; checkBox89.Checked  = false; checkBox90.Checked  = false;
            checkBox91.Checked  = false; checkBox92.Checked  = false; checkBox93.Checked  = false; checkBox94.Checked  = false; checkBox95.Checked  = false;
            checkBox96.Checked  = false; checkBox97.Checked  = false; checkBox98.Checked  = false; checkBox99.Checked  = false; checkBox100.Checked = false;
            checkBox101.Checked = false; checkBox102.Checked = false; checkBox103.Checked = false; checkBox104.Checked = false; checkBox105.Checked = false;
            checkBox106.Checked = false; checkBox107.Checked = false; checkBox108.Checked = false; checkBox109.Checked = false; checkBox110.Checked = false;
            checkBox111.Checked = false; checkBox112.Checked = false; checkBox113.Checked = false; checkBox114.Checked = false; checkBox115.Checked = false;
            checkBox116.Checked = false; checkBox117.Checked = false; checkBox118.Checked = false; checkBox119.Checked = false; checkBox120.Checked = false;
            checkBox121.Checked = false; checkBox122.Checked = false; checkBox123.Checked = false; checkBox124.Checked = false; checkBox125.Checked = false;
            checkBox126.Checked = false; checkBox127.Checked = false; checkBox128.Checked = false; checkBox129.Checked = false; checkBox130.Checked = false;
            checkBox131.Checked = false; checkBox132.Checked = false; checkBox133.Checked = false; checkBox134.Checked = false; checkBox135.Checked = false;
            checkBox136.Checked = false; checkBox137.Checked = false; checkBox138.Checked = false; checkBox139.Checked = false; checkBox140.Checked = false;
            checkBox141.Checked = false; checkBox142.Checked = false; checkBox143.Checked = false; checkBox144.Checked = false; checkBox145.Checked = false;
            checkBox146.Checked = false; checkBox147.Checked = false; checkBox148.Checked = false; checkBox149.Checked = false; checkBox150.Checked = false;
            checkBox151.Checked = false; checkBox152.Checked = false; checkBox153.Checked = false; checkBox154.Checked = false; checkBox155.Checked = false;
            checkBox156.Checked = false; checkBox157.Checked = false; checkBox158.Checked = false; checkBox159.Checked = false; checkBox160.Checked = false;

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e) { if (checkBox1.Checked == true) KE7001A.Write(":clos (@	1	!	1	!	1	)"); else KE7001A.Write(":open (@	1	!	1	!	1	)"); }
        private void checkBox2_CheckedChanged(object sender, EventArgs e) { if (checkBox2.Checked == true) KE7001A.Write(":clos (@	1	!	1	!	2	)"); else KE7001A.Write(":open (@	1	!	1	!	2	)"); }
        private void checkBox3_CheckedChanged(object sender, EventArgs e) { if (checkBox3.Checked == true) KE7001A.Write(":clos (@	1	!	1	!	3	)"); else KE7001A.Write(":open (@	1	!	1	!	3	)"); }
        private void checkBox4_CheckedChanged(object sender, EventArgs e) { if (checkBox4.Checked == true) KE7001A.Write(":clos (@	1	!	1	!	4	)"); else KE7001A.Write(":open (@	1	!	1	!	4	)"); }
        private void checkBox5_CheckedChanged(object sender, EventArgs e) { if (checkBox5.Checked == true) KE7001A.Write(":clos (@	1	!	1	!	5	)"); else KE7001A.Write(":open (@	1	!	1	!	5	)"); }
        private void checkBox6_CheckedChanged(object sender, EventArgs e) { if (checkBox6.Checked == true) KE7001A.Write(":clos (@	1	!	1	!	6	)"); else KE7001A.Write(":open (@	1	!	1	!	6	)"); }
        private void checkBox7_CheckedChanged(object sender, EventArgs e) { if (checkBox7.Checked == true) KE7001A.Write(":clos (@	1	!	1	!	7	)"); else KE7001A.Write(":open (@	1	!	1	!	7	)"); }
        private void checkBox8_CheckedChanged(object sender, EventArgs e) { if (checkBox8.Checked == true) KE7001A.Write(":clos (@	1	!	1	!	8	)"); else KE7001A.Write(":open (@	1	!	1	!	8	)"); }
        private void checkBox9_CheckedChanged(object sender, EventArgs e) { if (checkBox9.Checked == true) KE7001A.Write(":clos (@	1	!	1	!	9	)"); else KE7001A.Write(":open (@	1	!	1	!	9	)"); }
        private void checkBox10_CheckedChanged(object sender, EventArgs e) { if (checkBox10.Checked == true) KE7001A.Write(":clos (@	1	!	1	!	10	)"); else KE7001A.Write(":open (@	1	!	1	!	10	)"); }
        private void checkBox11_CheckedChanged(object sender, EventArgs e) { if (checkBox11.Checked == true) KE7001A.Write(":clos (@	1	!	2	!	1	)"); else KE7001A.Write(":open (@	1	!	2	!	1	)"); }
        private void checkBox12_CheckedChanged(object sender, EventArgs e) { if (checkBox12.Checked == true) KE7001A.Write(":clos (@	1	!	2	!	2	)"); else KE7001A.Write(":open (@	1	!	2	!	2	)"); }
        private void checkBox13_CheckedChanged(object sender, EventArgs e) { if (checkBox13.Checked == true) KE7001A.Write(":clos (@	1	!	2	!	3	)"); else KE7001A.Write(":open (@	1	!	2	!	3	)"); }
        private void checkBox14_CheckedChanged(object sender, EventArgs e) { if (checkBox14.Checked == true) KE7001A.Write(":clos (@	1	!	2	!	4	)"); else KE7001A.Write(":open (@	1	!	2	!	4	)"); }
        private void checkBox15_CheckedChanged(object sender, EventArgs e) { if (checkBox15.Checked == true) KE7001A.Write(":clos (@	1	!	2	!	5	)"); else KE7001A.Write(":open (@	1	!	2	!	5	)"); }
        private void checkBox16_CheckedChanged(object sender, EventArgs e) { if (checkBox16.Checked == true) KE7001A.Write(":clos (@	1	!	2	!	6	)"); else KE7001A.Write(":open (@	1	!	2	!	6	)"); }
        private void checkBox17_CheckedChanged(object sender, EventArgs e) { if (checkBox17.Checked == true) KE7001A.Write(":clos (@	1	!	2	!	7	)"); else KE7001A.Write(":open (@	1	!	2	!	7	)"); }
        private void checkBox18_CheckedChanged(object sender, EventArgs e) { if (checkBox18.Checked == true) KE7001A.Write(":clos (@	1	!	2	!	8	)"); else KE7001A.Write(":open (@	1	!	2	!	8	)"); }
        private void checkBox19_CheckedChanged(object sender, EventArgs e) { if (checkBox19.Checked == true) KE7001A.Write(":clos (@	1	!	2	!	9	)"); else KE7001A.Write(":open (@	1	!	2	!	9	)"); }
        private void checkBox20_CheckedChanged(object sender, EventArgs e) { if (checkBox20.Checked == true) KE7001A.Write(":clos (@	1	!	2	!	10	)"); else KE7001A.Write(":open (@	1	!	2	!	10	)"); }
        private void checkBox21_CheckedChanged(object sender, EventArgs e) { if (checkBox21.Checked == true) KE7001A.Write(":clos (@	1	!	3	!	1	)"); else KE7001A.Write(":open (@	1	!	3	!	1	)"); }
        private void checkBox22_CheckedChanged(object sender, EventArgs e) { if (checkBox22.Checked == true) KE7001A.Write(":clos (@	1	!	3	!	2	)"); else KE7001A.Write(":open (@	1	!	3	!	2	)"); }
        private void checkBox23_CheckedChanged(object sender, EventArgs e) { if (checkBox23.Checked == true) KE7001A.Write(":clos (@	1	!	3	!	3	)"); else KE7001A.Write(":open (@	1	!	3	!	3	)"); }
        private void checkBox24_CheckedChanged(object sender, EventArgs e) { if (checkBox24.Checked == true) KE7001A.Write(":clos (@	1	!	3	!	4	)"); else KE7001A.Write(":open (@	1	!	3	!	4	)"); }
        private void checkBox25_CheckedChanged(object sender, EventArgs e) { if (checkBox25.Checked == true) KE7001A.Write(":clos (@	1	!	3	!	5	)"); else KE7001A.Write(":open (@	1	!	3	!	5	)"); }
        private void checkBox26_CheckedChanged(object sender, EventArgs e) { if (checkBox26.Checked == true) KE7001A.Write(":clos (@	1	!	3	!	6	)"); else KE7001A.Write(":open (@	1	!	3	!	6	)"); }
        private void checkBox27_CheckedChanged(object sender, EventArgs e) { if (checkBox27.Checked == true) KE7001A.Write(":clos (@	1	!	3	!	7	)"); else KE7001A.Write(":open (@	1	!	3	!	7	)"); }
        private void checkBox28_CheckedChanged(object sender, EventArgs e) { if (checkBox28.Checked == true) KE7001A.Write(":clos (@	1	!	3	!	8	)"); else KE7001A.Write(":open (@	1	!	3	!	8	)"); }
        private void checkBox29_CheckedChanged(object sender, EventArgs e) { if (checkBox29.Checked == true) KE7001A.Write(":clos (@	1	!	3	!	9	)"); else KE7001A.Write(":open (@	1	!	3	!	9	)"); }
        private void checkBox30_CheckedChanged(object sender, EventArgs e) { if (checkBox30.Checked == true) KE7001A.Write(":clos (@	1	!	3	!	10	)"); else KE7001A.Write(":open (@	1	!	3	!	10	)"); }
        private void checkBox31_CheckedChanged(object sender, EventArgs e) { if (checkBox31.Checked == true) KE7001A.Write(":clos (@	1	!	4	!	1	)"); else KE7001A.Write(":open (@	1	!	4	!	1	)"); }
        private void checkBox32_CheckedChanged(object sender, EventArgs e) { if (checkBox32.Checked == true) KE7001A.Write(":clos (@	1	!	4	!	2	)"); else KE7001A.Write(":open (@	1	!	4	!	2	)"); }
        private void checkBox33_CheckedChanged(object sender, EventArgs e) { if (checkBox33.Checked == true) KE7001A.Write(":clos (@	1	!	4	!	3	)"); else KE7001A.Write(":open (@	1	!	4	!	3	)"); }
        private void checkBox34_CheckedChanged(object sender, EventArgs e) { if (checkBox34.Checked == true) KE7001A.Write(":clos (@	1	!	4	!	4	)"); else KE7001A.Write(":open (@	1	!	4	!	4	)"); }
        private void checkBox35_CheckedChanged(object sender, EventArgs e) { if (checkBox35.Checked == true) KE7001A.Write(":clos (@	1	!	4	!	5	)"); else KE7001A.Write(":open (@	1	!	4	!	5	)"); }
        private void checkBox36_CheckedChanged(object sender, EventArgs e) { if (checkBox36.Checked == true) KE7001A.Write(":clos (@	1	!	4	!	6	)"); else KE7001A.Write(":open (@	1	!	4	!	6	)"); }
        private void checkBox37_CheckedChanged(object sender, EventArgs e) { if (checkBox37.Checked == true) KE7001A.Write(":clos (@	1	!	4	!	7	)"); else KE7001A.Write(":open (@	1	!	4	!	7	)"); }
        private void checkBox38_CheckedChanged(object sender, EventArgs e) { if (checkBox38.Checked == true) KE7001A.Write(":clos (@	1	!	4	!	8	)"); else KE7001A.Write(":open (@	1	!	4	!	8	)"); }
        private void checkBox39_CheckedChanged(object sender, EventArgs e) { if (checkBox39.Checked == true) KE7001A.Write(":clos (@	1	!	4	!	9	)"); else KE7001A.Write(":open (@	1	!	4	!	9	)"); }
        private void checkBox40_CheckedChanged(object sender, EventArgs e) { if (checkBox40.Checked == true) KE7001A.Write(":clos (@	1	!	4	!	10	)"); else KE7001A.Write(":open (@	1	!	4	!	10	)"); }
        private void checkBox41_CheckedChanged(object sender, EventArgs e) { if (checkBox41.Checked == true) KE7001A.Write(":clos (@	2	!	1	!	1	)"); else KE7001A.Write(":open (@	2	!	1	!	1	)"); }
        private void checkBox42_CheckedChanged(object sender, EventArgs e) { if (checkBox42.Checked == true) KE7001A.Write(":clos (@	2	!	1	!	2	)"); else KE7001A.Write(":open (@	2	!	1	!	2	)"); }
        private void checkBox43_CheckedChanged(object sender, EventArgs e) { if (checkBox43.Checked == true) KE7001A.Write(":clos (@	2	!	1	!	3	)"); else KE7001A.Write(":open (@	2	!	1	!	3	)"); }
        private void checkBox44_CheckedChanged(object sender, EventArgs e) { if (checkBox44.Checked == true) KE7001A.Write(":clos (@	2	!	1	!	4	)"); else KE7001A.Write(":open (@	2	!	1	!	4	)"); }
        private void checkBox45_CheckedChanged(object sender, EventArgs e) { if (checkBox45.Checked == true) KE7001A.Write(":clos (@	2	!	1	!	5	)"); else KE7001A.Write(":open (@	2	!	1	!	5	)"); }
        private void checkBox46_CheckedChanged(object sender, EventArgs e) { if (checkBox46.Checked == true) KE7001A.Write(":clos (@	2	!	1	!	6	)"); else KE7001A.Write(":open (@	2	!	1	!	6	)"); }
        private void checkBox47_CheckedChanged(object sender, EventArgs e) { if (checkBox47.Checked == true) KE7001A.Write(":clos (@	2	!	1	!	7	)"); else KE7001A.Write(":open (@	2	!	1	!	7	)"); }
        private void checkBox48_CheckedChanged(object sender, EventArgs e) { if (checkBox48.Checked == true) KE7001A.Write(":clos (@	2	!	1	!	8	)"); else KE7001A.Write(":open (@	2	!	1	!	8	)"); }
        private void checkBox49_CheckedChanged(object sender, EventArgs e) { if (checkBox49.Checked == true) KE7001A.Write(":clos (@	2	!	1	!	9	)"); else KE7001A.Write(":open (@	2	!	1	!	9	)"); }
        private void checkBox50_CheckedChanged(object sender, EventArgs e) { if (checkBox50.Checked == true) KE7001A.Write(":clos (@	2	!	1	!	10	)"); else KE7001A.Write(":open (@	2	!	1	!	10	)"); }
        private void checkBox51_CheckedChanged(object sender, EventArgs e) { if (checkBox51.Checked == true) KE7001A.Write(":clos (@	2	!	2	!	1	)"); else KE7001A.Write(":open (@	2	!	2	!	1	)"); }
        private void checkBox52_CheckedChanged(object sender, EventArgs e) { if (checkBox52.Checked == true) KE7001A.Write(":clos (@	2	!	2	!	2	)"); else KE7001A.Write(":open (@	2	!	2	!	2	)"); }
        private void checkBox53_CheckedChanged(object sender, EventArgs e) { if (checkBox53.Checked == true) KE7001A.Write(":clos (@	2	!	2	!	3	)"); else KE7001A.Write(":open (@	2	!	2	!	3	)"); }
        private void checkBox54_CheckedChanged(object sender, EventArgs e) { if (checkBox54.Checked == true) KE7001A.Write(":clos (@	2	!	2	!	4	)"); else KE7001A.Write(":open (@	2	!	2	!	4	)"); }
        private void checkBox55_CheckedChanged(object sender, EventArgs e) { if (checkBox55.Checked == true) KE7001A.Write(":clos (@	2	!	2	!	5	)"); else KE7001A.Write(":open (@	2	!	2	!	5	)"); }
        private void checkBox56_CheckedChanged(object sender, EventArgs e) { if (checkBox56.Checked == true) KE7001A.Write(":clos (@	2	!	2	!	6	)"); else KE7001A.Write(":open (@	2	!	2	!	6	)"); }
        private void checkBox57_CheckedChanged(object sender, EventArgs e) { if (checkBox57.Checked == true) KE7001A.Write(":clos (@	2	!	2	!	7	)"); else KE7001A.Write(":open (@	2	!	2	!	7	)"); }
        private void checkBox58_CheckedChanged(object sender, EventArgs e) { if (checkBox58.Checked == true) KE7001A.Write(":clos (@	2	!	2	!	8	)"); else KE7001A.Write(":open (@	2	!	2	!	8	)"); }
        private void checkBox59_CheckedChanged(object sender, EventArgs e) { if (checkBox59.Checked == true) KE7001A.Write(":clos (@	2	!	2	!	9	)"); else KE7001A.Write(":open (@	2	!	2	!	9	)"); }
        private void checkBox60_CheckedChanged(object sender, EventArgs e) { if (checkBox60.Checked == true) KE7001A.Write(":clos (@	2	!	2	!	10	)"); else KE7001A.Write(":open (@	2	!	2	!	10	)"); }
        private void checkBox61_CheckedChanged(object sender, EventArgs e) { if (checkBox61.Checked == true) KE7001A.Write(":clos (@	2	!	3	!	1	)"); else KE7001A.Write(":open (@	2	!	3	!	1	)"); }
        private void checkBox62_CheckedChanged(object sender, EventArgs e) { if (checkBox62.Checked == true) KE7001A.Write(":clos (@	2	!	3	!	2	)"); else KE7001A.Write(":open (@	2	!	3	!	2	)"); }
        private void checkBox63_CheckedChanged(object sender, EventArgs e) { if (checkBox63.Checked == true) KE7001A.Write(":clos (@	2	!	3	!	3	)"); else KE7001A.Write(":open (@	2	!	3	!	3	)"); }
        private void checkBox64_CheckedChanged(object sender, EventArgs e) { if (checkBox64.Checked == true) KE7001A.Write(":clos (@	2	!	3	!	4	)"); else KE7001A.Write(":open (@	2	!	3	!	4	)"); }
        private void checkBox65_CheckedChanged(object sender, EventArgs e) { if (checkBox65.Checked == true) KE7001A.Write(":clos (@	2	!	3	!	5	)"); else KE7001A.Write(":open (@	2	!	3	!	5	)"); }
        private void checkBox66_CheckedChanged(object sender, EventArgs e) { if (checkBox66.Checked == true) KE7001A.Write(":clos (@	2	!	3	!	6	)"); else KE7001A.Write(":open (@	2	!	3	!	6	)"); }
        private void checkBox67_CheckedChanged(object sender, EventArgs e) { if (checkBox67.Checked == true) KE7001A.Write(":clos (@	2	!	3	!	7	)"); else KE7001A.Write(":open (@	2	!	3	!	7	)"); }
        private void checkBox68_CheckedChanged(object sender, EventArgs e) { if (checkBox68.Checked == true) KE7001A.Write(":clos (@	2	!	3	!	8	)"); else KE7001A.Write(":open (@	2	!	3	!	8	)"); }
        private void checkBox69_CheckedChanged(object sender, EventArgs e) { if (checkBox69.Checked == true) KE7001A.Write(":clos (@	2	!	3	!	9	)"); else KE7001A.Write(":open (@	2	!	3	!	9	)"); }
        private void checkBox70_CheckedChanged(object sender, EventArgs e) { if (checkBox70.Checked == true) KE7001A.Write(":clos (@	2	!	3	!	10	)"); else KE7001A.Write(":open (@	2	!	3	!	10	)"); }
        private void checkBox71_CheckedChanged(object sender, EventArgs e) { if (checkBox71.Checked == true) KE7001A.Write(":clos (@	2	!	4	!	1	)"); else KE7001A.Write(":open (@	2	!	4	!	1	)"); }
        private void checkBox72_CheckedChanged(object sender, EventArgs e) { if (checkBox72.Checked == true) KE7001A.Write(":clos (@	2	!	4	!	2	)"); else KE7001A.Write(":open (@	2	!	4	!	2	)"); }
        private void checkBox73_CheckedChanged(object sender, EventArgs e) { if (checkBox73.Checked == true) KE7001A.Write(":clos (@	2	!	4	!	3	)"); else KE7001A.Write(":open (@	2	!	4	!	3	)"); }
        private void checkBox74_CheckedChanged(object sender, EventArgs e) { if (checkBox74.Checked == true) KE7001A.Write(":clos (@	2	!	4	!	4	)"); else KE7001A.Write(":open (@	2	!	4	!	4	)"); }
        private void checkBox75_CheckedChanged(object sender, EventArgs e) { if (checkBox75.Checked == true) KE7001A.Write(":clos (@	2	!	4	!	5	)"); else KE7001A.Write(":open (@	2	!	4	!	5	)"); }
        private void checkBox76_CheckedChanged(object sender, EventArgs e) { if (checkBox76.Checked == true) KE7001A.Write(":clos (@	2	!	4	!	6	)"); else KE7001A.Write(":open (@	2	!	4	!	6	)"); }
        private void checkBox77_CheckedChanged(object sender, EventArgs e) { if (checkBox77.Checked == true) KE7001A.Write(":clos (@	2	!	4	!	7	)"); else KE7001A.Write(":open (@	2	!	4	!	7	)"); }
        private void checkBox78_CheckedChanged(object sender, EventArgs e) { if (checkBox78.Checked == true) KE7001A.Write(":clos (@	2	!	4	!	8	)"); else KE7001A.Write(":open (@	2	!	4	!	8	)"); }
        private void checkBox79_CheckedChanged(object sender, EventArgs e) { if (checkBox79.Checked == true) KE7001A.Write(":clos (@	2	!	4	!	9	)"); else KE7001A.Write(":open (@	2	!	4	!	9	)"); }
        private void checkBox80_CheckedChanged(object sender, EventArgs e) { if (checkBox80.Checked == true) KE7001A.Write(":clos (@	2	!	4	!	10	)"); else KE7001A.Write(":open (@	2	!	4	!	10	)"); }
        private void checkBox81_CheckedChanged(object sender, EventArgs e) { if (checkBox81.Checked == true) KE7001B.Write(":clos (@	1	!	1	!	1	)"); else KE7001B.Write(":open (@	1	!	1	!	1	)"); }
        private void checkBox82_CheckedChanged(object sender, EventArgs e) { if (checkBox82.Checked == true) KE7001B.Write(":clos (@	1	!	1	!	2	)"); else KE7001B.Write(":open (@	1	!	1	!	2	)"); }
        private void checkBox83_CheckedChanged(object sender, EventArgs e) { if (checkBox83.Checked == true) KE7001B.Write(":clos (@	1	!	1	!	3	)"); else KE7001B.Write(":open (@	1	!	1	!	3	)"); }
        private void checkBox84_CheckedChanged(object sender, EventArgs e) { if (checkBox84.Checked == true) KE7001B.Write(":clos (@	1	!	1	!	4	)"); else KE7001B.Write(":open (@	1	!	1	!	4	)"); }
        private void checkBox85_CheckedChanged(object sender, EventArgs e) { if (checkBox85.Checked == true) KE7001B.Write(":clos (@	1	!	1	!	5	)"); else KE7001B.Write(":open (@	1	!	1	!	5	)"); }
        private void checkBox86_CheckedChanged(object sender, EventArgs e) { if (checkBox86.Checked == true) KE7001B.Write(":clos (@	1	!	1	!	6	)"); else KE7001B.Write(":open (@	1	!	1	!	6	)"); }
        private void checkBox87_CheckedChanged(object sender, EventArgs e) { if (checkBox87.Checked == true) KE7001B.Write(":clos (@	1	!	1	!	7	)"); else KE7001B.Write(":open (@	1	!	1	!	7	)"); }
        private void checkBox88_CheckedChanged(object sender, EventArgs e) { if (checkBox88.Checked == true) KE7001B.Write(":clos (@	1	!	1	!	8	)"); else KE7001B.Write(":open (@	1	!	1	!	8	)"); }
        private void checkBox89_CheckedChanged(object sender, EventArgs e) { if (checkBox89.Checked == true) KE7001B.Write(":clos (@	1	!	1	!	9	)"); else KE7001B.Write(":open (@	1	!	1	!	9	)"); }
        private void checkBox90_CheckedChanged(object sender, EventArgs e) { if (checkBox90.Checked == true) KE7001B.Write(":clos (@	1	!	1	!	10	)"); else KE7001B.Write(":open (@	1	!	1	!	10	)"); }
        private void checkBox91_CheckedChanged(object sender, EventArgs e) { if (checkBox91.Checked == true) KE7001B.Write(":clos (@	1	!	2	!	1	)"); else KE7001B.Write(":open (@	1	!	2	!	1	)"); }
        private void checkBox92_CheckedChanged(object sender, EventArgs e) { if (checkBox92.Checked == true) KE7001B.Write(":clos (@	1	!	2	!	2	)"); else KE7001B.Write(":open (@	1	!	2	!	2	)"); }
        private void checkBox93_CheckedChanged(object sender, EventArgs e) { if (checkBox93.Checked == true) KE7001B.Write(":clos (@	1	!	2	!	3	)"); else KE7001B.Write(":open (@	1	!	2	!	3	)"); }
        private void checkBox94_CheckedChanged(object sender, EventArgs e) { if (checkBox94.Checked == true) KE7001B.Write(":clos (@	1	!	2	!	4	)"); else KE7001B.Write(":open (@	1	!	2	!	4	)"); }
        private void checkBox95_CheckedChanged(object sender, EventArgs e) { if (checkBox95.Checked == true) KE7001B.Write(":clos (@	1	!	2	!	5	)"); else KE7001B.Write(":open (@	1	!	2	!	5	)"); }
        private void checkBox96_CheckedChanged(object sender, EventArgs e) { if (checkBox96.Checked == true) KE7001B.Write(":clos (@	1	!	2	!	6	)"); else KE7001B.Write(":open (@	1	!	2	!	6	)"); }
        private void checkBox97_CheckedChanged(object sender, EventArgs e) { if (checkBox97.Checked == true) KE7001B.Write(":clos (@	1	!	2	!	7	)"); else KE7001B.Write(":open (@	1	!	2	!	7	)"); }
        private void checkBox98_CheckedChanged(object sender, EventArgs e) { if (checkBox98.Checked == true) KE7001B.Write(":clos (@	1	!	2	!	8	)"); else KE7001B.Write(":open (@	1	!	2	!	8	)"); }
        private void checkBox99_CheckedChanged(object sender, EventArgs e) { if (checkBox99.Checked == true) KE7001B.Write(":clos (@	1	!	2	!	9	)"); else KE7001B.Write(":open (@	1	!	2	!	9	)"); }
        private void checkBox100_CheckedChanged(object sender, EventArgs e) { if (checkBox100.Checked == true) KE7001B.Write(":clos (@	1	!	2	!	10	)"); else KE7001B.Write(":open (@	1	!	2	!	10	)"); }
        private void checkBox101_CheckedChanged(object sender, EventArgs e) { if (checkBox101.Checked == true) KE7001B.Write(":clos (@	1	!	3	!	1	)"); else KE7001B.Write(":open (@	1	!	3	!	1	)"); }
        private void checkBox102_CheckedChanged(object sender, EventArgs e) { if (checkBox102.Checked == true) KE7001B.Write(":clos (@	1	!	3	!	2	)"); else KE7001B.Write(":open (@	1	!	3	!	2	)"); }
        private void checkBox103_CheckedChanged(object sender, EventArgs e) { if (checkBox103.Checked == true) KE7001B.Write(":clos (@	1	!	3	!	3	)"); else KE7001B.Write(":open (@	1	!	3	!	3	)"); }
        private void checkBox104_CheckedChanged(object sender, EventArgs e) { if (checkBox104.Checked == true) KE7001B.Write(":clos (@	1	!	3	!	4	)"); else KE7001B.Write(":open (@	1	!	3	!	4	)"); }
        private void checkBox105_CheckedChanged(object sender, EventArgs e) { if (checkBox105.Checked == true) KE7001B.Write(":clos (@	1	!	3	!	5	)"); else KE7001B.Write(":open (@	1	!	3	!	5	)"); }
        private void checkBox106_CheckedChanged(object sender, EventArgs e) { if (checkBox106.Checked == true) KE7001B.Write(":clos (@	1	!	3	!	6	)"); else KE7001B.Write(":open (@	1	!	3	!	6	)"); }
        private void checkBox107_CheckedChanged(object sender, EventArgs e) { if (checkBox107.Checked == true) KE7001B.Write(":clos (@	1	!	3	!	7	)"); else KE7001B.Write(":open (@	1	!	3	!	7	)"); }
        private void checkBox108_CheckedChanged(object sender, EventArgs e) { if (checkBox108.Checked == true) KE7001B.Write(":clos (@	1	!	3	!	8	)"); else KE7001B.Write(":open (@	1	!	3	!	8	)"); }
        private void checkBox109_CheckedChanged(object sender, EventArgs e) { if (checkBox109.Checked == true) KE7001B.Write(":clos (@	1	!	3	!	9	)"); else KE7001B.Write(":open (@	1	!	3	!	9	)"); }
        private void checkBox110_CheckedChanged(object sender, EventArgs e) { if (checkBox110.Checked == true) KE7001B.Write(":clos (@	1	!	3	!	10	)"); else KE7001B.Write(":open (@	1	!	3	!	10	)"); }
        private void checkBox111_CheckedChanged(object sender, EventArgs e) { if (checkBox111.Checked == true) KE7001B.Write(":clos (@	1	!	4	!	1	)"); else KE7001B.Write(":open (@	1	!	4	!	1	)"); }
        private void checkBox112_CheckedChanged(object sender, EventArgs e) { if (checkBox112.Checked == true) KE7001B.Write(":clos (@	1	!	4	!	2	)"); else KE7001B.Write(":open (@	1	!	4	!	2	)"); }
        private void checkBox113_CheckedChanged(object sender, EventArgs e) { if (checkBox113.Checked == true) KE7001B.Write(":clos (@	1	!	4	!	3	)"); else KE7001B.Write(":open (@	1	!	4	!	3	)"); }
        private void checkBox114_CheckedChanged(object sender, EventArgs e) { if (checkBox114.Checked == true) KE7001B.Write(":clos (@	1	!	4	!	4	)"); else KE7001B.Write(":open (@	1	!	4	!	4	)"); }
        private void checkBox115_CheckedChanged(object sender, EventArgs e) { if (checkBox115.Checked == true) KE7001B.Write(":clos (@	1	!	4	!	5	)"); else KE7001B.Write(":open (@	1	!	4	!	5	)"); }
        private void checkBox116_CheckedChanged(object sender, EventArgs e) { if (checkBox116.Checked == true) KE7001B.Write(":clos (@	1	!	4	!	6	)"); else KE7001B.Write(":open (@	1	!	4	!	6	)"); }
        private void checkBox117_CheckedChanged(object sender, EventArgs e) { if (checkBox117.Checked == true) KE7001B.Write(":clos (@	1	!	4	!	7	)"); else KE7001B.Write(":open (@	1	!	4	!	7	)"); }
        private void checkBox118_CheckedChanged(object sender, EventArgs e) { if (checkBox118.Checked == true) KE7001B.Write(":clos (@	1	!	4	!	8	)"); else KE7001B.Write(":open (@	1	!	4	!	8	)"); }
        private void checkBox119_CheckedChanged(object sender, EventArgs e) { if (checkBox119.Checked == true) KE7001B.Write(":clos (@	1	!	4	!	9	)"); else KE7001B.Write(":open (@	1	!	4	!	9	)"); }
        private void checkBox120_CheckedChanged(object sender, EventArgs e) { if (checkBox120.Checked == true) KE7001B.Write(":clos (@	1	!	4	!	10	)"); else KE7001B.Write(":open (@	1	!	4	!	10	)"); }
        private void checkBox121_CheckedChanged(object sender, EventArgs e) { if (checkBox121.Checked == true) KE7001B.Write(":clos (@	2	!	1	!	1	)"); else KE7001B.Write(":open (@	2	!	1	!	1	)"); }
        private void checkBox122_CheckedChanged(object sender, EventArgs e) { if (checkBox122.Checked == true) KE7001B.Write(":clos (@	2	!	1	!	2	)"); else KE7001B.Write(":open (@	2	!	1	!	2	)"); }
        private void checkBox123_CheckedChanged(object sender, EventArgs e) { if (checkBox123.Checked == true) KE7001B.Write(":clos (@	2	!	1	!	3	)"); else KE7001B.Write(":open (@	2	!	1	!	3	)"); }
        private void checkBox124_CheckedChanged(object sender, EventArgs e) { if (checkBox124.Checked == true) KE7001B.Write(":clos (@	2	!	1	!	4	)"); else KE7001B.Write(":open (@	2	!	1	!	4	)"); }
        private void checkBox125_CheckedChanged(object sender, EventArgs e) { if (checkBox125.Checked == true) KE7001B.Write(":clos (@	2	!	1	!	5	)"); else KE7001B.Write(":open (@	2	!	1	!	5	)"); }
        private void checkBox126_CheckedChanged(object sender, EventArgs e) { if (checkBox126.Checked == true) KE7001B.Write(":clos (@	2	!	1	!	6	)"); else KE7001B.Write(":open (@	2	!	1	!	6	)"); }
        private void checkBox127_CheckedChanged(object sender, EventArgs e) { if (checkBox127.Checked == true) KE7001B.Write(":clos (@	2	!	1	!	7	)"); else KE7001B.Write(":open (@	2	!	1	!	7	)"); }
        private void checkBox128_CheckedChanged(object sender, EventArgs e) { if (checkBox128.Checked == true) KE7001B.Write(":clos (@	2	!	1	!	8	)"); else KE7001B.Write(":open (@	2	!	1	!	8	)"); }
        private void checkBox129_CheckedChanged(object sender, EventArgs e) { if (checkBox129.Checked == true) KE7001B.Write(":clos (@	2	!	1	!	9	)"); else KE7001B.Write(":open (@	2	!	1	!	9	)"); }
        private void checkBox130_CheckedChanged(object sender, EventArgs e) { if (checkBox130.Checked == true) KE7001B.Write(":clos (@	2	!	1	!	10	)"); else KE7001B.Write(":open (@	2	!	1	!	10	)"); }
        private void checkBox131_CheckedChanged(object sender, EventArgs e) { if (checkBox131.Checked == true) KE7001B.Write(":clos (@	2	!	2	!	1	)"); else KE7001B.Write(":open (@	2	!	2	!	1	)"); }
        private void checkBox132_CheckedChanged(object sender, EventArgs e) { if (checkBox132.Checked == true) KE7001B.Write(":clos (@	2	!	2	!	2	)"); else KE7001B.Write(":open (@	2	!	2	!	2	)"); }
        private void checkBox133_CheckedChanged(object sender, EventArgs e) { if (checkBox133.Checked == true) KE7001B.Write(":clos (@	2	!	2	!	3	)"); else KE7001B.Write(":open (@	2	!	2	!	3	)"); }
        private void checkBox134_CheckedChanged(object sender, EventArgs e) { if (checkBox134.Checked == true) KE7001B.Write(":clos (@	2	!	2	!	4	)"); else KE7001B.Write(":open (@	2	!	2	!	4	)"); }
        private void checkBox135_CheckedChanged(object sender, EventArgs e) { if (checkBox135.Checked == true) KE7001B.Write(":clos (@	2	!	2	!	5	)"); else KE7001B.Write(":open (@	2	!	2	!	5	)"); }
        private void checkBox136_CheckedChanged(object sender, EventArgs e) { if (checkBox136.Checked == true) KE7001B.Write(":clos (@	2	!	2	!	6	)"); else KE7001B.Write(":open (@	2	!	2	!	6	)"); }
        private void checkBox137_CheckedChanged(object sender, EventArgs e) { if (checkBox137.Checked == true) KE7001B.Write(":clos (@	2	!	2	!	7	)"); else KE7001B.Write(":open (@	2	!	2	!	7	)"); }
        private void checkBox138_CheckedChanged(object sender, EventArgs e) { if (checkBox138.Checked == true) KE7001B.Write(":clos (@	2	!	2	!	8	)"); else KE7001B.Write(":open (@	2	!	2	!	8	)"); }
        private void checkBox139_CheckedChanged(object sender, EventArgs e) { if (checkBox139.Checked == true) KE7001B.Write(":clos (@	2	!	2	!	9	)"); else KE7001B.Write(":open (@	2	!	2	!	9	)"); }
        private void checkBox140_CheckedChanged(object sender, EventArgs e) { if (checkBox140.Checked == true) KE7001B.Write(":clos (@	2	!	2	!	10	)"); else KE7001B.Write(":open (@	2	!	2	!	10	)"); }
        private void checkBox141_CheckedChanged(object sender, EventArgs e) { if (checkBox141.Checked == true) KE7001B.Write(":clos (@	2	!	3	!	1	)"); else KE7001B.Write(":open (@	2	!	3	!	1	)"); }
        private void checkBox142_CheckedChanged(object sender, EventArgs e) { if (checkBox142.Checked == true) KE7001B.Write(":clos (@	2	!	3	!	2	)"); else KE7001B.Write(":open (@	2	!	3	!	2	)"); }
        private void checkBox143_CheckedChanged(object sender, EventArgs e) { if (checkBox143.Checked == true) KE7001B.Write(":clos (@	2	!	3	!	3	)"); else KE7001B.Write(":open (@	2	!	3	!	3	)"); }
        private void checkBox144_CheckedChanged(object sender, EventArgs e) { if (checkBox144.Checked == true) KE7001B.Write(":clos (@	2	!	3	!	4	)"); else KE7001B.Write(":open (@	2	!	3	!	4	)"); }
        private void checkBox145_CheckedChanged(object sender, EventArgs e) { if (checkBox145.Checked == true) KE7001B.Write(":clos (@	2	!	3	!	5	)"); else KE7001B.Write(":open (@	2	!	3	!	5	)"); }
        private void checkBox146_CheckedChanged(object sender, EventArgs e) { if (checkBox146.Checked == true) KE7001B.Write(":clos (@	2	!	3	!	6	)"); else KE7001B.Write(":open (@	2	!	3	!	6	)"); }
        private void checkBox147_CheckedChanged(object sender, EventArgs e) { if (checkBox147.Checked == true) KE7001B.Write(":clos (@	2	!	3	!	7	)"); else KE7001B.Write(":open (@	2	!	3	!	7	)"); }
        private void checkBox148_CheckedChanged(object sender, EventArgs e) { if (checkBox148.Checked == true) KE7001B.Write(":clos (@	2	!	3	!	8	)"); else KE7001B.Write(":open (@	2	!	3	!	8	)"); }
        private void checkBox149_CheckedChanged(object sender, EventArgs e) { if (checkBox149.Checked == true) KE7001B.Write(":clos (@	2	!	3	!	9	)"); else KE7001B.Write(":open (@	2	!	3	!	9	)"); }
        private void checkBox150_CheckedChanged(object sender, EventArgs e) { if (checkBox150.Checked == true) KE7001B.Write(":clos (@	2	!	3	!	10	)"); else KE7001B.Write(":open (@	2	!	3	!	10	)"); }
        private void checkBox151_CheckedChanged(object sender, EventArgs e) { if (checkBox151.Checked == true) KE7001B.Write(":clos (@	2	!	4	!	1	)"); else KE7001B.Write(":open (@	2	!	4	!	1	)"); }
        private void checkBox152_CheckedChanged(object sender, EventArgs e) { if (checkBox152.Checked == true) KE7001B.Write(":clos (@	2	!	4	!	2	)"); else KE7001B.Write(":open (@	2	!	4	!	2	)"); }
        private void checkBox153_CheckedChanged(object sender, EventArgs e) { if (checkBox153.Checked == true) KE7001B.Write(":clos (@	2	!	4	!	3	)"); else KE7001B.Write(":open (@	2	!	4	!	3	)"); }
        private void checkBox154_CheckedChanged(object sender, EventArgs e) { if (checkBox154.Checked == true) KE7001B.Write(":clos (@	2	!	4	!	4	)"); else KE7001B.Write(":open (@	2	!	4	!	4	)"); }
        private void checkBox155_CheckedChanged(object sender, EventArgs e) { if (checkBox155.Checked == true) KE7001B.Write(":clos (@	2	!	4	!	5	)"); else KE7001B.Write(":open (@	2	!	4	!	5	)"); }
        private void checkBox156_CheckedChanged(object sender, EventArgs e) { if (checkBox156.Checked == true) KE7001B.Write(":clos (@	2	!	4	!	6	)"); else KE7001B.Write(":open (@	2	!	4	!	6	)"); }
        private void checkBox157_CheckedChanged(object sender, EventArgs e) { if (checkBox157.Checked == true) KE7001B.Write(":clos (@	2	!	4	!	7	)"); else KE7001B.Write(":open (@	2	!	4	!	7	)"); }
        private void checkBox158_CheckedChanged(object sender, EventArgs e) { if (checkBox158.Checked == true) KE7001B.Write(":clos (@	2	!	4	!	8	)"); else KE7001B.Write(":open (@	2	!	4	!	8	)"); }
        private void checkBox159_CheckedChanged(object sender, EventArgs e) { if (checkBox159.Checked == true) KE7001B.Write(":clos (@	2	!	4	!	9	)"); else KE7001B.Write(":open (@	2	!	4	!	9	)"); }
        private void checkBox160_CheckedChanged(object sender, EventArgs e) { if (checkBox160.Checked == true) KE7001B.Write(":clos (@	2	!	4	!	10	)"); else KE7001B.Write(":open (@	2	!	4	!	10	)"); }

        private void button4_Click(object sender, EventArgs e)
        {
                         
        }
        
        private void onFrameEvent(object sender, EventArgs e)
        {
            try
            {
                uEye.Camera Camera = sender as uEye.Camera;

                //Display the image on the screen
                Camera.Display.Render(DisplayHandle, uEye.Defines.DisplayRenderMode.FitToWindow);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }



        //The following are key definitions
        private void Quit_Click(object sender, EventArgs e)
        {
            try
            {
                //Free the allocated image memory and closes the program
                Camera.Exit();
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            try
            {

                IDS_Driver.saveimage(Camera, @"C:/Temp/TestIMG_" + GetTimeStamp(DateTime.Now)+".png");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }



        private void Live_Click(object sender, EventArgs e)
        {
            try
            {
                IDS_Driver.takepic(Camera);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void DisplayWindow_Click(object sender, EventArgs e)
        {

        }

        private void Refresh_Click_1(object sender, EventArgs e)
        {
            IDS_Driver.saveimage(Camera, @"C:/Temp/TestIMG_" + GetTimeStamp(DateTime.Now) + ".png");

        }

        private void button5_Click(object sender, EventArgs e)
        {
            filetimestamp= GetTimeStamp(DateTime.Now);
            Vektrex_Driver.switchoff(SMUSS400);


        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
        
         }
        private void Leakage_test()
        {
            try
            {
                for (int i = 1; i < 7; i++)
                {
                    dt.Columns.Add("Voltage(V)_" + i);
                    dt.Columns.Add("Current(uA)_" + i);

                }

                //switch vektrex back to primary use
                Vektrex_Driver.switchon(SMUSS400);
                for (int i = 1; i < 13; i++)
                {
                    int count = 1;

                    for (int j = 10; j < 49; j = j + 10)
                    {
                        Console.WriteLine(j);
                        dr = dt.NewRow();
                        dr["Row"] = "ANA" + (i - 1);
                        if (i < 7)
                        {
                            dr["Col"] = "CL0 - CL15";
                        }
                        else
                        {
                            dr["Col"] = "CR0 - CR15";
                        }

                        Console.WriteLine(dt);
                        Thread.Sleep(250);
                        Keithley_7001_Driver.clos_rows_test(KE7001A, KE7001B, i);
                        string tempcurr = Keithley2400_Driver.single_voltage(KE2400, -j);
                        Keithley_7001_Driver.open_rows_test(KE7001A, KE7001B, i);
                        volt_sweep[count] = (-j).ToString();
                        curr_sweep[count] = tempcurr;


                        dr["Bin"] = Bininig.pass_fail(Convert.ToDouble(tempcurr), -10, 0, 7);

                        count = count + 1;

                    }
                    for (int k = 0; k < 4; k++)
                    {
                        Console.WriteLine(k);
                        dr["Current(uA)_" + (k + 1)] = curr_sweep[k + 1];
                        dr["Voltage(V)_" + (k + 1)] = volt_sweep[k + 1];

                    }
                    dt.Rows.Add(dr);

                }

                
                Vektrex_Driver.switchoff(SMUSS400);
                // saveTestData(dt);



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            for (int i = 1; i < 7; i++)
            {
                dt.Columns.Add("Voltage(V)_" + i);
                dt.Columns.Add("Current(uA)_" + i);

            }

            //switch vektrex back to primary use
            Vektrex_Driver.switchon(SMUSS400);
            for (int i = 1; i < 13; i++)
            {
                int count = 1;

                for (double j = .3; j < .6; j = j + .1)
                {
                    Console.WriteLine(j);
                    dr = dt.NewRow();
                    dr["Row"] = "ANA" + (i - 1);
                    if (i < 7)
                    {
                        dr["Col"] = "CL0 - CL15";
                    }
                    else
                    {
                        dr["Col"] = "CR0 - CR15";
                    }

                    Console.WriteLine(dt);
                    Thread.Sleep(250);
                    Keithley_7001_Driver.clos_rows_test(KE7001A, KE7001B, i);
                    string tempcurr = Keithley2400_Driver.single_Current(KE2400, j);

                    Keithley_7001_Driver.open_rows_test(KE7001A, KE7001B, i);
                    volt_sweep[count] = (-j).ToString();
                    curr_sweep[count] = tempcurr;


                    dr["Bin"] = Bininig.pass_fail(Convert.ToDouble(tempcurr), -10, 0, 7);

                    count = count + 1;

                }
                for (int k = 0; k < 4; k++)
                {
                    Console.WriteLine(k);
                    dr["Current(uA)_" + (k + 1)] = curr_sweep[k + 1];
                    dr["Voltage(V)_" + (k + 1)] = volt_sweep[k + 1];

                }
                dt.Rows.Add(dr);

            }

            for (int i = 1; i < 3; i++)
            {
                int count = 1;
                Console.WriteLine(i);
                dr = dt.NewRow();

                if (i == 1)
                {
                    dr["Row"] = "LeftDie";
                    dr["Col"] = "CL0 - CL15";
                }
                else if (i == 2)
                {
                    dr["Row"] = "RightDie";
                    dr["Col"] = "CR0 - CR15";
                }
                for (double j = .3; j < .6; j = j + .1)
                {


                    Console.WriteLine(dt);
                    Thread.Sleep(250);
                    Keithley_7001_Driver.clos_rows_test_2(KE7001A, KE7001B, 8);
                    string tempcurr = Keithley2400_Driver.single_Current(KE2400, j);
                    Keithley_7001_Driver.open_rows_test_2(KE7001A, KE7001B, 8);
                    volt_sweep[count] = (-j).ToString();
                    curr_sweep[count] = tempcurr;


                    dr["Bin"] = Bininig.pass_fail(Convert.ToDouble(tempcurr), -20, 0, 7);

                    count = count + 1;

                }
                for (int k = 0; k < 4; k++)
                {
                    Console.WriteLine(k);
                    dr["Current(uA)_" + (k + 1)] = curr_sweep[k + 1];
                    dr["Voltage(V)_" + (k + 1)] = volt_sweep[k + 1];

                }
                dt.Rows.Add(dr);


            }
            Vektrex_Driver.switchoff(SMUSS400);
            // saveTestData(dt);


        }

    }
}

