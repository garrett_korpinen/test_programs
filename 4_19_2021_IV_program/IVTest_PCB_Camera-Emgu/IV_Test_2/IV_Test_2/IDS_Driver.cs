﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace IV_Test
{
    class IDS_Driver
    {
      
        public static void init(uEye.Camera cam)
        {
            try
            {
                cam.Init();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
         
          
        }
        public static void zoom(uEye.Camera cam,int zoomfactor)
        {
            try
            {

                cam.Zoom.Set(zoomfactor);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            

        }
        public static void setdelay(uEye.Camera cam, int delay)
        {
            try
            {

                cam.Trigger.Delay.Set(delay);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        public static void setgain(uEye.Camera cam, int gain)
        {
            try
            {

                cam.Gain.Equals(gain);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        public static void setbacklevl(uEye.Camera cam, int backlevel)
        {
            try
            {

                cam.BlackLevel.Equals(backlevel);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        public static void invertimg(uEye.Camera cam)
        {
            try
            {
                cam.Display.AutoRender.SetMode(uEye.Defines.DisplayRenderMode.Rotate_180);

                //uEye.DisplayAutoRender.SetMode(DisplayRenderMode mode);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        public static void gettriggersettings(uEye.Camera cam)
        {
            try
            {
                //added for mirroring
                cam.RopEffect.Set(uEye.Defines.RopEffectMode.UpDown,true);
                cam.RopEffect.Set(uEye.Defines.RopEffectMode.LeftRight, true);
                //cam.Zoom.Set(4);
                //cam.Timing.Exposure.Get(out double f64Value);
                //MessageBox.Show(f64Value.ToString());
                cam.Trigger.Set(uEye.Defines.TriggerMode.Lo_Hi);
                //cam.IO.Flash.SetMode(uEye.Defines.IO.FlashMode.TriggerLowActive);
                cam.Acquisition.Capture();
                cam.Trigger.Get(out uEye.Defines.TriggerMode mode);
                //MessageBox.Show(mode.ToString());
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        public static void forcepic(uEye.Camera cam)
        {
            try
            {
                cam.Trigger.Force();
              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        public static void memalloc(uEye.Camera cam)
        {
            try
            {
                cam.Memory.Allocate();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
        }
        public static void setexposure(uEye.Camera cam, int exposuretime)
        {
            try
            {
                cam.Timing.Exposure.Set(exposuretime);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
        public static void takepic(uEye.Camera cam)
        {
            try
            {
                cam.Acquisition.Capture();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
          
        }
        public static double getexptime(uEye.Camera cam)
        {
            try
            {
                cam.Timing.Exposure.Get(out double f64Value);
                return f64Value;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return 0;
            }


        }
        public static Bitmap getactivemem(uEye.Camera cam)
        {
            try
            {
                int s32MemID;
                cam.Memory.GetActive(out s32MemID);
                Bitmap image;
                cam.Memory.ToBitmap(s32MemID, out image);
                return image;
            }
            catch (Exception ex)
            {
                int s32MemID;
                cam.Memory.GetActive(out s32MemID);
                Bitmap image;
                cam.Memory.ToBitmap(s32MemID, out image);
                return image;

                MessageBox.Show(ex.Message);
            }
           


        }
        public static void saveimage(uEye.Camera cam,string path)
        {
            try
            {
                cam.Display.AutoRender.SetMode(uEye.Defines.DisplayRenderMode.Rotate_180);

                // takepic(cam);
                Bitmap image;

                image = getactivemem(cam);
                var converter = new ImageConverter();
                var imageData = (byte[])converter.ConvertTo(image, typeof(byte[]));
                image.Save(path);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

    }
}
